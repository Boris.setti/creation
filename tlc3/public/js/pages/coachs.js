/*
home-coachs-dev.js
Ce fichier contient tous les scripts .js de la page home-coachs.html, à l'exception des scripts
extérieurs.
*/
//###############################################################
// dépendances
//###############################################################
    /*
      - jQuery
      - dev-parametres.js   : paramètres pour mes scripts js    
      - commun.js           : Fonctions de debug
      - simu-serveur.json   : fichier permettant de travailler quand on n'a pas de serveur.
    */

//###############################################################
// variables Globales
//###############################################################

    //===============================================================
    // ressources
    //===============================================================

        // NB: tous les répertoires et noms de fichiers sont définis dans commun.js,
        //     seul le chemin vers le répertoire racine du projet est défini ici.

        // si commun.js n'a pas été lancé, ou n'a pas créé les variables de ressources
        if((typeof(chemin) == "undefined") || (typeof(nomFic) == "undefined")){
            errorDebug("Attention : les variables de ressources n'ont pas été créées par commun.js");
        } // si les variables de ressources n'ont pas été créées par commun.js
        
        // chemin vers la racine du projet
        //================================
        chemin.racine = ""; // chemin pour aller à la racine du projet (spécifique à cette page)
        
    //===============================================================
    // Variables d'infos sur les coachs
    //===============================================================
        // variable qui va recevoir les infos sur les coachs
        var coachs = {};

        // variable qui va recevoir la liste des coachs
        coachs.liste = null;

    //===============================================================
    // variables pour la cartographie
    //===============================================================

        // objet qui englobe toutes les variables liées à la cartographie,
        // pour éviter les conflits si d'autres variables du même nom sont
        // utilisées dans d'autres scripts js appelés depuis la même page.
        var carto = {};

        // variable pour l'objet map
        carto.map = undefined;

        // variable pour indiquer le status de la carte, affichée ou non
        carto.carteAffichee = false; // pour l'initialisation

        // marker de la position de l'utilisateur, sur la carte.
        carto.userMarker = null;
        
        // icône pour le marker de la position utilisateur
        carto.userMarkerIcon = null;

        // niveau de zoom par défaut
        carto.niveauZoomParDefaut = 11;

        // variable qui contiendra les symbols des marqueurs, qui correspondent aux différentes catégories de coachs
        carto.markerSymbols = []; // (2019 06 27) NB: n'est plus utilisé pour le moment, mais pourra reservir.

        // variable qui contiendra la liste des markers affichés sur la carte
        carto.listeMarkers = [];
        
        // variable qui va contenir l'carto.infoWindow, c'est à dire les infos sur un coach sélectionné
        carto.infoWindow= null;

    //===============================================================
    // Variables pour la localisation
    //===============================================================
        /*
            Cette variable globale contient toutes les données nécessaires
            pour la localisation
        */

        // objet qui englobe toutes les variables liées à la localisation,
        // pour éviter les conflits si d'autres variables du même nom sont
        // utilisées dans d'autres scripts js appelés depuis la même page.
        var loc = {};

        // clef API
        loc.apiKey = "AIzaSyCN9u2Lq6gVEBzeyQ-L6yNmFtCD4A4FKLY";

        // variable pour la localisation initiale forcée
        loc.locInit = ""; // par défaut, pas de localisation initiale forcée

        // variable pour l'adresse à geocoder
        loc.adresseAGeocoder = "";

        // initialisation des variables de localisation de l'utilisateur
        initUserLoc();

        // variable qui contiendra l'adresse pour laquelle l'utilisateur cherche les coachs à proximité
        loc.adresse = "";

    //===============================================================
    // Variables pour les filtres
    //===============================================================
        /*
        Pour l'instant, on code en dur tout ce qui doit aller dans le filtreCoachs.
        NB: Il n'est pas certain qu'on change ça pour passer par un transfert AJAX depuis la base de donnée.
            On verra ce qui est le plus efficace.
        */

        // objet avec tous les filtres et toutes les options
        var filtreCoachs = {};
        var temp;  // variable temporaire utilisée pour la définition de certains filtres.

        // initialisation des variables de l'objet filtreCoachs
        filtreCoachs.label    = [];  // noms affichés pour les filtres
        filtreCoachs.nom      = [];  // préfixes pour les id des cases à cocher pour un type de filtre
        filtreCoachs.fonction = [];  // liste des fonctions avec les parties spécifiques des filtres.
        filtreCoachs.options  = [];  // liste des options pour chaque type de filtre
        filtreCoachs.param    = [];  // liste de paramètres aditionnels pour le filtre

        // définitions générale pour les filtres, checkboxes, badges, ...
        //----------------------------------------------------------------
        filtreCoachs.prefixeClasse     = "TLC-coach-filtre-";
        filtreCoachs.prefixeIdCheckbox = "TLC-coach-filtre-option-";
        filtreCoachs.prefixeIdBadge    = "TLC-coach-filtre-badge-";
        filtreCoachs.prefixeIdSpinner  = "TLC-coach-fil-spinner-";
        filtreCoachs.ClasseCheckbox    = filtreCoachs.prefixeClasse + "checkbox";

        // Définition des filtres : nom, prefixe, classe, et liste des options associés
        //-----------------------------------------------------------------------------
        filtreCoachs.label.push("catégorie");
        filtreCoachs.nom.push("cat");
        filtreCoachs.fonction.push(filtreSpecifiqueCat);  
        filtreCoachs.options.push(tlc.categories);
        filtreCoachs.param.push(null); // ne sert pas pour ce filtre
            //-------//
        filtreCoachs.label.push("jours d'ouverture");
        filtreCoachs.nom.push("jours");
        filtreCoachs.fonction.push(filtreSpecifiqueJours);  
        filtreCoachs.options.push(["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]);
        filtreCoachs.param.push(null); // ne sert pas pour ce filtre
            //-------//
        filtreCoachs.label.push("heures d'ouverture");
        filtreCoachs.nom.push("heures");
        filtreCoachs.fonction.push(filtreSpecifiqueHeures);  
        filtreCoachs.options.push(["8h-10h", "10h-12h", "12h-14h", "14h-16h", "16h-18h", "18h-20h"]);
        temp = {}; // utiliastion de la variable temporaire
        temp.heureDeb = [ 8, 10, 12, 14, 16, 18]; // heures de début des créneaux
        temp.heureFin = [10, 12, 14, 16, 18, 20]; // heures de fin des créneaux
        filtreCoachs.param.push(temp); // ne sert pas pour ce filtre
            //-------//
        filtreCoachs.label.push("prix d'une séance");
        filtreCoachs.nom.push("prix");
        filtreCoachs.fonction.push(filtreSpecifiquePrix);  
        filtreCoachs.options.push(["gratuité de la 1ère séance", "prix non renseigné", "< 50€", "50€ - 75€", 
                                    "75€ - 100€", "100€ - 150€", "150€ - 200€", "200€ - 250€", "> 250€"]);
        temp = {}; // utilisation de la variable temporaire
        temp.prixMin = [0, 0,  0, 50,  75, 100, 150, 200, 250];     // exlu
        temp.prixMax = [0, 0, 50, 75, 100, 150, 200, 250, 1000000]; // inclu    
        filtreCoachs.param.push(temp); 

    //===============================================================
    // Variables pour les recherches
    //===============================================================

        // objet avec les variables pour les recherches
        var recherche = {};

        // variable pour mémoriser la dernière recherche de coach
        recherche.lastCoachSearch = ""; 

        // mémorise le dernier type de recherche {"adresse", "coach"}. Par défaut, c'est une adresse
        // car au lancement de la page, on cherche les coachs à proximité de la localisation de l'utilisateur.
        recherche.lastSearchType = "adresse"; 

        // mémorise le nombre de recherches
        recherche.nbre = 0;


        // nombre maxi de coachs pour la recherche d'un coach.
        // NB: ne s'applique pas à la recherche des coachs ) proximité.
        // NB: la valeur est renvoyée par le serveur lors de la recherche d'un coach.
        //     Elle est définie dans parametres.php
        recherche.nbreMaxCoachs = null;

//###############################################################
// fonction génériques
//###############################################################

    //================================================================================================================
    // ET logique élément à élément entre 2 tableaux
    //================================================================================================================
    function etTableaux(a, b){
        /*---------------------------------------------------------------------------------------------
         cette fonction réalise un ET logique entre le tableau a et la tableau b.
         Ce ET logique se fait élément par élément.
         Ca implique que les 2 tableaux doivent avoir la même taille.

         On l'a mis en place car la ligne de commande "c = a && b;" donne des résultats aberrants.
        ----------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "etTableaux()");

        // init
        var c = []; // tableau avec les résultats

        // vérifie que les tableaux ont bien la même taille
        if(a.length === b.length){

            // boucle sur les éléments des tableaux
            for(num = 0; num<a.length; num++){
                c.push(a[num] && b[num]);  // réalise l'opération logique
            } // for(num = 0; num<a.length; num++)

            return c; // renvoie le tableau résultat

        } else { // if(a.length === b.length)
            return 0; // on renvoie une erreur
        } // if(a.length === b.length) - else
    } // function etTableaux()

    //================================================================================================================
    // tableau de n élements, initialisés à une valeur donnée
    //================================================================================================================
    function initTableau(nbre, valeur){
        /*-------------------------------------------------------------------------------------------
        function initTableau(nbre, valeur)

        Cette fonction crée un tableau avec "nbre" éléments, tous initialisés avec "valeur".

        Entrées : 
        ---------
        nbre    : le nombre d'éléments du tableau.
        valeur  : la valeur d'initialisation de chaque élément du tableau (tous identiques).

        Sorties : 
        ---------
        tableau : le tableau initialisé.
        -------------------------------------------------------------------------------------------*/

        // définition et création du tableau
        var tableau = []; 

        // boucle pour ajouter le bon nombre d'éléments
        for(num = 0; num<nbre; num++){
            tableau.push(valeur);
        } // for(num = 0; num<nbre; num++)

        // renvoie le tableau de résultats
        return tableau;
    } // function initTableau()

    //================================================================================================================
    // suppression des doublons dans les éléments d'un tableau
    //================================================================================================================
    function supprimeDoublons(a){

        b = [];

        // boucle for sur les éléments du tableau a
        for(num = 0; num < a.length; num++){
            if(b.indexOf(a[num]) == -1){ // si le nouvel élément n'est pas déjà dans b
                b.push(a[num]);  // alors on l'y ajoute
            } // if
        } // for(num ...)

        // revoie les résultats
        return b;
    } // function supprimeDoublonsTableau()

//###############################################################
// conversions ID - num- slected
//###############################################################

    // cette partie regroupe des fonctions de conversion, qui sont surtout utilisées dans
    // la partie filtrage, mais qui pourrait aussi être utilisées ailleurs.

    //================================================================================================================
    // id de coachs -> numéros de coachs
    //================================================================================================================
    function idCoach2numCoach(idCoach){

        /*-----------------------------------------------------------------------------
        function idCoach2numCoach(idCoach)

        Cette fonction converti une liste d'ID de coachs, en une liste d'index
        de coachs dans la variable globale coachs.liste .

        Entrées : 
        ---------
        idCoach  : un ID de coach, ou un tableau avec des ID de coachs.
                   NB: chaque ID peut être un nombre, ou une chaîne de caractère avec ce nombre.

        Sorties : 
        ---------
        numCoach : un numéro ou un tableau avec les numéros des coachs correspondants.
                   NB: le numéro peut être 0 (1er numéro). 

        Notes : 
        -------
        fonction validée le 2019 06 26
        -----------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "idCoach2numCoach()");

        // init
        var index; // variable temporaire

        // fonctionnement différent si la variable d'entrée est un tableau de nombre ou juet un nombre
        if(typeof(idCoach) === 'object'){  // si idCoach est un tableau
            var numCoach = []; // tableau qui va recevoir les index des coachs

            // boucle sur le nombre d'ID de la liste
            for(numId = 0; numId < idCoach.length; numId++){
                index = coachs.id.indexOf(parseInt(idCoach[numId]));
                if(index > -1){
                    numCoach.push(index);
                } else {
                    numCoach.push(null);
                } // if(index > -1) - else
            } // for(num)

        } else {  // si idCoach est une valeur numérique
            var numCoach;

            // extraction de l'index
            index = coachs.id.indexOf(parseInt(idCoach));

            // traitement des résultats
            if(index > -1){
                numCoach = index;
            } else {
                numCoach = null;
            } // if(index > -1) - else
        } // if(typeof(idCoach) - else

        // renvoi des résultats
        return numCoach;
    } // function idCoach2numCoach()

    //================================================================================================================
    // id d'offres -> numéros d'offres
    //================================================================================================================
    function idOffre2numOffre(idOffre){

        /*-----------------------------------------------------------------------------
        function idOffre2numOffre(idOffre)

        Cette fonction converti une liste d'ID d'offres de coaching, en une liste d'index
        d'offres de coaching dans la variable globale coachs.offres .

        Entrées : 
        ---------
        idOffre  : un ID d'une offre, ou un tableau avec des ID d'offres de coaching.
                   NB: chaque ID peut être un nombre, ou une chaîne de caractère avec ce nombre.

        Sorties : 
        ---------
        numOffre : un numéro ou un tableau avec les numéros des offres correspondantes.
                   NB: le numéro peut être 0 (1er numéro). 

        Notes : 
        -------
        fonction validée le 2019 06 26
        -----------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "idOffre2numOffre()");

        // init
        var index; // variable temporaire

        // fonctionnement différent si la variable d'entrée est un tableau de nombre ou juet un nombre
        if(typeof(idOffre) === 'object'){  // si idOffre est un tableau
            var numOffre = []; // tableau qui va recevoir les index des coachs

            // boucle sur le nombre d'ID de la liste
            for(numId = 0; numId < idOffre.length; numId++){
                index = coachs.offres.id.indexOf(parseInt(idOffre[numId]));
                if(index > -1){
                    numOffre.push(index);
                } else {
                    numOffre.push(null);
                } // if(index > -1) - else
            } // for(num)

        } else {  // si idOffre est une valeur numérique
            var numOffre;

            // extraction de l'index
            index = coachs.offres.id.indexOf(parseInt(idOffre));

            // traitement des résultats
            if(index > -1){
                numOffre = index;
            } else {
                numOffre = null;
            } // if(index > -1) - else
        } // if(typeof(idCoach) - else

        // renvoi des résultats
        return numOffre;
    } // function idOffre2numOffre()

    //================================================================================================================
    // tableau des coachs sélectionnés, à partir d'une liste d'ID de coachs
    //================================================================================================================
    function idCoachs2coachsSelected(coachsSelected, idCoachs){

        /*-----------------------------------------------------------------------------
        idCoachs2coachsSelected(idCoach)

        Cette fonction converti une liste d'ID de coachs, en un tableau dont le nombre 
        de case est égal au nombre de coachs à proximité de l'utilisateur. chaque case
        contient true pour un coach dont l'ID est dans la liste idCoach, et false sinon.

        Entrées : 
        ---------
        coachsSelected : le tableau des coachs sélectionnés, initialisé avec false à toutes les cases. 
        idCoachs        : un tableau avec des ID de coachs

        Sorties : 
        ---------
        coachsSelected : un tableau avec true pour les coachs sélectionnés et false sinon. 

        Notes : 
        -------

        -----------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "idCoachs2coachsSelected()");  
        
        // recherche des index des coachs dont les id sont dans le tableau idCoachs
        //--------------------------------------------------------------------------
        indexCoachs = idCoach2numCoach(idCoachs);

        // boucle sur le nombre de coachs pour mettre à jour la variable des coachs sélectionnés.
        for(numI = 0; numI < indexCoachs.length; numI++){
            if(indexCoachs[numI] != null){ // si pas de souci avec l'ID du coachs
                coachsSelected[indexCoachs[numI]] = true; // on sélectionne le coachs
            } else { // sinon
                errorDebug("idCoachs2coachsSelected() : un problème avec l'ID un coach : ID = "+ idCoachs[numI] );
                if((typeof(option) != "undefined") && option.debug){
                    console.log(idCoachs);
                    console.log(indexCoachs);
                }
            }
        } // for(numI)

        // renvoi du résultat
        return coachsSelected;
    } // function idCoachs2coachsSelected()

    //================================================================================================================
    // tableau des coachs sélectionnés, à partir d'une liste d'ID de coachs
    //================================================================================================================
    function numCoachs2coachsSelected(coachsAlreadySelected, numCoachs){

        /*-----------------------------------------------------------------------------
        function numCoach2coachsSelected(coachsAlreadySelected, numCoachs)

        Cette fonction converti une liste de numéros de coachs, en un tableau dont le nombre 
        de case est égal au nombre de coachs à proximité de l'utilisateur. chaque case
        contient true pour un coach dont l'ID est dans la liste des numéros de coachs, et false sinon.

        Entrées : 
        ---------
        coachsAlreadySelected : le tableau des coachs déjà sélectionnés. 
        numCoachs             : un tableau avec les numéros de coachs sélectionnés

        Sorties : 
        ---------
        coachsSelected : un tableau avec true pour les coachs sélectionnés et false sinon. 

        Notes : 
        -------
        fonction validée le 2019 06 26
        -----------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "numCoach2coachsSelected()");

        // init
        // IMPORTANT : le .slice() est là pour que le tableau soit copié. sans ça le tableau n'est pas copié et 
        //             on obtient 2 pointeurs sur le même tableau !!!
        var coachsSelected = coachsAlreadySelected.slice(); 

        if(typeof(numCoachs) === 'number'){ // si c'est un nombre seul
            numCoachs = [numCoachs]; // on le transforme en tableau
        }
        
        // boucle sur le nombre de coachs pour mettre à jour la variable des coachs sélectionnés.
        for(numC = 0; numC < numCoachs.length; numC++){
            coachsSelected[numCoachs[numC]] = true; // on sélectionne le coachs
        } // for(numC)

        // renvoi du résultat
        return coachsSelected;
    } // function numCoach2coachsSelected()

    //================================================================================================================
    // numeros d'offres -> numéros de coachs
    //================================================================================================================
    function numOffre2numCoach(numOffre){

        /*-----------------------------------------------------------------------------
        function numOffre2numCoach(numOffre)

        Cette fonction converti une liste de numéros d'offres de coaching, en une liste 
        de numéros de coachs dans la variable globale coachs.liste .

        Entrées : 
        ---------
        numOffre  : un numéro d'offre, ou un tableau avec des numéros d'offres de coaching.
                   NB: chaque numéro peut être un nombre, ou une chaîne de caractère avec ce nombre.

        Sorties : 
        ---------
        numCoach : un numéro ou un tableau avec les numéros des coachs correspondants.
                   NB: le numéro peut être 0 (1er numéro). 

        Notes : 
        -------
        fonction validée le 2019 06 26
        -----------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "numOffre2numCoach()");

        // init
        var index; // variable temporaire

        // fonctionnement différent si la variable d'entrée est un tableau de nombre ou juet un nombre
        if(typeof(numOffre) === 'object'){  // si numOffre est un tableau
            var idCoachs = []; // tableau qui va recevoir les ID des coachs

            // boucle sur le nombre d'offre de la liste
            for(numO = 0; numO < numOffre.length; numO++){
                idCoachs.push(coachs.offres[numO].coach_id); // id du coach correspondant à l'offre
            } // for(num)

        } else {  // si numOffre est une valeur numérique
            // id du coach correspondant à l'offre
            var idCoachs = coachs.offres[parseInt(numOffre)].coach_id;
        } // if(typeof(numOffre) - else

        // conversion des id de coachs en numéros de coachs
        numCoach = idCoach2numCoach(idCoachs);

        // renvoi des résultats
        return numCoach;        
    } // function numOffre2numCoach()

    //================================================================================================================
    // numeros d'offres pour un coach donné
    //================================================================================================================
    function numCoach2numOffres(numCoach){

        /*-----------------------------------------------------------------------------
        function numCoach2numOffre(numCoach)

        Cette fonction fournit les numéros d'offres correspondant à un coach donné.

        Entrées : 
        ---------
        numCoach : un numéro de coach.
        
        Sorties : 
        ---------
        numOffres  : un tableau avec les numéros d'offres de coaching correspondant.

        Notes : 
        -------
        fonction validée le 2019 07 10
        -----------------------------------------------------------------------------*/

        infoDebug("log", "numCoach2numOffres()");

        var numOffres = [];

        // boucle for sur l'ensemble des offres
        for(numOffre = 0; numOffre < coachs.offres.length; numOffre++){
            // si l'offre appartient au coach
            if(idCoach2numCoach(coachs.offres[numOffre].coach_id) == numCoach){
                // alors on ajoute l'offre dans le tableau des offres
                numOffres.push(numOffre);
            } // if
        } // for

        // renvoie le tableau des offres correspondant au coach
        return numOffres;
    } // function numCoach2numOffres()

//###############################################################
// AJAX
//###############################################################

    //===========================================================================
    // fonction pour demander au serveur la liste des coachs à proximité
    //===========================================================================
    function demandeCoachsProches() {

        /*---------------------------------------------------------------------------------------------------
        récupération des infos sur les coachs
        comme on ne sait pas quand on va recevoir les données (ça peut mettre du temps), il faut une
        gestion asynchrone. C'est le rôle de la fonction receptionListeCoachs();            

        Entrées : 
        ---------
        Aucune entrée : la localisation envoyée au serveur est directement prise dans les variables globales
        loc.userLoc.lat et loc.userLoc.lng

        Sortie : 
        --------
        Aucune, car c'est la fonction receptionListeCoachs(), appelée en callback, qui reçoit les infos sur les coachs
        -------------------------------------------------------------------------------------------------------*/

        //debug
        infoDebug("log", "demandeCoachsProches()");

        // affichage de la modal indiquant qu'on a bien pris en compte une action utilisateur,
        // Elle affiche un spinner faisant patienter l'utilisateur et sera fermé lorsque les données
        // seront reçues.
        $('#modal-reception-donnees').modal('show'); // affichage de la modal

        // envoi de la demande au serveur
        sendToServer(ajaxCall.root, ajaxCall.coachsProxi, {lat:loc.userLoc.lat, lng:loc.userLoc.lng}, receptionListeCoachs);
    } // function demandeCoachsProches()

    //===========================================================================
    // réception des donées AJAX avec la liste des coachs à proximité
    //===========================================================================
    // Il faut que ce soit une fonction, afin de pouvoir être appelée en asynchrone, uniquement
    // lorsque les données auront bien été reçues.    
    function receptionListeCoachs(donneesStr){
      
        // debug
        infoDebug("log","receptionListeCoachs()");
        infoDebug("chaineJson", "receptionListeCoachs() : chaine JSON, méthode POST : "+donneesStr);

        // transformation de la chaîne JSON en un objet Javascript
        var data = JSON.parse(donneesStr); 

        // masquage de la modal indiquant qu'on a bien pris en compte une action utilisateur.
        // On n'en a plus besoin maintenant que les données ont été reçues.
        // NB: on a besoin du setTimeout car sans ça, la fonction pour masque la modal
        //     est appelée avant la fonction pour l'afficher. Le résultat est qu'elle n'est jamais
        //     masquée. Pour être sûr, on ajoute donc un petit setTimeout.
        //     après essais, le mini est 600 ms. On prend une petite marge là dessus, c'est pourquoi
        //     on met 700 ms.
        // en plus on utilise le $(function(){}), ce qui permet d'attendre que la page ait été mis à jour avant de commencer le décompte.
        $(function(){
            setTimeout(function(){
                $('#modal-reception-donnees').modal('hide'); // suppression de la Modal
                $('#TLC-spinner-champs-recherche').hide(); // affichage du spinner à côté du champ de recherche d'un coach

            }, 700);
        });
        

        // vérification des données
        if(data.statusOK){
        // debug
        infoDebug("info","receptionListeCoachs() : données reçues = OK");

        // supprime l'ancienne liste des coachs et les markers correspondants
        supprimeCoachs();  


        // récupération des données sur les coachs à proximité
        //====================================================
        coachs.liste        = data.infosCoachs; // récupération des données principales sur les coachs
        coachs.creneaux     = data.creneaux;    // données sur les créneaux horaires des coachs
        coachs.offres       = data.offres;      // données sur les différentes offres des coachs
        if(typeof(data.nbreMaxReponses) != "undefined"){
            // valeur du nombre max de réponses. N'est fourni que pour la recherche d'un coach
            recherche.nbreMaxCoachs = data.nbreMaxReponses;
        }

        // traitement des données
        //=======================
        // création du tableau permettant la conversion rapide d'un id de coach en un numéro (<=> index)
        // de coach et inversement
        coachs.id = [];
        for(index = 0; index < coachs.liste.length; index++){
            coachs.id.push(parseInt(coachs.liste[index].coach_id)); // ajout de l'iID (converti en integer), dans la liste
        } // for(index)

        // création du tableau permettant la conversion rapide d'un id d'une offre' en un numéro (<=> index)
        // d'offre et inversement
        // NB: C'est un peu limite comme façon de programmer car "coachs.offres" est un tableau, et on ajoute un
        //     champ "id" comme si c'était un objet. Le javascript est permissif et permet de le faire, mais 
        //     peut être qu'un jour il sera moins permissif.
        coachs.offres.id = [];
        for(index = 0; index < coachs.offres.length; index++){
            coachs.offres.id.push(parseInt(coachs.offres[index].ofc_id)); // ajout de l'iID (converti en integer), dans la liste
        } // for(index)

        // calcul de la distance entre chaque coach et l'utilisateur
        var diffLat, diffLng;
        coachs.dist = []; // init du tableau des distances entre coachs et utilisateur
        for(numCoach = 0; numCoach < coachs.liste.length; numCoach++){
            // (°) différence de latitude.
            diffLat = parseFloat(coachs.liste[numCoach].coach_geocod_lat) - loc.userLoc.lat; 
            // (°) différence de longitude, avec prise en compte de la latitude, pour que les distances soient comparables
            diffLng = (parseFloat(coachs.liste[numCoach].coach_geocod_lng) - loc.userLoc.lng)*Math.cos(loc.userLoc.lat * Math.PI / 180); 
            
            coachs.dist.push(Math.sqrt(diffLat*diffLat + diffLng*diffLng)*40000/360); // critère proportionnel à la distance
        } // for(numCoach...)
        
        

        // actions avec la liste des coachs récupérés
        //===========================================
        majCarto()                      // mise à jour de la carte, en la recentrant sur l'utilisateur
        .then(affMarqueursCoachs())     // affichage des markers des coachs
        .then(addEncartCoachs())        // affichage des encarts sur les coachs
        .then(filtrage())               // puis application du filtre
        .then(majBadges())              // puis mise à jour des valeurs des badges
        .then(trieDistances())          // puis trie les encarts selon la proximité décroissante avec l'utilisateur.
        .then(majStatClic())            // ajoute des évènement dédiés sur tous les éléments pour lesquels on veut des stats de clic.
        .catch(function(erreur){        // gestion des erreurs
            errorDebug("receptionListeCoachs() : "+erreur); 
        }); // affichage des marqueurs des coachs 

        // Mise à jour du texte de la zone d'accueil
        // NB: Cette action peut être asynchrone par rapport  aux action précédentes.
        //     On ne l'inclue donc pas dans la chaîne de promesses.
        majZoneAccueil();       

        // envoi des stats de position utilisateur, ou position recherchée par l'utilisateur.
        // NB: cette action peut être asynchrone.
        // NB: statPos() est appelée par receptionListeCoachs() plutôt que par NouvelleLoc() afin 
        //     d'attendre que la liste des coachs soit reçue, et qu'il soit possible d'enregistrer
        //     le nombre de coachs à proximité dans les stats.
        // mais comme cette receptionListeCoachs() peut recevoir des données sur changement d'adresse, ou pour la 
        // recherche d'un coach. on n'enregistre les stats de position que lors d'un changement d'adresse.
        // Et pour la localisation initiale (localisation()), recherche.lastSearchType = "adresse", donc
        // ça fonctionne.
        if(recherche.lastSearchType == "adresse"){statPos();} // on appelle statPos

        } // if(data.statusOK)
        else{
        // debug
        warnDebug("receptionListeCoachs() : données reçues = NOK");
        } // if(data.statusOK) - else
    } // function receptionListeCoachs(data)


    //===========================================================================
    // fonction pour demander au serveur de rechercher un coach particulier
    //===========================================================================
    function demandeRechercheCoach(searchString) {

        /*---------------------------------------------------------------------------------------------------
        function demandeRechercheCoach()

        Demande au serveur de chercher si un coach est dans la base de données

        comme on ne sait pas quand on va recevoir les données (ça peut mettre du temps), il faut une
        gestion asynchrone. C'est le rôle de la fonction receptionRechercheCoach();         

        Entrées : 
        ---------
        searchString : la chaîne de caractère avec le nom à chercher

        Sortie : 
        --------
        Aucune, car c'est la fonction receptionRechercheCoach(), appelée en callback, qui reçoit les infos sur les coachs
        -------------------------------------------------------------------------------------------------------*/

        //debug
        infoDebug("log", "demandeRechercheCoach()");

        // affichage de la modal indiquant qu'on a bien pris en compte une action utilisateur,
        // Elle affiche un spinner faisant patienter l'utilisateur et sera fermé lorsque les données
        // seront reçues.
        $('#modal-reception-donnees').modal('show'); // affichage de la modal
        $('#TLC-spinner-champs-recherche').show(); // affichage du spinner à côté du champ de recherche d'un coach
        
        // envoie des données au serveur
        sendToServer(ajaxCall.root, ajaxCall.coachSearch, 
                     {searchString:searchString}, receptionListeCoachs);
    } // function demandeRechercheCoach()

//###############################################################
// carto
//###############################################################


    //===============================================================
    // initMap
    //===============================================================
    //Cette fonction est appelée par googleAPI dès qu'il a fini de vérifier la clef
    function initMap() {
        
        //debug
        infoDebug("log", "initMap()");  
        
        // boucle pour attendre la fin de l'initialisation avant d'exécuter initMap
        var attente = 0;
        while(!fonctionnement.initOK){
            // debug
            if(attente == 0){
                infoDebug("info", "initMap(): Attente de la fin de l'initialisation")
            }
            attente = 1;
        } // while
        
        
        // chargement de l'icône pour le marker de la position utilisateur sur la carte.
        // NB: il est nécessaire d'attendre que google maps soit lancé car on crée un objet
        // défini dans l'API google maps.
        carto.userMarkerIcon = {
            url: chemin.racine + chemin.icones + nomFic.svg.markerUserPos,
            scaledSize: new google.maps.Size(30, 48),
        };

        // vérifie la présence de la carte. Si elle n'est pas encore affichée,
        // alors il faut le faire.
        if(!carto.carteAffichee){ // si 1erè loc à afficher
            // alors on ajoute la carte sur la page car elle n'y était pas encore.
            addMap();
            // mise à jour de l'état de la carte
            carto.carteAffichee = true;
            // ajout du marker avec la position de l'utilisateur.
            addUserMarker();        
        } // si 1ère loc à afficher

        // localisation de l'utilisateur
        localisation();

        // préparation de l'infoWindow, qui permet d'afficher des infos pour un coach donné
        carto.infoWindow = new google.maps.InfoWindow({
            content: document.getElementById('info-content')
        });
    } // function initMap

    //===============================================================
    // ajout de la carte
    //===============================================================
    function addMap(){
        /*-----------------------------------------------------------------
            function addMap()

            Cette fonction ajoute la carte sur la page html

        -----------------------------------------------------------------*/

        // debug()
        infoDebug("log", "addMap()");

        // initialisation de la carte, centrée à la position par défaut
        carto.map = new google.maps.Map(document.getElementById('TLC-map'), {
            center: loc.userLoc, 
            zoom: carto.niveauZoomParDefaut
        });
    } // function addMap()

    //===============================================================
    // fonction de mise à jour de la carte
    //==============================================
    function majCarto() {
        return new Promise((resolve, reject) =>{    // promesse, pour gérer le fonctionnement asynchrone
        
            //debug
            infoDebug("log", "majCarto()");

            // Centrage de la carte pour être entre les 2 extrêmes en longitude et en latitude
            //================================================================================
            // init
            var lat, lng;
            var latMin = loc.userLoc.lat;
            var latMax = loc.userLoc.lat;
            var lngMin = loc.userLoc.lng;
            var lngMax = loc.userLoc.lng;
            
            // boucle sur les positions des coachs pour repérer les extrêmes
            for(var numCoach = 0; numCoach < coachs.liste.length; numCoach++) {
                lat = parseFloat(coachs.liste[numCoach].coach_geocod_lat);
                lng = parseFloat(coachs.liste[numCoach].coach_geocod_lng);
                if(lat > latMax){latMax = lat} else if(lat < latMin){latMin = lat}
                if(lng > lngMax){lngMax = lng} else if(lng < lngMin){lngMin = lng}
            } // for(numCoach)

            // calcul du centre entre les extrêmes
            lat = (latMin + latMax)/2;
            lng = (lngMin + lngMax)/2;

            // calcul du centre et des bornes de la carte
            var locCentre = new google.maps.LatLng(lat,    lng   ); // centre de la carte
            var sw        = new google.maps.LatLng(latMin, lngMin); // coordonnées du coin sud-Ouest dela zone encadrant les coachs
            var ne        = new google.maps.LatLng(latMax, lngMax); // coordonnées du coin Nord-Est dela zone encadrant les coachs
            var bounds    = new google.maps.LatLngBounds(sw, ne);   // bornes pour la carte

            // distances en jeu (pour gérer l'échelle de la carte)
            // (km) distance selon la latitude.
            var diffLat = (latMax - latMin)*40000/360; // dist(°)*40000Km/360°
            // (Km) différence de longitude, avec prise en compte de la latitude, pour que les distances soient comparables
            var diffLng = (lngMax-lngMin)*Math.cos(lat * Math.PI / 180)*40000/360; 
            // calcul de la distance de la diagonale de la zone encadrant les coachs et l'utilisateur.
            var dist = Math.sqrt(diffLat * diffLat + diffLng * diffLng); // distance en Km

            // MAJ de la carte en la centrant entre les extrêmes
            if((coachs.liste.length == 0) || (dist < 30) ){ // si aucun coach ou si tout est dans une zone de moins de 30 Km de diagonale
                carto.map.setCenter(locCentre);               // centrage de la carte
                carto.map.setZoom(carto.niveauZoomParDefaut); // niveau de zoom par défaut
            } else { // s'il y a des coachs
                carto.map.fitBounds(bounds);
            }
            
            //debug
            infoDebug("info", "majCarto() : loc = {lat = "+loc.userLoc.lat+"; lng = "+loc.userLoc.lng+"}");

            // pour terminer la promesse
            resolve("majcarto() = OK");
        }); // promesse
    } // function majCarto()

    //===============================================================
    // définition des symbols des markers, correspondant aux différentes
    // catégories de coachs
    //==================================================================
    function initMarkerSymbols() {

        /*
            (2019 06 27) cette fonction n'est plus utilisée car on utilise maintenant le symbole
            standard et la couleur standard pour tous les coachs.
            On la garde cependant car elle pourra servir plus tard à azjouter d'autres choses sur 
            la carte, comme des évènements ou des centres de formation de coaching.
        */
        
        /* NB: pour différencier les symbols, on peut jouer sur 
            - le fait que le symbol soit plein ou creux
            - la couleur de remplissage
            - la transparence de la couleur de remplissage
            - la couleur du trait
        */
        
        //debug
        infoDebug("log", "initMarkersSymbols()");

        // objets Symbols de base, sur lesquels on va s'appuyer pour créer les symbols pour les différentes catégories de coachs
        //----------------------------------------------------------------------------------------------------------------------
        var positionSymbolCreux = {
            path: 'M0,20a2,2,0,0,1-1.94-1.51c-.8-3.19-2.71-5.3-4.74-7.54C-9.3,8.07,-12,5.09,-12,0a12,12,0,0,1,24,0c0,5.09-2.7,8.07-5.32,10.95-2,2.23-3.95,4.35-4.74,7.54A2,2,0,0,1,0,20ZM0,-8a8,8,0,0,0-8,8c0,3.55,1.89,5.63,4.28,8.26A31.57,31.57,0,0,1,0,12.95a31.57,31.57,0,0,1,3.72-4.69C6.11,5.63,8,3.55,8,0A8,8,0,0,0,0,-8Z',
            anchor: new google.maps.Point(0, 20),
            fillColor: 'green',
            fillOpacity: 0.8,
            scale: 1,
            strokeColor: 'black', // couleur des lignes
            strokeWeight: 0.25
        }; //  positionSymbolCreux
            
        var positionSymbolPlein = {
            path: 'M0,20a2,2,0,0,1-1.94-1.51c-.8-3.19-2.71-5.3-4.74-7.54C-9.3,8.07,-12,5.09,-12,0a12,12,0,0,1,24,0c0,5.09-2.7,8.07-5.32,10.95-2,2.23-3.95,4.35-4.74,7.54A2,2,0,0,1,0,20Z',
            anchor: new google.maps.Point(0, 20),
            fillColor: 'green',
            fillOpacity: 0.7,
            scale: 1,
            strokeColor: 'black', // couleur des lignes
            strokeWeight: 0.25
        }; // positionSymbolPlein
        
        
        // définition des symbols des markers pour les différentes catégories de coachs
        //-----------------------------------------------------------------------------
        // NB: la variable carto.markerSymbols; a été définie en globale, pour être accessible aux
        //     différentes fonctions.
        var num;
        for(num=0; num<carto.listeCouleurs .length; num++) { addMarkerSymbol(num); }
        
        // fonction permettant de créer un nouveau symbol pour les markers
        function addMarkerSymbol(num) {
            var nouveauPositionSymbol = Object.create(positionSymbolCreux); // initialisation du symbol du marker de position
            nouveauPositionSymbol.fillColor = carto.listeCouleurs [num];            // modification de la couleur de remplissage
            carto.markerSymbols.push(nouveauPositionSymbol);                        // ajoute le nouveau symbole de position dans le tableau dédié
        } // function addMarkerSymbol()
    } // function initMarkerSymbols() 

    //===============================================================
    // fonction qui supprime les coachs,
    // sur la carte, dans la liste des marqueurs, et dans la liste des coachs
    //==================================================================
    function supprimeCoachs() {

        //debug
        infoDebug("log", "supprimeCoachs()");

        while(carto.listeMarkers.length > 0){
            // supprime les écouteurs d'évènements associés à ce marker
            google.maps.event.clearListeners(carto.listeMarkers[0], 'click'); 
            if(carto.listeMarkers[0]){
                carto.listeMarkers[0].setMap(null); // 1er marker effacé de la carte
            }
            carto.listeMarkers[0] = null;           // supprime le marker lui-même
            carto.listeMarkers.shift();             // supprime le 1er marker de la liste
            coachs.liste.shift();                   // supprime le 1er coach de la liste
        } // boucle while

        // supprime les encarts des coachs
        $(function() {   // partie en jQuery

            // supprime les encarts des coachs
            $("#TLC-encarts-coachs").empty();

        }); // $(function()  // partie en jQuery    
    } // function supprimeCoachs()

    //===============================================================
    // pour ajouter le marker de la position utilisateur
    //===============================================================
    function addUserMarker(){
        
        //debug
        infoDebug("log", "addUserMarker()");
        
        // préparation du titre affiché lors du survol
        var titreSurvol;
        if(loc.adresse.length > 0){
            titreSurvol = loc.adresse;
        } else {
            titreSurvol = "votre position";
        }
        
        // affichage du marker de l'utilisateur sur la carte
        // NB: carto.userMarkerIcon est déclaré dans les variables globales,
        //     et initialisé dans initMap()
        carto.userMarker = new google.maps.Marker({
            position: {
                lat: parseFloat(loc.userLoc.lat), 
                lng: parseFloat(loc.userLoc.lng)
            },  // la position du coach
            animation: google.maps.Animation.DROP,      // pour le faire arriver en tombant
            icon: carto.userMarkerIcon,                 // Marker dédié pour l'utilisateur
            title: titreSurvol,                         // pour afficher le nom du coach au survol
            map: carto.map                                  // pointeur vers la carte sur laquelle afficher le marker
        });
    } // function addUserMarker()
    
    //===============================================================
    // supprime le marker de la position utilisateur
    //===============================================================
    function supprimeUserMarker(){

    
        // vérifie s'il y a déjà un marker pour l'utilisateur
        if(carto.userMarker != null){
            // supprime le marker de la carte
            carto.userMarker.setMap(null); 
            
            // supprime le marker lui même
            carto.userMarker  = null;
        } // if(carto.userMarker...)
    } // function supprimeUserMarker()

    //===============================================================
    // fonction qui affiche sur la carte, les marques correpsondants
    // aux coachs sélectionnés
    //==================================================================
    function affMarqueursCoachs() {
        return new Promise((resolve, reject) =>{    // promesse, pour gérer le fonctionnement asynchrone

            //debug
            infoDebug("log", "affMarqueursCoachs()");

            for(var num = 0; num<coachs.liste.length; num++){

                // marqueur placé la la bonne position sur la carte
                carto.listeMarkers.push(new google.maps.Marker({
                    position: {
                        lat: parseFloat(coachs.liste[num].coach_geocod_lat), 
                        lng: parseFloat(coachs.liste[num].coach_geocod_lng)
                    },  // la position du coach
                    animation: google.maps.Animation.DROP,                              // pour le faire arriver en tombant
                    //icon: carto.markerSymbols[catCoach],  // supprimé car obsolète    // marker correspondant à la catégorie du coach
                    title: coachs.liste[num].coach_nom,                                 // pour afficher le nom du coach au survol
                    map: carto.map,                                                         // pointeur vers la carte sur laquelle afficher le marker
                    num: num                                                            // info qu'on affiche en plus, et qui n'est pas utilisé par l'API
                }));
                
                // ajout d'un gestionnaire d'évènement au clic sur le marker du coach
                addEventMarkersCoachs(num); 

            } // boucle for
        // on indique que la promesse est remplie (gestion de fonctions asynchrones)
        resolve("affMarqueursCoachs() : promesse terminée"); 
        }); // promise
    } // function affMarqueursCoachs()

    //===============================================================
    // Gestionnaire d'évènement appelé au clic sur le marker d'un coach
    //===============================================================
    function clickSurMarkerCoach(e) {
        
        //debug
        infoDebug("log", "clickSurMarkerCoach()");

        var marker = this;              // récupération d'un pointeur vers le marker qui a été cliqué
        carto.infoWindow.open(carto.map, marker);   // affichage de l'carto.infoWindow au niveau du marker
        remplitInfoWindow(marker.num);  // remplit l'carto.infoWindow avec les bonnes infos

        // Gestion des stats
        // Comme on n'a pas accès à l'élément html des markers des coachs,
        // on appelle directement statClicReceiver. 
        // Ce n'est pas la manière standard de faire mais on fait une exception.
        var priorite = 2;
        var nom = "markerCoach";
        var param1 = null;
        var param2 = null;
        var lienSortie = false;
        statClicReceiver(priorite, nom, param1, param2, lienSortie);
        
        
        // debug
        infoDebug("log", "clickSurMarkerCoach()");
        infoDebug("info", marker.title);
        infoDebug("info", "numero du coach dans la liste = "+marker.num);
    } // function clickSurMarkerCoach()

    //===============================================================
    // Gère le contenu de l'carto.infoWindow
    //===============================================================
    function remplitInfoWindow(numCoach){
    
        /*-----------------------------------------------------------------------
            function remplitInfoWindow()
        
            Cette fonction permet de remplir l'infoWindow qui est affiché lorsqu'
            on clique sur le marker d'un coach.
            
            Entrées : 
            ---------
            numCoach : Le numéro du coach dans la liste des coachs à proximité.
                       Attention : la liste commence au n° 0.
        -----------------------------------------------------------------------*/
    
        // création du bloc html permettant d'ajouter l'encart d'un coach.
        var blocHtml = createEncartCoach(numCoach);
    
        $(function(){ // partie en jQuery
        
            // sélection de l'infoWindow et ajout de l'encart.
            // NB: il faudra peut être vider l'encart précédent.
            $("#info-content").empty().append(blocHtml);
            
            // mise à jour des éléments à suivre pour les stats
            majStatClic();
            
        }); // partie en jQuery
    } // function remplitInfoWindow()
    
    //===============================================================
    // cache le marker d'un coach
    //===============================================================
    function hideMarkerCoach(numCoach){

        //debug
        infoDebug("log", "hideMarkerCoach()");

        // vérifications que le numéro du coach est bien un numéro valide
        if(numCoach <= coachs.liste.length){
            carto.listeMarkers[numCoach].setMap(null);  // on cache le marker
        }
    } //function hideMarkerCoach

    //===============================================================
    // affiche le marker d'un coach
    //===============================================================
    function showMarkerCoach(numCoach){

        //debug
        infoDebug("log", "showMarkerCoach()");

        // vérifications que le numéro du coach est bien un numéro valide
        if(numCoach <= coachs.liste.length){
            carto.listeMarkers[numCoach].setMap(carto.map);  // on cache le marker
        }
    } //function showMarkerCoach

    //================================================================
    // animation d'un marker d'un coach
    //================================================================
    function animateMarkerCoach(){
    
        /*-------------------------------------------------------------------------
            animateMarkerCoach()

            Cette fonction est appelée par le bouton de repérage d'un coach dans
            l'encart avec les détails du coach.
            
            Elle supprime l'animation de chaque marker de coach, avant d'ajouter
            une animation qui fait bondir le marker d'un coach, sur le coach spécifié.

            NB: le numéro du coach dont on doit faire bondir le marker n'est pas passé
            dans les paramètres de la fonction, mais est trouvé dans les données
            associées au bouton qui a appelé la fonction animateMarkerCoach().
        -------------------------------------------------------------------------*/


        // debug
        infoDebug("log", "animateMarkerCoach() : numCoach = "+$(this).data('numCoach'));
    
        // extraction du numéro de coach
        var numCoach = $(this).data('numCoach');

        // supprime les animations des markers des coachs
        supprimeAnimationMarkersCoachs();

        // appel à la fonction pour afficher la carte.
        // Pour le cas où elle aurait été masquée
        showMap();

        // centrage de la carte sur le coach à localiser
        carto.map.setCenter({lat : parseFloat(coachs.liste[numCoach].coach_geocod_lat), lng : parseFloat(coachs.liste[numCoach].coach_geocod_lng)});

        // animation du marker correspondant à ce coach
        carto.listeMarkers[numCoach].setAnimation(google.maps.Animation.BOUNCE);
    } // function animateMarkerCoach()

    //================================================================
    // suppression des animations des markers des coachs
    //================================================================
    function supprimeAnimationMarkersCoachs(){

        /*-------------------------------------------------------------------------
        supprimeAnimationMarkersCoachs()

        Cette fonction supprime l'animation de chaque marker de coach sur la carte
        -------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "supprimeAnimationMarkersCoachs()");


        // boucle for pour supprimer les animations précédentes
        for(num=0; num < carto.listeMarkers.length; num++){
            carto.listeMarkers[num].setAnimation(null);
        } // for()
    } // function supprimeAnimationMarkersCoachs()

    //================================================================
    // Cache la carte
    //================================================================
    function hideMap(){

        // debug
        infoDebug("log", "hideMap()");

        $(function(){ // partie en jQuery

            // intervertit les 2 icônes pour le bouton de la carte
            $("#TLC-button-carte-off").hide();
            $("#TLC-button-carte-on").show();

            //cache le bouton pour centrer la carte sur l'utilisateur
            $("#TLC-button-carte-center-user").hide();

            // cache la carte
            $("#TLC-zone-carto").hide();
        }); // partie en jQuery
    } // function hideMap()

    //================================================================
    // affiche la carte
    //================================================================
    function showMap(){

        // debug
        infoDebug("log", "showMap()");

        $(function(){ // partie en jQuery

            // intervertit les 2 icônes pour le bouton de la carte
            $("#TLC-button-carte-on").hide();
            $("#TLC-button-carte-off").show();

            // affiche le bouton pour centrer la carte sur l'utilisateur
            $("#TLC-button-carte-center-user").show();
            
            // affiche la carte
            $("#TLC-zone-carto").show();
        }); // partie en jQuery
    } // function showMap()

    //================================================================
    // centrer la carte sur l'utilisateur
    //================================================================
    function centreCarteSurUser(){
    
        /*-------------------------------------------------------------------------
            centreCarteSurUser()

            Cette fonction est appelée par le bouton associé.
            
            Elle supprime l'animation de chaque marker de coach, puis
            centre la carte sur l'utilisateur.

        -------------------------------------------------------------------------*/


        // debug
        infoDebug("log", "centreCarteSurUser()");
    
        // supprime les animations des markers des coachs
        supprimeAnimationMarkersCoachs();

        // appel à la fonction pour afficher la carte.
        // Pour le cas où elle aurait été masquée
        showMap();

        // centrage de la carte sur l'utilisateur
        carto.map.setCenter(loc.userLoc);
    } // function centreCarteSurUser()

//###############################################################
// localisation
//###############################################################

    //===============================================================
    // gestion de la localisation
    //===============================================================
    function localisation(){
        /*-----------------------------------------------------------
            function localiastion()

            Cette fonction permet de coordonner toutes les fonctions de localisation
            dans l'ordre qui va bien.

            Elle appelle NouvelleLoc() si une localisation est trouvée, et appelle
            également les fonctions permettant de mettre à jour le message de la zone
            d'accueil.
        ------------------------------------------------------------*/

        infoDebug("log", "localisation()");

        // appel aux fonctions de localisation via l'adresse IP
        // NB: les différentes fonctions de la chaîne échangent un paramètre statusGeoLocOK,
        //     qui se met à true quand une localisation est réalisée avec succès.
        //     quand c'est le cas, les fonctions suivantes sont shuntées (bout de code pour
        //     faire ça à l'intérieur de chacune des fonctions).
        locForcee()                     // vérifie s'il y a une localisation forcée par le serveur
        .then(navGeoLoc)                // localisation en demandant au navigateur
        .then(ipGeoLoc)                 // localisation à partir de l'adresse IP
        .then(cookieGeoLoc)             // loc à partir d'un cookie avec derniere loc utilisée sur ce site
        .then(function(statusGeoLocOK){
            if(statusGeoLocOK){
                // gestion de la nouvelle localisation
                nouvelleLoc();
            } // if(statusIpGeoLocOK)
            else{
                // gestion du cas où la localisation est impossible
                noLoc();
            } // if(statusGeoLocOK) - else
        }) // .then
        .catch(function(errorMsg){
            infoDebug("info", "localisation : erreur avec ipGeoLoc() => " + errorMsg);
        }); // .catch
    } // function localisation()

    //===============================================================
    // fonction appelée quand localisation impossible
    //===============================================================
    function noLoc(){
        /*-----------------------------------------------------------
            function noLoc()

            Cette fonction est appelée lorsque tous les éssais de localisation
            ont échoué.
        -----------------------------------------------------------*/

        // debug
        infoDebug("log", "noLOc()");

        // maj du type de localisation
        loc.userLoc.typeLoc = 6; // <=> type de localisation = pas de localisation <=> loc par défaut

        // mise à jour du message de la zone d'accueil
        msgAccueilNoLoc();

        // utilisation de la localisation par défaut
        nouvelleLoc();
    } // function noLoc()

    //===============================================================
    // fonction de géolocalisation de l'utilisateur avec adresse IP
    //===============================================================
    function ipGeoLoc(locDejaOK) {

        /*-----------------------------------------------------------
            function ipGeoLoc()

            Cette fonction gère la localisation via l'adresse ip.
            Elle renvoie une promesse pour faciliter la gestion du fonctionnement
            asynchrone.

            Entrée : 
            --------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                Une promesse
                    => si resolve : 
                        - statusOK : booléen qui renvoie le status de la localisation
                    => si reject : 
                        -errorMsg  : une chaîne de caractère avec le message d'erreur
        -----------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{
            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "ipGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }

            //debug
            infoDebug("log", "ipGeoLoc()");

            // appel à API de géloc de google
            // NB: toutes ces fonctions se passe le paramètre locDejaOK, qui vaut false s'il
            //     n'y a pas encore de loc et true si la localisation est OK.
            gcGeoloc()
            .then(ipapiGeoLoc)
            .then(googGeoLoc)
            .then(resolve)  // indique que promesse résolue ET passe le paramètre de status de localisation
            .catch(reject);  // indique que promesse rejetée ET passe le message d'erreur

            // setTimeout pour gérer le cas où les fonctions appelées ne répondent pas
            // NB: la durée du timeout est de 2 fois la durée standard car ipGeoLoc()
            // va in fine appeler successivement 2 fonctions qui ont chacune une durée standard
            // pourle timeout
            setTimeout(function(){
                infoDebug("info", "ipGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout*3); // setTimeout

        }); // promise              
    } // function ipGeoLoc()

    //===============================================================
    // fonction de géolocalisation IP avec gc
    //===============================================================
    function gcGeoloc(locDejaOK){
        /*
            gcGeoLoc()

            Cette fonction fait appel à l'API de gc via l'adresse IP.
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        */

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "gcGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }
            //debug
            infoDebug("log", "gcGeoloc()");

            // préparation de la requête
            var url = "https://europe-west1-projet1-1553783593230.cloudfunctions.net/gcGL"; // URL
            var request = new XMLHttpRequest();     // nouvel objet XHR
            request.responseType = 'json';          // type de réponse qu'on attend
            request.open('POST', url);              // spécification de la méthode et de l'URL à laquelle récupérer les données
            
            // envoi de la requête
            request.send();
            
            // setTimeout pour gérer le cas où l'API ne répond pas
            var to = setTimeout(function(){
                infoDebug("info", "gcGeoloc() => timeout");
                //  au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout*2); // setTimeout


            // gestionnaire d'évènement pour la réception des données.
            // Il faut en effet attendre que les données soient reçues
            request.onload = function() {

                if(request.response){ // on vérifie qu'il y a bien une réponse

                    // récupère les infos
                    var coord   = request.response.cityLatLong.split(',');

                    // vérification qu'on reçoit bien des coordonnées
                    if(coord.length >1){

                        // réinitialisation des données de position utilisateur
                        initUserLoc();
                        
                        // récupération des données
                        loc.userLoc.lat                 = coord[0]                  // latitude
                        loc.userLoc.lng                 = coord[1];                 // longitude
                        loc.userLoc.codePays            = request.response.country; // code du pays
                        loc.userLoc.nomVille            = request.response.city;    // nom de la ville
                        loc.userLoc.userIP              = request.response.userIP;  // adresse Ip de l'utilisateur

                        // maj du type de localisation
                        loc.userLoc.typeLoc = 7; // <=> type de localisation = localisation par IPAPI

                        //debug
                        debugAffInfosLoc();

                        // indique que la promesse s'est terminée correctement et
                        // renvoie un booléen pour indiquer le succès de la localisation
                        resolve(true);

                        // et supprime le timeout
                        clearTimeout(to);

                        // debug
                        infoDebug("info", "gcGeoloc() : localisation OK");

                    } // s'il y a bien des coordonnées
                    else { // si on n'a pas reçu de coord
                        // indique que la promesse s'est terminée correctement et
                        // renvoie un booléen pour indiquer l'échec de la localisation
                        resolve(false);
                        // et supprime le timeout
                        clearTimeout(to);
                        // debug
                        infoDebug("info", "gcGeoloc() : erreur de localisation");
                    } // si on n'a pas reçu de coord

                }  // s'il y a une réponse
                else { // si pas de réponse
                    // indique que la promesse s'est terminée correctement et
                    // renvoie un booléen pour indiquer l'échec de la localisation
                    resolve(false);
                    // et supprime le timeout
                    clearTimeout(to);
                    // debug
                    infoDebug("info", "gcGeoloc() : Pas de réponse");                    
                } // si pas de réponse


            }; // gestionnaire d'évènement pour la réception de la réponse


        }); // promise      
    } // function gcGeoloc()

    //===============================================================
    // fonction de géolocalisation avec API google Geolocation
    //===============================================================
    function googGeoLoc(locDejaOK) {
        /*
            googGeoLoc()

            Cette fonction fait appel à l'API de géoloc de google via l'adresse IP.
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        */

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "googGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }
            //debug
            infoDebug("log", "googGeoLoc()");

            // préparation de la requête
            var url = "https://www.googleapis.com/geolocation/v1/geolocate?key="+loc.apiKey; // URL
            // si dessous l'url passant par notre serveur, pour masquer la clé API
            // mais pour le moment, ça ne fonctionne pas.
            // var url = "index.php?proxy=gGeolocAPIProxy";

            var request = new XMLHttpRequest();     // nouvel objet XHR
            request.responseType = 'json';          // type de réponse qu'on attend
            request.open('POST', url);              // spécification de la méthode et de l'URL à laquelle récupérer les données
            
            // envoi de la requête
            request.send();
            
            // setTimeout pour gérer le cas où l'API ne répond pas
            var to = setTimeout(function(){
                infoDebug("info", "googGeoLoc() => timeout");
                //  au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout


            // gestionnaire d'évènement pour la réception des données.
            // Il faut en effet attendre que les données soient reçues
            request.onload = function() {
                //debug
                /*
                infoDebug("log", "googGeoLoc() : reception des données");
                if((typeof(option) != "undefined") && option.debug && option.debugCat.get("test") ){
                    infoDebug("test", "request.response = ");
                    console.log(request.response);
                }
                */

                // réinitialisation des données de position utilisateur
                initUserLoc();

                // MAJ des variables de localisation
                    var userLoc;
                    var precisionLoc;
                    var errors;
                if(request.response){ // on vérifie qu'il y a bien une réponse
                    userLoc      = request.response.location;
                    precisionLoc = request.response.accuracy;
                    errors       = request.errors;
                }
                
                // gestion des erreurs
                if(userLoc=== undefined) { // si erreur
                    warnDebug('googGeoLoc() : erreur de géolocalisation via API');
                    warnDebug('            => erreur = ' + errors);
                    warnDebug('            => precisionLoc = ' + precisionLoc);
                    warnDebug('request.response = ');
                    console.log(request.response);

                    // indique que la promesse s'est terminée correctement et
                    // renvoie un booléen pour indiquer l'échec de la localisation
                    resolve(false);
                    // et supprime le timeout
                    clearTimeout(to);

                } else { // si pas d'erreur

                    loc.userLoc = userLoc; // on mémorise la localisation de l'utilisateur.
                    // maj du type de localisation
                    loc.userLoc.typeLoc = 3; // <=> type de localisation = localisation par google
                    
                    //debug
                    debugAffInfosLoc();

                    // indique que la promesse s'est terminée correctement et
                    // renvoie un booléen pour indiquer le succès de la localisation
                    resolve(true);
                    // et supprime le timeout
                    clearTimeout(to);

                    // // débug - pour test
                    // // on fait en sorte que la réponse arrive plus tard que le timeout
                    // setTimeout(function(){
                    //  loc.userLoc = userLoc; // on mémorise la localisation de l'utilisateur.
                        
                    //  // debug
                    //  infoDebug("info", "googGeoLoc() : loc.userLoc = (lat = "+loc.userLoc.lat+"; lng = "+loc.userLoc.lng+")");
                    //  infoDebug("info", "              : précision = " + precisionLoc+ " m");
                    //  infoDebug("info", "              : errors = " + errors);

                    //  resolve(true);
                    // }, 4000); // setTimeout

                } // gestion d'erreurs
            }; // gestionnaire d'évènement pour la réception de la réponse

        }); // promise      
    } // function googGeoLoc()

    //===============================================================
    // fonction de géolocalisation avec API IP-API
    //===============================================================
    function ipapiGeoLoc(locDejaOK) {
        /*-----------------------------------------------------------
            ipapiGeoLoc()

            Cette fonction fait appel à l'API de géoloc de google via l'adresse IP.
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        --------------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "ipapiGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }

            //debug
            infoDebug("log", "ipapiGeoLoc()");
            
            //------------------------------------------------------------
            // préparation  et lancement de la requêtede la requête
            // var url = 'http://ip-api.com/json/?fields=status,message,country,countryCode,city,lat,lon,mobile,proxy,query';
            // version courte de la même URL, avec un code équivalent à l'ensemble des champs
            var url = 'http://ip-api.com/json/?fields=254163';

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.send();         


            // setTimeout pour gérer le cas où l'API ne répond pas
            var to = setTimeout(function(){
                infoDebug("info", "ipapiGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout


            // callback pour la réception des données
            xhr.onreadystatechange = function() {

                // si erreur de reception des données
                if(this.status != 200){
                    infoDebug("info", "ipapiGeoLoc() : erreur dans la réception des données");
                    infoDebug("info", "               this.status     = "+this.status);
                    // on indique que promesse terminée ET que la localisation est NOK
                    // NB: on fait un resolve(true) et non un reject, pour que dans ipGeoLoc(),
                    //     cette fonction puisse être chaînée avec d'autres fonctions.
                    resolve(false); 
                    // et supprime le timeout
                    clearTimeout(to);
                    // et quitte la fonction
                    return;         
                }

                // si la réponse n'est pas encore complètement arrivée
                // NB: readyState = 0 => données pas encore envoyées
                //     readyState = 1, 2 ou 3 => étapes intermédiaires (reception du header, ...)
                //     readyState = 4 => données complètement reçues
                if (this.readyState <4){
                    // débug
                    infoDebug("info", "ipapiGeoLoc() : réception des données en cours");
                    // on quitte la fonction, car elle sera de nouveau appelées aux
                    // différentes étapes de la réception des donées, et notamment 
                    // lorsque les données seront complètement reçues.
                    return;
                }

                // à partir de ce point, normalement les données sont correctement reçues
                // on exploite donc les données reçues
                var response = JSON.parse(this.responseText);
                if(response.status !== 'success') { // si réponse NOK
                    // debug
                    warnDebug("ipapiGeoLoc() : données reçues NOK : "+ response.message); // message d'erreur
                    // on indique que promesse terminée ET que la localisation est NOK
                    resolve(false);
                    // et supprime le timeout
                    clearTimeout(to);
                    return; // et on quitte la fonction
                } // si statut NOK des données reçues
                
                // à partir de ce stade, les données reçues sont OK
                
                // réinitialisation des données de position utilisateur
                initUserLoc();
                
                // récupération des données
                loc.userLoc.lat                 = response.lat;         // latitude
                loc.userLoc.lng                 = response.lon;         // longitude
                loc.userLoc.nomPays             = response.country;     // nom du pays
                loc.userLoc.codePays            = response.countryCode; // code du pays
                loc.userLoc.nomVille            = response.city;        // nom de la ville
                loc.userLoc.connectionMobile    = response.mobile;      // si connexion mobile
                loc.userLoc.connectionAnonyme   = response.proxy;       // si utilisation d'un proxy pour rendre anonyme
                loc.userLoc.userIP              = response.query;       // adresse Ip de l'utilisateur
                
                // maj du type de localisation
                loc.userLoc.typeLoc = 4; // <=> type de localisation = localisation par IPAPI

                //debug
                debugAffInfosLoc();

                // on indique que promesse terminée ET que la localisation est NOK
                resolve(true);
                // et supprime le timeout
                clearTimeout(to);
            }; // callback pour la réception des données
            
            
            
            //------------------------------------------------------------
            
            // pour l'instant, on renvoie directement un resolve(false) car l'appel
            // à l'API n'est pas encore en place.
            // infoDebug("info", "ipapiGeoLoc() : pour l'instant, fonction vide => pas de localisation")
            // resolve(false);

            // débug - pour test
            // pour vérifier le fonctionnement si l'API ne répond pas
            // on fait en sorte que la réponse arrive plus tard que le timeout
            // setTimeout(function(){
            //  resolve(false);
            // }, 2000); // setTimeout

            // setTimeout pour gérer le cas où l'API ne répond pas
            setTimeout(function(){
                infoDebug("info", "ipapiGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout

        }); // promise      
    } // function ipapiGeoLoc()

    //===============================================================
    // fonction pour débuger les infos de localisation
    //===============================================================
    function debugAffInfosLoc(){
        // type de localisation utilisée
        var txtTypeLoc = ["undefined", "loc forcée par la page", "navigateur",
                          "google", "IPAPI", "user search", "No Loc", "gcGeoloc"];

        infoDebug("info", "données de localisation reçues : ");
        infoDebug("info", "               type de loc       : "+txtTypeLoc[loc.userLoc.typeLoc]);               
        infoDebug("info", "               latitude          : "+loc.userLoc.lat);
        infoDebug("info", "               longitude         : "+loc.userLoc.lng);
        infoDebug("info", "               nom du pays       : "+loc.userLoc.nomPays);
        infoDebug("info", "               code du pays      : "+loc.userLoc.codePays);
        infoDebug("info", "               nom de la ville   : "+loc.userLoc.nomVille);
        infoDebug("info", "               connexion mobile  : "+loc.userLoc.connectionMobile);
        infoDebug("info", "               connexion anonyme : "+loc.userLoc.connectionAnonyme);
        infoDebug("info", "               precision         : "+loc.userLoc.precision);
        infoDebug("info", "               erreurs           : "+loc.userLoc.errors);
        infoDebug("info", "               IP utilisateur    : "+loc.userLoc.userIP);

    } // function debugAffInfosLoc()

    //===============================================================
    // initilise la position utilisateur
    //===============================================================
    function initUserLoc(){
    
        /*-----------------------------------------------------------
            function initUserLoc()
            
            Cette fonction permet de réinitialiser tous les champs de
            la position utilisateur. C'est nécessaire car on peut utiliser
            plusieurs API différentes pour la localisation, et elles ne
            fournissent pas toutes les mêmes données. Donc, pour éviter de
            mélanger des champs mis à jours et d'autres non mis à jour, on
            réinitialise tous les champs avant chaque modification des
            variables de position utilisateur.
            
            C'est directement la variable globale qui est modifiée.
        -----------------------------------------------------------*/

        // Localisation par défaut de l'utilisateur :
        loc.userLoc = {lat: 48.8588377, lng: 2.2770203};            // Paris
        //loc.userLoc = {lat: 43.604652,  lng: 1.4442090000000007};   // Toulouse
        
        // autres champs
        loc.userLoc.nomPays             = "";           // (string) nom du pays
        loc.userLoc.codePays            = "";           // (string) code du pays
        loc.userLoc.nomVille            = "";           // (string) nom de la ville
        loc.userLoc.connectionMobile    = undefined;    // (booléen) si connexion mobile
        loc.userLoc.connectionAnonyme   = undefined;    // (booléen) si utilisation d'un proxy pour rendre anonyme
        loc.userLoc.userIP              = "";           // (string) adresse Ip de l'utilisateur
        loc.userLoc.precision           = undefined;    // précision de la localisation
        loc.userLoc.errors              = "";           // erreur lors de la localisation
        loc.userLoc.typeLoc             = 0;            // (integer)  voir ci-dessous
        // typeLoc : 0 = undefined, 1 = loc forcée par la page, 2 = navigateur, 3 = google, 
        //           4 = IPAPI, 5 = user search; 6 = No Loc <=> default, 7 = gcGeoloc
    } // function initUserLoc()
    
    //===============================================================
    // Géolocalisation de l'utilisateur via le navigateur
    //===============================================================
    function navGeoLoc(locDejaOK) {
        /*-----------------------------------------------------------
            navGeoLoc()

            Cette fonction localise l'utilisateur grâce à l'option geolocation du navigateur
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        --------------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "navGeoloc() => fonction shuntée");
                resolve(true); 
                return;
            }

            if (!window.isSecureContext){
                // NB: quitte la fonction car l'accès à la géoloc du navigateur
                // ne peut se faire que dans un contexte sécurisé.
                infoDebug("log", "navGeoloc() : contexte non-sécurisé => fonction shuntée");
                resolve(false);
                return;
            }

            //debug
            infoDebug("log", "navGeoloc()");            

            // Si le navigateur accepte l'option de geolocalisation
            if (navigator.geolocation) {  
              // extrait la position de l'utilisateur
              navigator.geolocation.getCurrentPosition(function(position) {
              
                // réinitialisation des données de position utilisateur
                initUserLoc();            
              
                // MAJ de la position
                loc.userLoc = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                // maj du type de localisation
                loc.userLoc.typeLoc = 2; // <=> type de localisation = localisation par le navigateur

                // debug
                infoDebug("info", "navGeoLoc() : Nav OK, loc = {lat = "+loc.userLoc.lat+"; lng = "+loc.userLoc.lng+"}");

                // indique que la promesse et terminée
                // et renvoie true pour indiquer que la localisation est OK
                resolve(true);


              }, function() {  // fonction appelée en cas d'erreur de geoloc par le navigateur
                    // debug
                    infoDebug("info", "navGeoLoc() : Nav OK, mais erreur de localisation");

                    // indique que la promesse et terminée
                    // et renvoie false pour indiquer que la localisation est NOK
                    resolve(false);

              });  // fonction appelée en cas d'erreur de geoloc par le navigateur
            } else {
              // Le navigateur ne supprote pas l'option de geolocalisation
                    // debug
                    infoDebug("info", "navGeoLoc() : Nav NOK : le navigateur ne supporte pas l'option de geolocalisation");

                    // indique que la promesse et terminée
                    // et renvoie false pour indiquer que la localisation est NOK
                    resolve(false);
            }// si le navigateur ne supporte pas l'option de geolocalisation

            // setTimeout pour gérer le cas où le navigateur ne érpond pas
            setTimeout(function(){
                infoDebug("info", "navGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout

        }); // promise  
    } // function navGeoLoc()

    //===============================================================
    // appelé en cas de changement d'adresse
    //===============================================================
    function changeLoc(){
        /*-----------------------------------------------------------
            function changeLoc()

            Cette fonction est appelée en cas de changement d'adresse
            dans le champ de changement d'adresse.

            NB: les API de géoCodage vont directement chercher dans la 
            variable globale loc.adresseAGeocoder l'adresse à géoCoder.
        -----------------------------------------------------------*/

        // debug
        infoDebug("log", "changeLoc()");

        // maj du type de localisation
        loc.userLoc.typeLoc = 5; // <=> type de localisation = User Search

        // appel à la fonction de géo-codage
        // pour transformer le nom de lieu en coordonnées
        // NB: geoCod renvoie une variable statusCod, qui contient
        //     une chaîne de caractère avec le statut de géocodage.
        //          "OK"                => géocodage OK, adresse trouvée
        //          "champ vide"        => champ de recherche vide
        //          "adresse inconnue"  => adresse inconnue
        //          "meme adresse"      => même adresse que pour recherche précédente.
        //          "timeout"           => le serveur ne répond pas à temps
        geoCod()
        .then(function(statusCod){ // fonction appelé une fois le géocodage terminé
            // si géo-codeg OK
            if((statusCod == "OK")||(statusCod == "meme adresse")){
                nouvelleLoc();
            } // if(statusCodOK)
            else {
                // gère la zone d'accueil et la carte pour indiquer que l'adresse est inconnue.
                msgAccueilMauvaiseLoc(statusCod);
            } // if(statusCodOK) - else
        })  
        .catch(function(){ // fonction appelée en cas d'erreur avec la promesse de geoCod()
            // gère la zone d'accueil et la carte pour indiquer que l'adresse est inconnue.
            msgAccueilMauvaiseLoc(statusCod);
        }); // en cas d'erreur
    } // function changeLoc()

    //===============================================================
    // gère le géocodage
    //===============================================================
    function geoCod() {

        /*-----------------------------------------------------------
            function geoCod()

            Cette fonction gère la localisation via l'adresse ip.
            Elle renvoie une promesse pour faciliter la gestion du fonctionnement
            asynchrone.

            Entrée : 
            --------
            Sorties : 
            ---------
                Une promesse
                    => si resolve : 
                        - statusCod
                            "OK"                => géocodage OK, adresse trouvée
                            "champ vide"        => champ de recherche vide
                            "adresse inconnue"  => adresse inconnue
                            "meme adresse"      => même adresse que pour recherche précédente.
                            "timeout"           => le serveur ne répond pas à temps
                    => si reject : 
                        -errorMsg  : une chaîne de caractère avec le message d'erreur
        -----------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            //debug
            infoDebug("log", "geoCod()");

            // adresse à géo-coder
            var adresse = loc.adresseAGeocoder;

            // si l'adresse est vide, on quitte la fonction
            if(adresse.length == 0){
                infoDebug("info", "googGeoCod() : Le champ de recherche est vide");
                resolve("champ vide");
                return; // alors on quitte la fonction
            }

            // si la recherche est la même que précédemment 
            // (même adresse et même type de recherche)
            if((adresse == loc.adresse) && (recherche.lastSearchType == "adresse")){
                infoDebug("info", "googGeoCod() : même recherche que précedemment");
                resolve("meme adresse");
                return; // alors on quitte la fonction
            }
            
            // NB: à ce stade, on a vérifié qu'il s'agit bien d'une nouvelle recherche
            
            // appel aux API de géocodage
            // NB: toutes ces fonctions se passent le paramètre statusCod, qui vaut false 
            // tant que le géocodage est NOK, et "OK" quand le géocodage est OK.
            // NB: toutes les fonctions de géocodage vont directement chercher dans la 
            // variable globale loc.adresseAGeocoder l'adresse à géocodeer.
            // NB: pour l'instant (2019 09 18) il n'y a qu'une seule  fonction de géo-codage,
            // mais la structure permet d'en ajouter d'autres facilement.
            googGeoCod()
            .then(resolve)   // indique que promesse résolue ET passe status de géocodage
            .catch(reject);  // indique que promesse rejetée ET passe le message d'erreur

            // setTimeout pour gérer le cas où les fonctions appelées ne répondent pas
            // NB: la durée du timeout est de 1 fois la durée standard car geoCod()
            // va in fine appeler successivement 1 fonctions avec une durée standard
            // pourle timeout
            setTimeout(function(){
                infoDebug("info", "geoCod() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans geoCod()
                resolve("timeout");
            }, fonctionnement.dureeTimeout*1); // setTimeout

        }); // promise              
    } // function geoCod()

    //===============================================================
    // géocodage avec l'API de google maps
    //===============================================================
    function googGeoCod(statusCod) {
        /*-----------------------------------------------------------
            function googGeoCod()
            
            Cette fonction trouve la latitude et la longitude correspondant à une adresse 
            donnée par l'utilisateur.
            Elle renvoie une promesse pour une gestion de l'aspect asynchrone facilité.

            Variables globales utilisées :
            ------------------------------
                loc.adresseAGeocoder : contient l'adresse à géo-coder
            Entrées : 
            ---------
                - statusCod
                    undefined           => pas de géocodage réalisé par d'éventuelles autres fonction
                    "OK"                => géocodage OK, adresse trouvée (dans ce cas il a été fait par une autre fonction)
                    "champ vide"        => champ de recherche vide
                    "adresse inconnue"  => adresse inconnue
                    "meme adresse"      => même adresse que pour recherche précédente.
                    "timeout"           => le serveur ne répond pas à temps
            Sorties : 
            ---------
                une promesse
        -----------------------------------------------------------*/


        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si le géo-codag est déjà OK via une autre fonction
            if(statusCod == "OK"){
                infoDebug("info", "googGeoCod() => fonction shuntée");
                resolve(statusCod); 
                return;
            }

            //debug
            infoDebug("log", "googGeoCod()");

            // adresse à géo-coder
            var adresse = loc.adresseAGeocoder;

            // maj du nombre de recherches
            recherche.nbre++;

            // MAJ des infos de l'adresse demandée et du type de recherche
            loc.adresse = adresse;
            recherche.lastSearchType = "adresse";

            // variable pour objet geocodeur
            var geocoder = new google.maps.Geocoder(); 

            // géocodage de l'adresse
            geocoder.geocode( { 'address': loc.adresse}, function(results, status) {
                if (status == 'OK') {
                    // réinitialisation des données de position utilisateur
                    initUserLoc();              
                
                    // position correpondant à l'adresse
                    loc.userLoc.lat = results[0].geometry.location.lat();
                    loc.userLoc.lng = results[0].geometry.location.lng();

                    // debug
                        infoDebug("info", 'googGeoCod() : fonction anonyme : OK, loc.userLoc = {'+loc.userLoc+'}');

                    // indique que la promesse est terminée
                    // et envoie true pour indiquer que la localisation est correcte
                    resolve("OK");

                } else { // si erreur de géocodage
                // debug
                    warnDebug('googGeoCod() : adresse inconnue : ' + status);

                    // indique que la promesse est terminée
                    // et envoie false pour indiquer que la localisation est NOK
                    resolve("adresse inconnue");

                } // si erreur de géocodage
            }); // géocodage de l'adresse

            // setTimeout pour gérer le cas la recherche de l'élement sur la page web
            // met trop de temps
            setTimeout(function(){
                infoDebug("info", "googGeoCod() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve("timeout");
            }, fonctionnement.dureeTimeout); // setTimeout

        }); // promise          
    }// function googGeoCod

    //===============================================================
    // fonction appelée en cas de changement de localisation
    //======================================================
    function nouvelleLoc() {
        // debug
        infoDebug("log", "nouvelleLoc()");

        supprimeUserMarker();   // supprime le marker de la position utilisateur.
        demandeCoachsProches(); // Demande au serveur la liste des coachs à proximité de l'utilisateur
        addUserMarker();        // ajout du marker avec la position de l'utilisateur.
    } // function nouvelleLoc()

    //===============================================================
    // Localisation forcée
    //==============================================================
    function locForcee(locDejaOK) {

        /*------------------------------------------------------------------
            function locForcee()

            Cette fonction permet de vérifier si le serveur a forcé une localisation.
            Ca permet notamment de gérer un appel à la page des coachs avec comme
            paramètre get une localisation (ex: touslescoachs.com/coachs?loc=Toulouse).
            Ca permet notamment de pouvoir tenir compte des recherches de l'utilisateur
            sur des moteurs de recherche avec une recherche de type "coachs Toulouse"
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Pour forcer une localisation : 
            ------------------------------
                il faut que l'élément "#TLC-ville-init" de la page web contienne le nom 
                d'une localité. On utilise alors le géocodage pour transformer ce nom en
                un jeu de coordonnées.


            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        -------------------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "cookieGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }

            //debug
            infoDebug("log", "locForcee()");

            // localisation forcée, s'il y en a une
            //-------------------------------------
            loc.locInit = $("#TLC-ville-init").text();
            loc.locInit = loc.locInit.trim(); // suppression des espaces en début et fin de chaîne

            // s'il y a une localiastion initiale forcée
            if(loc.locInit != ""){
                // debug
                infoDebug("info", "locForcee() : localisation initiale forcée à : "+loc.locInit);
                // MAJ de l'adresse à géo-coder
                loc.adresseAGeocoder = loc.locInit; 
                // MAJ du type de localisation
                loc.userLoc.typeLoc = 1; // <=> type de localisation = localisation forcée par une variable de la page web
                // appel à la fonction de géo-codage
                // pour transformer le nom de lieu en coordonnées
                // NB: googGeoCod renvoie le status (true ou false du géocodage)
                geoCod()
                .then(resolve)  // promesse terminée + renvoie le status de géocodage
                .catch(reject); // en cas d'erreur

            } // s'il y a une localisation forcée
            else { // si pas de localisation forcée
                // debug
                infoDebug("info", "locForcee() : pas de localisation forcée");
                // on indique que la promesse est terminée
                // et on renvoie false car pas de localisation forcée
                resolve(false);
            } // si pas de localisation forcée

            // setTimeout pour gérer le cas la recherche de l'élement sur la page web
            // met trop de temps
            setTimeout(function(){
                infoDebug("info", "cookieGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout

        }); // promise          
    } // function locForcee()

    //===============================================================
    // localisation à partir d'un cookie qui a enregistré la loc précédente
    //==============================================================
    function cookieGeoLoc(locDejaOK) {
        /*----------------------------------------------------------
            function cookieGeoLoc()

            Cette fonction regarde si une localisation précédente est
            enregistrée dans un cookie et si oui l'utilise.
            Elle renvoie une promesse pour faciliter la gestion asynchrone.

            Entrées : 
            ---------
                locDejaOK : si locDejaOK=true, alors la fonction est shuntée.
                            Ca permet de chaîner facilement et lisiblement les fonctions
                            de géolocalisation en faisant en sorte que dès que l'une d'elle
                            arrive à localiser, les autres soient shuntées.
            Sorties : 
            ---------
                une promesse est renvoyée pour gérer plus facilement l'aspec asynchrone.
                    => lorsque la promesse est "resolve", alors le paramètre renvoyé est un
                    booléen indiquant si la localisation est un succès ou un échec.
                    NB: dans cas, la fonction a directement modifié la variable globale de
                    localiation et les coordonnées se trouvent dedans.
        ----------------------------------------------------------*/

        // renvoie une promesse pour gérer le fonctionnemnt asynchrone
        return new Promise((resolve, reject) =>{

            // shunt de la fonction si la localisation est déjà OK via une autre fonction
            if(locDejaOK){
                infoDebug("info", "cookieGeoLoc() => fonction shuntée");
                resolve(true); 
                return;
            }

            //debug
            infoDebug("log", "cookieGeoLoc()");

            // pour l'instant, on renvoie directement un resolve(false) car l'utilisation
            // de cookies n'est pas encore en place.
            infoDebug("info", "cookieGeoLoc() : pour l'instant, fonction vide => pas de localisation")
            resolve(false);

            // ATTENTION : quand on utilisera vraiment les cookies, il faudra penser à réinitialiser
            // la position utilisateur avant de la mettre à jour. Pour ça, décommenter la ligne ci-dessous.
                    // réinitialisation des données de position utilisateur
                    //initUserLoc();            
            
            
            // débug - pour test
            // pour vérifier le fonctionnement si l'API ne répond pas
            // on fait en sorte que la réponse arrive plus tard que le timeout
            // setTimeout(function(){
            //  resolve(false);
            // }, 2000); // setTimeout

            // setTimeout pour gérer le cas où l'API ne répond pas
            setTimeout(function(){
                infoDebug("info", "cookieGeoLoc() => timeout");
                // Au bout du délai, on indique que la promesse est terminée, mais que la localisation
                // n'a pas pu être faite.
                // NB: on n'utilise surtout pas de reject, car sinon ça va planter le
                //     chaînage des fonctions de localisation dans ipGeoLoc()
                resolve(false);
            }, fonctionnement.dureeTimeout); // setTimeout

        }); // promise      
    } // function cookieGeoLoc()

    //===============================================================
    // stats sur la position
    //==============================================================
    function statPos(){
        /*----------------------------------------------------------
            function statPos()

            Cette fonction enregistre la position anonymisée dans 
            la base de données.
        ----------------------------------------------------------*/

        // debug
        infoDebug("log", "statPos()");

        // vérifie s'il faut shunter cette fonction
        if(tlcParam.stats.statPos == OFF){return;}

        // preparation d'un objet à envoyer au serveur
        //============================================
        var objToSend = {}
        // localiastion
        objToSend.sp_lat            = loc.userLoc.lat;
        objToSend.sp_lng            = loc.userLoc.lng;
        // type de localisation
        objToSend.sp_typeLoc        = loc.userLoc.typeLoc;
        // si connection mobile ou non
        if(loc.userLoc.connectionMobile){
            objToSend.sp_appareilNomade  = true; // connection Mobile
        }
        else{
            objToSend.sp_appareilNomade  = false; // connection non mobile, ou on en sait rien.
        }

        // si il y a des coachs à proximité
        objToSend.sp_nbreCoachsProxy = 0;  // init
        if((typeof(coachs) != "undefined") 
            && (typeof(coachs.liste) != "undefined") 
            && (coachs.liste != null)){
            objToSend.sp_nbreCoachsProxy = coachs.liste.length;
        }
        
        // debug
        infoDebug("test", "éléments envoyés :");
        if((typeof(option) != "undefined") && option.debug && (option.debugCat["test"] == true)){
            console.log(objToSend);
        }

        // envoi des infos au serveur
        if((typeof(option) != "undefined") && option.debug){
            // version qui affiche des infos de debug dans la console
            sendToServer(ajaxCall.root, ajaxCall.statPos, objToSend, testDonneesRecues);
        }
        else{
            // version de production, qui n'envoie pas d'infos de debug
            sendToServer(ajaxCall.root, ajaxCall.statPos, objToSend);
        }
    } // function statPos()

//###############################################################
// Zone d'accueil
//###############################################################
    //================================================================================================================
    // Mise à jour du texte avec le nombre de coachs
    //================================================================================================================
    function majZoneAccueil(){
    
        /*---------------------------------------------------------------------------------
        function majAccueil()
        
        Cette fonction met à jour le texte de la zone d'accueil, avec le nombre de coachs.
        
        si moins de 2 coachs => "Coachs à proximité"
        si au moins 2 coachs => nbre + " coachs à proximité"
        et si l'utilisateur a entré une adresse, alors on ajoute " de " +  adresse
        ---------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "majZoneAccueil()");

        // extraction du nombre de coachs
        var nbreCoachs = coachs.liste.length;


        
        // préparation du texte à afficher
        //================================
        var txtZoneAccueil = '';


        switch(recherche.lastSearchType){
            case "adresse":
                // gestion du nombre de coachs
                if(nbreCoachs <=1){ 
                    txtZoneAccueil += "Coachs à proximité";
                } else {
                    txtZoneAccueil += nbreCoachs + ' coachs à proximité';
                } // if(nbreCoachs...) - gestion du nombre de coachs
                if(loc.adresse.length > 0){ // gestion de l'adresse
                    txtZoneAccueil += ' de ' + loc.adresse;
                } // if - gestion de l'adresse
                break;

            case "coach":
                // gestion du nombre de coachs
                if(nbreCoachs == 0){
                    txtZoneAccueil += "Aucun coach ne correspond à votre recherche";
                } else if(nbreCoachs == 1){
                    txtZoneAccueil += " 1 coach trouvé";
                } else if(nbreCoachs < recherche.nbreMaxCoachs){ // si plus de 1 coach
                    txtZoneAccueil += nbreCoachs + " coachs trouvés";
                } else { // si 15 ou plus coachs trouvés
                    txtZoneAccueil += "Plus de " + recherche.nbreMaxCoachs + " coachs trouvés. affinez votre recherche."
                } // if - gestion du nombre de coachs
                break;
            default:
                errorDebug("majZoneAccueil() : recherche.lastSearchType = valeur inconnue");
        } // switch


        
        // modification du titre de la zone d'accueil
        messageAccueil(txtZoneAccueil);
    } // function majZoneAccueil()
    
    //================================================================================================================
    // Modification du message de la zone d'accueil
    //================================================================================================================
    function messageAccueil(message){

        /*-------------------------------------------------------------
            function MessageAccueil()
    
            Modification du message de la zone d'accueil

            Entrées : 
            ---------
            message : message à afficher en zone d'accueil
        -------------------------------------------------------------*/

        // debug
        infoDebug("log", "messageAccueil()");

        $(function() {   // partie en jQuery
            $("#TLC-accueil-titre").empty().append(message);
        }); // partie en jQuery
    } // function MessageAccueil()


    //================================================================================================================
    // gère zone d'accueil et carte lorsque la localisation est impossible
    //================================================================================================================
    function msgAccueilNoLoc(){
        /*---------------------------------------------------------------------
            function msgAccueilNoLoc()

            gère le message d'accueil et la carte lorsque la localisation est impossible

        ---------------------------------------------------------------------*/

        // debug
        infoDebug("log", "msgAccueilNoLoc()");

        var txtPasDeLoc = "Choisissez une ville ou recherchez un coach par son nom";

        // affiche un message pour indiquer que pas de géoloc
        messageAccueil(txtPasDeLoc);

        // gestion de la carte
        //--------------------
        // - afficher une carte par défaut s'il n'y en a pas
        // - sinon : 
        //      - supprimer la carte s'il y en a une
        //      - afficher des photos et message sur le coaching
    } // function msgAccueilNoLoc()

    //================================================================================================================
    // gère zone d'accueil et carte lorsque la localisation est erronée
    //================================================================================================================
    function msgAccueilMauvaiseLoc(statusCod){
        /*---------------------------------------------------------------------
            function msgAccueilMauvaiseLoc()

            gère le message d'accueil et la carte lorsque la localisation est impossible
            
            Entrées : 
            ---------
                statusCod : une chaîne de caractère avec le statut de géocodage
                    "OK"                => géocodage OK, adresse trouvée (msgAccueilMauvaiseLoc n'est pas appelé dans ce cas)
                    "champ vide"        => champ de recherche vide
                    "adresse inconnue"  => adresse inconnue
                    "meme adresse"      => même adresse que pour recherche précédente. (msgAccueilMauvaiseLoc n'est pas appelé dans ce cas)
                    "timeout"           => le serveur ne répond pas à temps


        ---------------------------------------------------------------------*/

        // debug
        infoDebug("log", "msgAccueilMauvaiseLoc()");

        // message par défaut
        var txtMsg;
        
        // message en fonction du statut de géoCodage
        switch(statusCod) {
            case "adresse inconnue":
                txtMsg = "adresse inconnue. Choisissez une autre ville ou recherchez un coach par son nom";
            break;
            case "champ vide":
                // dans ce cas, on n'affiche rien
                return;
            break;
            case "timeout":
                txtMsg = "serveur injoignable, essayez plus tard";
            break;
            default:
                // message par défaut
                txtMsg = "adresse inconnue. Choisissez une autre ville ou recherchez un coach par son nom";
        } // switch 

        // affiche un message pour indiquer que pas de géoloc
        messageAccueil(txtMsg);

        // gestion de la carte
        //--------------------
        // - afficher une carte par défaut s'il n'y en a pas
        // - sinon : 
        //      - supprimer la carte s'il y en a une
        //      - afficher des photos et message sur le coaching
    } // function msgAccueilMauvaiseLoc()

//###############################################################
// filtrage
//###############################################################


    //================================================================================================================
    // génère un id pour une checkbox correspondant à une option donnée d'un filtre donné
    //================================================================================================================
    function idCheckbox(numFiltre, numOption){

        return filtreCoachs.prefixeIdCheckbox + filtreCoachs.nom[numFiltre] + '-' + numOption;
    } // function idCheckbox()

    //================================================================================================================
    // génère un id pour un badge correspondant à une option donnée d'un filtre donné
    //================================================================================================================
    function idBadge(numFiltre, numOption){

        return filtreCoachs.prefixeIdBadge + filtreCoachs.nom[numFiltre] + '-' + numOption;
    } // function idBadge()

    //================================================================================================================
    // génère un id pour un spinner correspondant à une option donnée d'un filtre donné
    //================================================================================================================
    function idSpinner(numFiltre, numOption){

        return filtreCoachs.prefixeIdSpinner + filtreCoachs.nom[numFiltre] + '-' + numOption;
    } // function idBadge()

    //================================================================================================================
    // ajout des filtres sur la page
    //================================================================================================================
    function addFiltres(){

        var options;            // variable pour récupérer les options correspondants à un filtre
        var txtId;              // variable pour les noms d'id
        var txtClasse;          // variable pour les noms de classes
        var blocHtml;           // variable avec le bloc hatml à ajouter pour ajouter une option
        var selection;          // sélecteur pour la zone de filtres
        var selectedCheckbox;   // sélecteur pour une checkbox
        var txtIdCardHeader; 
        var txtIdCardBody;
        var txtCollapse;
        var txtHideShow;
        var txtIdBadgeFiltre;   // txt de l'id du bagde indiquant le nombre de choix cochés pour chaque filtre
        
        // debug
        infoDebug("log", "addFiltres()");

        $(function() {   // partie en jQuery

            // sélecteur de la zone de filtres
            selection = $("#TLC-filtre-items");
            // boucle sur le tableau des noms de filtres
            filtreCoachs.label.forEach(function(itemFiltre, indexFiltre) {
                
                // txt de l'id du header de la card qui sert pour collapser le filtre
                txtIdCardHeader = "cardHeader-filtre-"+indexFiltre;

                // txt de l'id du body de la card qui sert pour collapser le filtre
                txtIdCardBody = "cardBody-filtre-"+indexFiltre;             

                //txt de l'id de la partie collapsable du filtre
                txtCollapse = "collapse-filtre-"+indexFiltre;

                // txt pour indiquer l'état du collapse : hide ou show
                
                    // on ne déplie que le 1er collapse
                    if(indexFiltre == 0){
                        txtHideShow = 'show';
                    } else {
                        txtHideShow = 'hide';
                    }
                
                    // tous les filtres repliés
                    //txtHideShow = 'hide';
                
                //txt de l'id du badge indiquant le nombre d'options cochées pour ce filtre
                txtIdBadgeFiltre = "TLC-badge-filtre-" + indexFiltre;

                // préparation du bloc html qui va permettre d'insérer les filtres et les options
                blocHtml = '<div class="card w-100">'  // w-100 pour que ça occupe toute la largeur disponible
                          //+       '<div class="card-header coul-yellow" id="'+txtIdCardHeader+'">'
                          +         '<div class="row coul-gris247 align-items-center">'
                          +             '<div class="col-9">'
                          +                 '<h3 class=" '+filtreCoachs.prefixeClasse+'nom ">' // titre du filtre 
                          +                     '<button class="TLC-filtre-coach btn btn-link statClic" sc-nom="filtre" sc-typeParam1="text" sc-priorite="2" type="button" data-toggle="collapse" data-target="#'+txtCollapse+'" aria-expanded="true" aria-controls="'+txtCollapse+'">'
                          +                         itemFiltre
                          +                     '</button>'
                          +                 '</h3>'
                          +             '</div>' // row
                          +             '<div class="col-3">' // col du badge (pour le nombre de cases cochées dans le filtre)
                          +                 '<p class="text-center">'
                          +                     '<span class="badge badge-success" id="'+txtIdBadgeFiltre+'"> 0 </span>' // badge avec le nombre de cases cochées dans le filtre
                          +                     ' ' // des alt-255 pour que le badge ne soit pas complètement sur le bord
                          +                 '</p>'
                          +             '</div>' // col du badge (pour le nombre de cases cochées dans le filtre)
                          +         '</div>' // row
                          +     '</div>'
                          +     '<div id="'+txtCollapse+'" class="collapse '+txtHideShow+'" aria-labelledby="'+txtIdCardHeader+'" data-parent="#TLC-filtre-items">'
                          +         '<div class="card-body" id="'+txtIdCardBody+'">'
                          +         '</div>'
                          //+       '</div>'
                          + '</div>';

                // ajoute le bloc html pour dans lequel il va y avoir le filtre, avec toutes les
                // options pour qu'il soit collapsable.
                selection.append(blocHtml); 
                
                // masquage du badge contenant le nombre de cases cochées du filtre car il est à 0
                // et qu'on ne l'affiche que si le nombres est supérieur à 0.
                $("#TLC-badge-filtre-" + indexFiltre).hide();

                // sélecteur de la zone pour mettre les options du filtre
                selectionZoneOptions = $("#"+txtIdCardBody);

                // récupération des options correspondant au filtre
                options = filtreCoachs.options[indexFiltre];

                // boucle sur les options
                options.forEach(function(itemOption, indexOption){
                    // txt de l'id de la checkbox
                    txtIdCheckbox = idCheckbox(indexFiltre, indexOption);
                    // txt de l'id du badge
                    txtIdBadge = idBadge(indexFiltre, indexOption);
                    // txt de l'id du spinner
                    txtIdSpinner = idSpinner(indexFiltre, indexOption);
                    // txt de la classe pour les checkboxes d'un même filtre
                    txtClasse = filtreCoachs.prefixeClasse  + filtreCoachs.nom[indexFiltre];
                    // txt du bloc html permettant d'ajouter une option
                    // NB: on définit le texte du bloc en une seule fois plutôt que d'ajouter
                    // successivement chaque ligne car si on ajoute successivement chaque ligne,
                    // alors, jQuery corrige ce qu'il considère comme des erreurs.
                    // par exemple, si on ajoute une ligne avec "<div>" et qu'il ne voit pas tout de suite
                    // le "</div>", alors, jQuery le rejoute automatiquement.
                    blocHtml = '<div class="form-check">'
                                  +'<input class="form-check-input statClic '+txtClasse+' '+filtreCoachs.ClasseCheckbox+'" '
                                  +        'sc-nom="filtre-option" sc-param1="'+itemFiltre+'" sc-param2="'+itemOption+'" sc-priorite="1" '
                                  +        'type="checkbox" value="" id="'+txtIdCheckbox+'">'
                                  +'<label class="form-check-label statClic" sc-nom="filtre-option" sc-param1="'+itemFiltre+'" sc-param2="'+itemOption+'" sc-priorite="1" '+'for="'+txtIdCheckbox+'">'
                                  +     itemOption + '   ' // NB: on ajoute des alt-255 après itemOption pour forcer un espace
                                  +'</label>'
                                  +'<span class="badge badge-light" id="'+txtIdBadge+'" > ' // juste après le '<span>', le texte initial des badges
                                  +'</span>'
                                  +'<div class="spinner-border spinner-border-sm text-primary" role="status" id="'+txtIdSpinner+'">' // spinner
                                  +     '<span class="sr-only">Loading...</span>'
                                  +'</div>' // spinner
                              +'</div>';

                    // ajout du bloc html sur la page
                    selectionZoneOptions.append(blocHtml);

                    // ajout de données sur la checkbox, afin de pouvoir plus tard extraire facielement le n° d'option
                    // correspondant
                    selectedCheckbox = $('#'+txtIdCheckbox); // sélecteur de la checkbox
                    selectedCheckbox.data('infos', {numFiltre:indexFiltre, numOption:indexOption, 
                        prefixeFiltre:filtreCoachs.nom[indexFiltre], txtIdSpinner:txtIdSpinner}); // ajout des infos

                    // masquage du spinner
                    $('#'+txtIdSpinner).hide('');

                }); // options.forEach
            }); // filtreCoachs.label.forEach

            // ajout d'un évènement appelé au changement d'une des checkboxes d'un des filtres.
            // L'évènement lance la fonction filtrage()
            addEventFiltre(); 
        }); // $(function()
    } // function addFiltres()

    //================================================================================================================
    // Mise à jour des valeurs des badges
    //================================================================================================================
    function majBadges(){
        /*
            Cette fonction met à jour les valeurs des badges, c'est à dire
            le nombre de coachs correspondant à chaque option, pour chaque option 
            de chaque filtre.
        */

        // debug
        infoDebug("log", "majBadges()");

        var selection;
        var idCoachs;

        $(function() {   // partie en jQuery

            // boucle for sur le nombre de filtres
            for(numFiltre = 0; numFiltre < filtreCoachs.nom.length; numFiltre++){
                // boucle for sur le nombre d'options du filtre
                for(numOption = 0; numOption < filtreCoachs.options[numFiltre].length; numOption++){
                    // texte de l'id du badge correspondant
                    txtIdBadge = idBadge(numFiltre, numOption);

                    // appel du filtre spécifique correspondant pour savoir combien
                    // de coachs remplissent la condition de cette option, de ce filtre
                    idCoachs = filtreCoachs.fonction[numFiltre](numOption, numFiltre);

                    // supprime les doublons dans la liste des coachs.
                    // Il peut en effet y avoir des doublons quand des coachs proposent 
                    // plusieurs offres.
                    idCoachs = supprimeDoublons(idCoachs);

                    // Sélection du badge
                    // ATTENTION : risque de comportement asynchrone !
                    selection = $("#"+txtIdBadge);
                    selection.text(idCoachs.length);

                } // for(numOption ...)
            } // for(numFiltre ...)
        }); // $(function() Partie en jQuery
    } // function majBadges()

    //================================================================================================================
    // affSelectedCoachs() : affiche les markers des coachs sélectionnés
    //================================================================================================================
    function affSelectedCoachs(coachsSelected){

        // debug
        infoDebug("log", "affSelectedCoach()");

        // supprime les animations des markers des coachs
        supprimeAnimationMarkersCoachs();

        // boucle sur l'ensemble des coachs
        coachsSelected.forEach(function(selected, numCoach){        
            if(selected){
                showMarkerCoach(numCoach); // affiche le marker du coach
                showEncartCoach(numCoach); // affiche l'encart du coach
            } else { // if(selected)
                hideMarkerCoach(numCoach); // cache le marker du coach
                hideEncartCoach(numCoach); // cache l'encart du coach
            } // if(selected) - else
        }); // coachsSelected.forEach
    } // function affSelectedCoachs()

    //================================================================================================================
    // filtrage() : fonction qui gère l'ensemble des filtres
    //================================================================================================================
    function filtrage(){

        // debug
        infoDebug("log", "filtrage()");

        // liste des checkboxes qui sont sélectionnées
        var checkboxesSelectionnees =  $("."+filtreCoachs.ClasseCheckbox+":checked"); 

        // tableau des coachs sélectionnés, initialisé à une longueur égale au nombre de coachs
        // init du tableau avec par défaut true pour chaque élément
        var coachsSelected = initTableau(coachs.liste.length, true);

        // appel aux différents filtres
        // on fait des ET logiques sur les différents filtres, car ça permet d'utiliser les filtres
        // ensemble. Par exemple : on pourrait sélectionner un coach de catégorie "développement perso",
        // qui soit ouvert le "mardi", sur le créseau "12h-14h".

        // nouvelle version, avec un filtre générique qui gère des numéros de filtre, et qui appelle
        // les fonctions spécifiques nécessaires. NB: le second paramètre est le n° du filtre.
        filtreGenerique(coachsSelected, 0)                       // filtre des catégories
        .then(function (coachsSelected){
            coachsSelected = filtreGenerique(coachsSelected, 1); // filtre des jours ouvrés
            return coachsSelected;
        })
        .then(function (coachsSelected){
            coachsSelected = filtreGenerique(coachsSelected, 2); // filtre des heures d'ouverture
            return coachsSelected;
        })
        .then(function (coachsSelected){
            coachsSelected = filtreGenerique(coachsSelected, 3); // filtre des prix de la 1ère séanec
            return coachsSelected;
        })
        .then(function (coachsSelected){
            // gestion de l'affichage ou non des markers des coachs, en fonction de leur état,
            // sélectionné ou non.
            affSelectedCoachs(coachsSelected);  
        })
        .catch(function(){
            // gestion des erreurs
            warnDebug("filtrage() : Il y a eu une erreur avec le filtrage");
        });
    } // function filtrage()

    //================================================================================================================
    // filtre générique
    //================================================================================================================
    function filtreGenerique(coachsAlreadySelected, numFiltre){
        /*-------------------------------------------------------------------------------------------------
        function filtreGenerique(coachsAlreadySelected)

        Cette fonction est un filtre générique, qui gère les cases à cocher d'un type de filtre donné, et qui 
        appelle une partie spécifique au besoin.

        Entrées : 
        ---------
        coachsAlreadySelected   : les coachs qui ont été sélectionnés par ailleurs, par d'autres filtres
        numFiltre               : Index du filtre à gérer (commence par 0).

        Sorties : 
        ---------
        coachsFinalySelected    : les coachs sélectionnés par l'ensemble des filtres appliqués jusque là.

        Notes : 
        -------
        Dans cette fonction : 
        coachsAlreadySelected   : les coachs qui ont été sélectionnés par ailleurs, par d'autres filtres 
        coachsSelected          : les coachs qui sont sélectionnés par ce filtre
        coachsFinalySelected    : les coachs sélectionnés par l'ensemble des filtres appliqués jusque là.

        ------------------------------------------------------------------------------------------------*/

        /*
        Notes de programmation : 
        NB: on reconnaît les checkboxes grâce au nom de classe commun à toutes les checkbox 
            des filtres (filtreCoachs.prefixeClasse), et au nom du filtre, "cat" pour le filtre des catégories de coachs.
        NB: au final, ce serait bien que cette référence au nom du filtre se fasse automatiquement par mapping avec
            la base de données (ORM).
        NB: le resolve et le reject ont été mis à l'intérieur du bloc if-else. En effet, sans ça, 
            le bloc if-else est traité comme une fonction asynchrone, et le resolve-reject est revoyé avant
            d'avoir le résultat à envoyer, avec du coup une promesse rejected.
        */

        // promesse, pour gérer le fonctionnement asynchrone, car les fonctions jQuery utilisées renvoient des
        // résultats en asynchrone.
        return new Promise((resolve, reject) =>{    
        
            // debug
            infoDebug("log", "filtreGenerique()");

            var nomFiltre = filtreCoachs.nom[numFiltre]; // nom du filtre à appliquer
            var coachsSelected;       // variables pour les coachs sélectionnés par ce filtre uniquemenbt
            var coachsFinalySelected; // variable pour les coachs sélectionnés au final
            var idCoachs;             // pour récupérer la liste d'id de coachs renvoyés par le filtre spécifique.

            $(function() {   // partie en jQuery    
                // Sélection de toutes les checkboxes "checked" du filtre de la catégorie
                var selection = $("."+filtreCoachs.prefixeClasse+nomFiltre+":checked");
            
                //txt de l'id du badge indiquant le nombre d'options cochées pour ce filtre
                var txtIdBadgeFiltre = "TLC-badge-filtre-" + numFiltre;
                                
                // maj du bagde du filtre indiquant le nombre de cases cochées
                var badgeNbreCasesCochees = $("#"+txtIdBadgeFiltre); // sélection du badge associé au filtre
                badgeNbreCasesCochees.text(''+selection.length); // maj du nombre de cases cochées
                infoDebug('test', txtIdBadgeFiltre);
                if(selection.length > 0){
                    badgeNbreCasesCochees.show();
                } else {
                    badgeNbreCasesCochees.hide();
                }
                
            
                // on n'applique le filtre que s'il y a des chechboxes du filtre qui sont activées
                if(selection.length > 0){

                    // debug
                    infoDebug("test", "filtreGenerique() => "+selection.length+" cases sont cochées");

                    // init du tableau avec par défaut false pour chaque élément
                    coachsSelected = initTableau(coachs.liste.length, false);

                    // boucle sur le nombre de cases cochées
                    selection.each(function(numSelect){
                        // numéro de l'option sélectionnée
                        var numOption = $(this).data('infos').numOption;
                        // debug
                        infoDebug("test", "filtreGenerique() : numOption = "+numOption);

                        // fonction spécifique
                        //====================
                        // Appel de la fonction qui gère la partie spécifique du filtre.
                        // le pointeur vers cette fonction est dans le tableau "filtreCoachs.fonction".
                        // On lui passe les paramètres (coachsSelected, numOption)
                        idCoachs = filtreCoachs.fonction[numFiltre](numOption, numFiltre);

                        // conversion de la liste d'id de coachs, en un vecteur de coachs sélectionnés ou non
                        coachsSelected = idCoachs2coachsSelected(coachsSelected, idCoachs);
                    }); // selection.each


                } else { // if(selection.length > 0)
                    // debug
                    infoDebug("test", "filtreGenerique() => pas de cases cochées");

                    // dans ce cas, on ne filtre pas et il faut donc mettre à true tous les élément du 
                    // tableau des coachs sélectionnés
                    // init du tableau avec par défaut true pour chaque élément
                    coachsSelected = initTableau(coachs.liste.length, true);
                } // if(selection.length > 0)

                // ET logique entre les coachs déjà sélectionnés et ceux qui viennent d'être sélectionné
                // pour obtenir les coachs qui sont sélectionnés au final.
                coachsFinalySelected = etTableaux(coachsAlreadySelected, coachsSelected);

                // debug
                // console.log("filtreGenerique(), numFiltre = "+numFiltre);
                // console.log(" - coachsAlreadySelected : ");
                // console.log(coachsAlreadySelected);              
                // console.log(" - coachsSelected : ");
                // console.log(coachsSelected);             
                // console.log(" - coachsFinalySelected : ");
                // console.log(coachsFinalySelected);               

                // gestion des résultats renvoyés, et notamment, des erreurs
                // pour ça, on test que le tableau des coachs sélectionnés contient bien des éléments.
                if (coachsSelected[coachsSelected.length-1] === undefined) {
                    reject("filtreGenerique() : coachsSelected === undefined");
                    //debug
                    warnDebug("filtreGenerique() : promesse rejetée");
                } else {
                    // renvoie le tableau des coachs finalement sélectionnés
                    resolve(coachsFinalySelected);
                    // debug
                    infoDebug("info", "filtreGenerique() : promesse résolue");
                }  // if        
            }); // $(function()
        }); // promise
    } // function filtreGenerique()

    //================================================================================================================
    // filtre spécifique pour les catégories de coachs
    //================================================================================================================
    function filtreSpecifiqueCat(numOption){

        // Cette fonction renvoie un tableau avec les ID des coachs sélectionnés.

        // debug
        infoDebug("log", "filtreSpecifiqueCat()");

        var idCoachs = [];  // tableau qui va recevoir les id des coachs sélectionnés

        // boucle for sur les offres de coaching (NB: un coach peut avoir plusieurs offres, dans des catégories différentes)
        for(numO = 0; numO < coachs.offres.length; numO++){
            if(coachs.offres[numO].ofc_cat == numOption){ // si l'offre est de la catégorie numOption
                // alors on ajoute l'ID du coach dont on traite l'offre, à la liste des coachs qu'on veut sélectionner
                idCoachs.push(coachs.offres[numO].coach_id); 
            } // if(coachs.offres ...)
        } // for(numO ...)

        // renvoi du résultat
        return idCoachs;
    } // function filtreSpecifiqueCat()

    //================================================================================================================
    // filtre spécifique pour les jours d'ouverture
    //================================================================================================================
    function filtreSpecifiqueJours(numOption, numFiltre){

        // Cette fonction renvoie un tableau avec les ID des coachs sélectionnés.

        // debug
        infoDebug("log", "filtreSpecifiqueJours()");

        // init
        //-----
        // liste des id des coachs qui exercent le jour n° numOption.
        var idCoachs = []; 
        // extraction du jour n°numOption
        var jour = filtreCoachs.options[numFiltre][numOption];
        // Tableau des index des coachs sélectionnés.
        var indexCoachs;

        // boucle sur les créneaux horaires
        //----------------------------------
        for(numCreneau = 0; numCreneau < coachs.creneaux.length; numCreneau++){
            // si le créneau est le bon jour
            if(coachs.creneaux[numCreneau].cr_jour == jour){
                // on ajout l'ID du coach auquel appartient le créneau, dans le tableau idCoachs,
                // qui contient la liste des ID des coachs qu'il faudra sélectionner
                idCoachs.push(parseInt(coachs.creneaux[numCreneau].coach_id));
            } // if(coachs.creneaux(...)
        } // for(numCreneau

        // renvoi du résultat
        //-------------------
        return idCoachs;
    } // function filtreSpecifiqueJours()

    //================================================================================================================
    // filtre spécifique pour les heures d'ouverture
    //================================================================================================================
    function filtreSpecifiqueHeures(numOption, numFiltre){

        // Cette fonction renvoie un tableau avec les ID des coachs sélectionnés.

        // debug
        infoDebug("log", "filtreSpecifiqueHeures()");

        // init
        //-----
        // liste des id des coachs dont un des créneau inclut, au moins en partie, le créneau n° numOption
        var idCoachs = []; 

        // extraction du créneau n° numOption
        var heureDeb = filtreCoachs.param[numFiltre].heureDeb[numOption];
        var heureFin = filtreCoachs.param[numFiltre].heureFin[numOption];

        // boucle sur les créneaux horaires
        for(numCreneau = 0; numCreneau < coachs.creneaux.length; numCreneau++){
            // extraction des heures de début et de fin de créneau et conversion en heures décimales (avec des 100èpmes d'heure)
            hCreneauDeb = parseFloat(coachs.creneaux[numCreneau].cr_heure_deb) + parseFloat(coachs.creneaux[numCreneau].cr_minutes_deb /60);
            hCreneauFin = parseFloat(coachs.creneaux[numCreneau].cr_heure_fin) + parseFloat(coachs.creneaux[numCreneau].cr_minutes_fin /60);
            // vérification si le créneau regardé est à cheval sur le créneau coché par l'utilisateur
            if((hCreneauDeb < heureFin)&&(hCreneauFin > heureDeb)){ // si c'est le cas
                idCoachs.push(parseInt(coachs.creneaux[numCreneau].coach_id)); // on ajoute l'ID du coach correspondant à la liste des coachs sélectionnés
            }
        } // for(numCreneau)

        // renvoi du résultat
        return idCoachs;
    } // function filtreSpecifiqueHeures()


    //================================================================================================================
    // filtre spécifique pour Le prix de la 1ère séance
    //================================================================================================================
    // En cours de modification 
    function filtreSpecifiquePrix(numOption, numFiltre){

        // Cette fonction renvoie un tableau avec les ID des coachs sélectionnés.

        // debug
        infoDebug("log", "filtreSpecifiquePrix()");

        // cette fonction met à true les numéros des coachs dont le prix de la 1ère séanec
        // correspond à l'option n° numOption.

        // init
        //-----
        // liste des id des coachs dont un des créneau inclut, au moins en partie, le créneau n° numOption
        var idCoachs = []; 
        var prixMin;
        var prixMax;

        // boucle for sur les offres de coaching (NB: un coach peut avoir plusieurs offres, dans des catégories différentes)
        for(numO = 0; numO < coachs.offres.length; numO++){
            // traitement séparé des 2 premières options
            if(numOption == 0){  // gratuité de la première séance
                if(coachs.offres[numO].ofc_gratuite_seance1 == "1"){ // si la 1ère séance est gratuite pour ce coach
                    // alors on ajoute l'ID du coach dont on traite l'offre, à la liste des coachs qu'on veut sélectionner
                    idCoachs.push(coachs.offres[numO].coach_id); 
                } // if(coachs
            } else if(numOption == 1){  // prix non renseigné
                if(coachs.offres[numO].ofc_prix == 0){ // si prix non renseigné
                    // alors on ajoute l'ID du coach dont on traite l'offre, à la liste des coachs qu'on veut sélectionner
                    idCoachs.push(coachs.offres[numO].coach_id); 
                } // if(coachs ...)
            } else { 
                // extraction des paramètres de prix de l'option n° numOption
                prixMin = filtreCoachs.param[numFiltre].prixMin[numOption];
                prixMax = filtreCoachs.param[numFiltre].prixMax[numOption];

                if((coachs.offres[numO].ofc_prix > prixMin) && (coachs.offres[numO].ofc_prix <= prixMax)){ // Si le prix d'une séance est dans la bonne fourchette
                    // alors on ajoute l'ID du coach dont on traite l'offre, à la liste des coachs qu'on veut sélectionner
                    idCoachs.push(coachs.offres[numO].coach_id); 
                } // if(coachs.offres ...)
            } // if(numOption)  => traitepment des différentes options
        } // for(numO ...)  

        // renvoi du résultat
        return idCoachs;
    } // function filtreSpecifiquePrix()

//###############################################################
// encarts coachs
//###############################################################

    //================================================================================================================
    // Création du block html pour faire un enccart de description d'un coach
    //================================================================================================================
    function createEncartCoach(numCoach){

        /*-------------------------------------------------------------------------------------
            function createEncartCoach(numCoach)
        
            Cette fonction crée un block html permettant de faire un encart de description
            d'un coach.
            
            Entrées : 
            ---------
            numCoach : le numéro du coach dans la liste des coachs à proximité.
                       NB: en fait il s'agit plutôt d'un index car le 1er de la liste est le numéro 0.
            
            Sorties : 
            ---------
            blocHtml : une string avec tout le texte pour ajouter l'encart du coach numCoach.
            
        -------------------------------------------------------------------------------------*/     

        var blocHtml;

        // debug
        infoDebug("log", "encartCoach()");


        // préparation du texte des catégories de coaching à partir des numéros d'offres
        //------------------------------------------------------------------------------

        // variables
        var numFiltreCat = 0; // numéro du filtre des catégories
        var categories = [];
        var numCat; 
        var txtCat;
        var prix;
        var devise;
        var duree;
        var grauiteSeance1;

        // recherche des offres du coach
        var numOffres = numCoach2numOffres(numCoach);

        // boucle for sur le nombre d'offres
        for(num = 0; num < numOffres.length; num++){
            numCat = coachs.offres[numOffres[num]].ofc_cat;  // numéro de la catégorie de coaching correspondant à l'offre
            categories.push(filtreCoachs.options[numFiltreCat][numCat]);
        }

        // préparation du lien vers la photo pour l'encart
        //------------------------------------------------
        var txtSrcImg  = chemin.racine + chemin.images + nomFic.jpg.prefixePhotoEncart + coachs.id[numCoach] + nomFic.jpg.suffixePhotoEncart;
        
        // préparation du titre de l'encart, avec le nom du coach et un lien vers la page du coach
        //----------------------------------------------------------------------------------------
        var txtNomCoach = '';
        txtNomCoach +=  '<div class="col">';
        txtNomCoach +=      '<h2 class="card-title ">';
        txtNomCoach +=          '<strong>';
        txtNomCoach +=              '<a class="TLC-encart-nom-coach statClic" sc-nom="encart-nomCoach" sc-lienSortie="true" sc-priorite="1" sc-param1="'+coachs.id[numCoach]+'" href="#">'+coachs.liste[numCoach].coach_prenom+' '+coachs.liste[numCoach].coach_nom+'</a>';
        txtNomCoach +=          '</strong>';
        txtNomCoach +=      '</h2>';
        txtNomCoach +=  '</div>'; // col


        // préparation du badge avec la note du coach
        //-------------------------------------------
        var note = coachs.liste[numCoach].coach_note;
        var txtBadgeNote = '';
        if((note>0)&(coachs.liste[numCoach].coach_option_aff_note == 1)){
            txtBadgeNote += '<p class="TLC-encart-badge-note text-right statClic" sc-nom="encart-BadgeNote" sc-lienSortie="false" sc-priorite="2" sc-param1="'+coachs.id[numCoach]+'" ><span class="badge badge-success"><strong>';
            txtBadgeNote +=     note+'/10';// note du coach
            txtBadgeNote += '</strong></span></p>';
        } else { // si note == 0
            txtBadgeNote += '<p>      </p>'; // NB: ce sont des alt-255 et non des espaces
        } // si note == 0
        

        // préparation du texte pour ajouter le tooltip
        var txtTooltip= ' data-toggle="tooltip" data-placement="left" ';
                        //+'data-template=\'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>\'';

        // préparation du bouton permettant de localiser le coach sur la carte
        //--------------------------------------------------------------------
        // NB: après avoir mis en place le bouton, on lui attribuera la fonction pour animer
        // le marker du coach sur la carte. Voir un peu plus bas.
        var txtButonLocate = '';
        txtButonLocate += '<p class="text-right TLC-encart-btn-loc">';
        txtButonLocate +=   '<a class="btn btn-primary statClic" sc-nom="encart-locCoach" sc-lienSortie="false" sc-priorite="2" sc-param1="'+coachs.id[numCoach]+'"  href="#TLC-container-carto" role="button" id="TLC-encart-bouton-marker-'+numCoach+'" '+txtTooltip+' title="localiser le coach sur la carte">';
        txtButonLocate +=       '<img src="'+chemin.racine + chemin.icones+nomFic.svg.boutonLocCoach+'" alt="Voir sur la carte" title="Voir sur la carte" style="width:32px;height:32px;">';
        txtButonLocate +=   '</a>';
        txtButonLocate += '</p>';


        // préparation du bouton permettant de voir les livres conseillés par ce coach
        //----------------------------------------------------------------------------
        var txtButonLivres = '';
        txtButonLivres += '<p class="text-right">';
        txtButonLivres +=   '<a class="TLC-encart-btn-livres btn btn-primary statClic" sc-nom="encart-boutonLivre" sc-lienSortie="true" sc-priorite="2" sc-param1="'+coachs.id[numCoach]+'"  href="#" role="button" id="TLC-encart-bouton-livres-coach-'+numCoach+'" data-toggle="tooltip" data-placement="left" title="Livres conseillés par le coach">';
        txtButonLivres +=       '<img src="'+chemin.racine + chemin.icones+nomFic.svg.boutonLivresConseilles+'" alt="Livres conseillés" title="Livres conseillés" style="width:32px;height:32px;">';
        txtButonLivres +=   '</a>';
        txtButonLivres += '</p>';


        // préparation des coordonnées du coach
        //-------------------------------------
        var txtCoord = '';

        // distance
        var txtDist = Math.round(coachs.dist[numCoach]*10)/10 + "Km";
        txtCoord += '<span>';
        txtCoord +=  ' ' + txtDist;
        txtCoord += '</span> <br>';

        
        // titre
        if(coachs.liste[numCoach].coach_title.length > 0) {
            txtCoord += '<img src="'+chemin.racine + chemin.icones+nomFic.svg.iconeTitreCoach+'" alt="titre :  : " title="titre" style="width:20px;height:20px;">';
            txtCoord += '<span>';
            txtCoord +=  ' ' + coachs.liste[numCoach].coach_title;
            txtCoord += '</span> <br>';
        } // if(titre)


        // commentaire libre 3 lignes
        if(coachs.liste[numCoach].coach_comment_3lines.length > 0) {
            txtCoord += '<p class="text-center">';
            //txtCoord +=  coachs.liste[numCoach].coach_comment_3lines;
            txtCoord +=  coachs.liste[numCoach].coach_comment_3lines.replace("\r", "<br>");
            txtCoord += '</p>';
        } // if(titre)

        // adresse
        txtCoord += '<adress>';
        txtCoord += '<img src="'+chemin.racine + chemin.icones+nomFic.svg.iconeAdresse+'" alt="adresse : " title="adresse" style="width:20px;height:20px;">';
        txtCoord += '<span>';
        txtCoord +=  ' ' + coachs.liste[numCoach].coach_adr;
        txtCoord += '</span> <br>';

        // tel
        txtCoord += '<img src="'+chemin.racine + chemin.icones+nomFic.svg.iconeTel+'" alt="tel :  : " title="tel" style="width:20px;height:20px;">';
        txtCoord += '<span>';
        txtCoord +=  ' ' + coachs.liste[numCoach].coach_tel;
        txtCoord += '</span> <br>';

        // page web perso
        if(coachs.liste[numCoach].coach_page_web_perso.length > 0){
            txtCoord += '<img src="'+chemin.racine + chemin.icones+nomFic.svg.iconePageWeb+'" alt="page web :  :  : " title="page web" style="width:20px;height:20px;">';
            txtCoord += '<span>';
            txtCoord +=  ' ' + coachs.liste[numCoach].coach_page_web_perso;
            txtCoord += '</span> <br>';
        } // if(page web perso)
        
        txtCoord += '</adress>';

        txtCoord += '<br>';
        txtCoord += '';
        txtCoord += '';
        txtCoord += '';

        // préparation du tableau permettant d'afficher les caractéristiques d'une offre
        //------------------------------------------------------------------------------
        var txtTableOffres = '';
        var txtOffres = '';

        // boucle for sur le nombre d'offres
        for(num = 0; num < numOffres.length; num++){
            // extration des infos nécessaires
            numCat          = coachs.offres[numOffres[num]].ofc_cat;
            txtCat          = filtreCoachs.options[0][numCat];
            prix            = coachs.offres[numOffres[num]].ofc_prix;
            devise          = coachs.offres[numOffres[num]].ofc_devise;
            duree           = coachs.offres[numOffres[num]].ofc_duree_seance;
            grauiteSeance1  = coachs.offres[numOffres[num]].ofc_gratuite_seance1;

            // Mise en forme de la durée
            //---------------------------
            var duree_h = Math.floor(duree); // nombre d'heures
            var duree_m = Math.round((duree-duree_h)*60); // nombre de minutes
            var txtDuree; // texte pour la durée
            var txtDureeMin; // texte pour la partie minutes

            // gestion du nombre de digits des minutes
            if(duree_m > 9){
                txtDureeMin = ''+duree_m;
            } else if(duree_m > 0){
                txtDureeMin = '0'+duree_m;
            } else {
                txtDureeMin = '00';
            }

            if(duree_h > 0){
                txtDuree = duree_h+'h';
                if(duree_m > 0){
                    txtDuree += txtDureeMin;
                }
            } else {
                txtDuree = txtDureeMin+"'";
            }
            
            // Mise en forme du prix
            //----------------------
            var txtPrix = '';
            if(prix > 0){
                var txtPrixPrincipal = '' + Math.round(prix); // pour les Euros
                var cts       = (prix - Math.round(prix))*100;  // centimes
                if(cts == 0){
                    txtPrix += txtPrixPrincipal + devise;
                } else if(cts < 10){
                    txtPrix += txtPrixPrincipal + '.0' + cts + devise;
                } else {
                    txtPrix += txtPrixPrincipal + '.' + cts + devise;
                }
            } // if(prix > 0)
                

            // mise en forme du séparateur prix / duree
            //-----------------------------------------
            var txtSepPrixDuree = '';
            if(prix != 0 && duree != 0){
                txtSepPrixDuree = ' / ';
            }
                
                
            // txtOffres    
            //---------------------------
            
            // blocs
            txtOffres +=    '<div class="border border-primary bg-light shadow-sm p-3 mb-5 bg-white rounded">';
            
            // blocs
            txtOffres +=    '<div class="row">';

            
            // prix et durée de séance
            txtOffres +=    '<div class="col">';
            txtOffres +=    '<span>';
            txtOffres +=        'séance : ' + txtPrix + txtSepPrixDuree + txtDuree + '<br>';
            txtOffres +=    '</span>';
            if(grauiteSeance1 == 1){ // gratuité de la 1ère séance
                txtOffres +=    '<span>';
                txtOffres +=        '1ère séance gratuite';
                txtOffres +=    '</span>';
            }
            txtOffres +=    '</div>'; // col
            
            // badge avec la catégorie
            txtOffres +=    '<div class="col">';
            txtOffres +=        '<p class="text-right">';
            txtOffres +=            '<span class="badge badge-success"><strong>';
            txtOffres +=                categories[num];
            txtOffres +=            '</strong></span>  '; 
            txtOffres +=        '</p>';
            txtOffres +=    '</div>'; // col            

            // blocs
            txtOffres +=    '</div>';
            
            // blocs
            txtOffres +=    '</div>';

        } // for(num)

        


        // préparation du bloc html permettant d'insérer l'encart du coach
        //----------------------------------------------------------------
        blocHtml = ''
         //+ '<!-- Zone avec les encarts pour les coachs -->'
         + '<div id="TLC-encart-coach-'+numCoach+'"> '
         +   ''
         //+   '<!-- container pour l'encart du coach -->'
         +   '<div class="container-fluid">'
         +     '<div class="row">'
         +       '<div class="col align-self-start">'
         //+         '<!-- card -->'
         +         '<div class="card mb-3 statClic" sc-nom="encart" sc-lienSortie="false" sc-priorite="1" sc-param1="'+coachs.id[numCoach]+'"  > '
         +           '<div class="row no-gutters "> '
         +              ''
         //+             '<!-- card header -->'
         +             '<div class="col-md-3 TLC-coach-card-header ">'
         +               '<img src="' + txtSrcImg + '" class="card-img" alt="photo" title="photo du coach" style="width:120px;height:150px;">' 
         +             '</div> ' //+'<!-- card header -->'
         +               ''
         //+             '<!-- colonne pour la partie body -->'
         +             '<div class="col-md-9 TLC-coach-card-body ">'
         //+               '<!-- container pour mettre 2 colonnes dans le body de la card -->'
         +               '<div class="container-fluid ">'
         +                 '<div class="row no-gutters">'
         //+                   '<!-- 1ère colonne du body de la card -->'
         +                   '<div class="col-9">'
         //+                     '<!-- card body -->'
         +                     '<div class="card-body ">'
         +                       '<div class="row">'
         +                           txtNomCoach // titre avec le nom du coach et un lien vers sa page web
         +                       '</div>' // row
         +                       txtCoord // coordonnées du coach
         //+                     '<!-- tableau avec les différentes infos -->'
         +                       txtOffres
         +                     '</div>'//+'<!-- card body -->'
         +                   '</div>'//+' <!-- 1ère colonne du body de la card --> '
         //+                    '<!-- 2ème colonne du body de la card -->'
         +                   '<div class="col-3">'
         +                     '<div class="card-body ">'
         +                       txtBadgeNote // badge avec la note du coach
         +                       txtButonLocate  // bouton permettant de localiser le coach sur la carte.
         +                       txtButonLivres // bouton permettant de voir les livres conseillés par ce coach
         +                     '</div>'
         +                   '</div>'//+' <!-- 2ème colonne du body de la card -->'
         +                 '</div>'//+' <!-- row -->'
         +               '</div>'//+' <!-- container pour mettre 2 colonnes dans le body de la card -->'
         +             '</div>'//+' <!-- colonne pour la partie body -->'
         +           '</div>'//+' <!-- row -->'
         +         '</div>'//+' <!-- card -->'
         +       '</div>'//+' <!-- col -->'
         +     '</div>'//+' <!-- row -->'
         +   '</div>'//+' <!-- container pour l'encart du coach -->'

         // renvoi du bloc html permettant d'ajouter l'encart sur la page.
         return(blocHtml);
    } // function createEncartCoach()   
    //================================================================================================================
    // ajoute l'encart d'un des coachs, dans la liste des coachs à proximité
    //================================================================================================================
    function addUnEncartCoach(numCoach){        

        /*-----------------------------------------------------------------------
            addUnEncartCoach()

            Cette fonction affiche un encart pour chaque coach de la liste.
            
            Entrées : 
            ---------
            numCoach : le numéro du coach dont il faut afficher l'encart, dans 
                       la liste des coachs. NB: le 1er numéro est 0.
        ------------------------------------------------------------------------*/
        
        // création du bloc html permettant d'ajouter l'encart sur la page
        var blocHtml = createEncartCoach(numCoach);

        // ajout du bloc pour créer la carte et ajout d'un évènement appelé lors du clic
        // sur le bouton pour localiser le coach
        
        $(function() {   // partie en jQuery

            // recherche de l'emplacement pour ajouter l'encart
            // et ajout du bloc html avec l'encart du coach
            $("#TLC-encarts-coachs").append(blocHtml);

            // enregistre le numéro du coach dans des données associées au bouton
            $("#TLC-encart-bouton-marker-"+numCoach).data('numCoach', numCoach);

            // ajout de l'appel à la fonction pour animer le marker correspondant au coach
            addEventEncartCoachs(numCoach);
        }); // $(function()  // partie en jQuery
    } // function addUnEncartCoach()

    //================================================================================================================
    // Tous les encarts pour les coachs
    //================================================================================================================
    function addEncartCoachs(){

        /*-----------------------------------------------------------------------
            addEncartCoachs()

            Cette fonction affiche un encart pour chaque coach de la liste.
        ------------------------------------------------------------------------*/      

        // debug
        infoDebug("log", "addEncartCoachs()");

        // boucle sur tous les coachs de la liste
        for(numCoach = 0; numCoach < coachs.liste.length; numCoach++){
            addUnEncartCoach(numCoach);
        }
        
                
    } // function addEncartCoachs()

    //================================================================================================================
    // Pour cacher l'encart d'un Coach
    //================================================================================================================
    function hideEncartCoach(numCoach){

        /*--------------------------------------------------------------------------
            Cette fonction cache l'encart dédié au coach n° numCoach.
            Elle est notamment nécessaire pour que le filtrage se répercute
            sur les encarts des coachs.
        --------------------------------------------------------------------------*/
        
        var selection;

        // debug
        infoDebug("log", "hideEncartCoach()");

        $(function() {   // partie en jQuery

            // recherche de l'emplacement pour ajouter l'encart
            selection = $("#TLC-encart-coach-"+numCoach);

            // cahce l'encart l'encart
            selection.hide();

        }); // $(function()  // partie en jQuery        
    } // function hideEncartCoach()

    //================================================================================================================
    // Pour rendre visible l'encart d'un Coach
    //================================================================================================================
    function showEncartCoach(numCoach){
        /*-----------------------------------------------------------------------
            Cette fonction cache l'encart dédié au coach n° numCoach.
            Elle est notamment nécessaire pour que le filtrage se répercute
            sur les encarts des coachs.
        -----------------------------------------------------------------------*/
        
        var selection;

        // debug
        infoDebug("log", "showEncartCoach()");

        $(function() {   // partie en jQuery

            // recherche de l'emplacement pour ajouter l'encart
            selection = $("#TLC-encart-coach-"+numCoach);

            // cahce l'encart l'encart
            selection.show();

        }); // $(function()  // partie en jQuery        
    } // function showEncartCoach()

//###############################################################
// Tri des encarts
//###############################################################

    //================================================================================================================
    // tri d'un tableau
    //================================================================================================================
    function triGeneral(critere){
        /*--------------------------------------------------------------------------------------------------------
            triGeneral(critere)
            
            Cette fonction, appelée par les différentes fonctions spécifiques de tri, se sert du critère passé en paramètre,
            pour afficher les encarts selon l'ordre croissant du critère.
            
            Entrées : 
            ---------
                critere : un tableau avec les valeurs du critère de classement
                
            Sorties : 
            ---------
                pas de sorties.
                
            Notes : 
            -------
            Validé le 2019 08 02
        --------------------------------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "triGeneral()");
    
        // NB: l'affiche du spinner est fait directement dans les fonction qui appelent le triGeneral()
        //     afin d'éviter qu'un changement d'adresse n'active le spinner car la fonction triGeneral()
        //     est appelée dans ce cas.

        // Après un petit instant (pour indiqquer à l'utilisateur que sa demande esr bien prise en compte) 
        setTimeout(function(){
            // masquage du spinner
            $("#TLC-navbartri-spinner").hide();
            
            // tri des encarts selon le critère
            //----------------------------------
            var ordre = sort(critere);
            
            // on détache tous les encarts par ordre de numCoach
            //--------------------------------------------------
            var encart = []; // tableau qui va recueillir les pointeurs vers les encarts détachés
            for(numCoach = 0; numCoach < coachs.liste.length; numCoach++){
                encart.push($("#TLC-encart-coach-"+numCoach).detach());  // on détache l'encart du coach n° numCoach
            } // for(numCoach...)
            
            // réaffichage des encarts dans le bon ordre
            //-------------------------------------------
            for(num = 0; num < coachs.liste.length; num++){
                $("#TLC-encarts-coachs").append(encart[ordre[num]]);
            } // for(numEncart...)
            
        }, 400);  // setTimeout
    } // function triGeneral()
    
    //================================================================================================================
    // Tri des encarts par prix progressifs
    //================================================================================================================
    function triePrix(){
        /*------------------------------------------------------------------------------------------------
            function triePrix()
            
            Cette fonction trie les encarts des coachs par prix progressifs.
            
            pas d'entrées ni sorties.
        ------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "triePrix()");
        
        // création du tableau du critère de tri
        //-----------------------------------------
        var critere = [];
        var numOffres;
        var prixOffre;
        for(numCoach = 0; numCoach < coachs.liste.length; numCoach++){
            //debug
            //critere.push(numCoach);
            // extraction des offres de ce coach
            numOffres = numCoach2numOffres(numCoach);
            // extraction des prix
            var prix = [];
            for(numO = 0; numO < numOffres.length; numO++){
                prixOffre = coachs.offres[numOffres[numO]].ofc_prix;
                if(prixOffre > 0){  // si le coahc a renseigné le prix
                    prix.push(prixOffre);
                } else {  // si le coach n'a pas renseigné le prix
                    if(coachs.offres[numOffres[numO]].ofc_gratuite_seance1){ // si la première séance est gratuite
                        // alors le coach sera classé devant ceux dont la première séance n'est pas gratuite
                        prix.push(888888); // ON met un grand chiffre pour que ce soit derrière les coachs dont le prix de l'offre est indiqué
                                           // mais mais élevé que ceux qui n'ont pas renseigné le prix et dont la première séance n'est pas gratuite.
                    } else {
                        prix.push(999999);
                    } // if(coachs...)
                } // if(coachs...) - else
            } // for(numO..)
            // extraction du prix mini pour les offres de ce coach
            // NB: le prix est sous forme d'une chaîne de caractères, c'est pourquoi il faut le convertir en nombre.
            critere.push(Math.min(parseFloat(prix)));
        } // for(numCoach...)
        
        // affichage des encarts dans l'ordre des valeurs croissantes du critère
        //=======================================================================
        // affichage du spinner indiquant qu'on a bien pris en compte une action utilisateur
        // NB: il sera ensuite caché par la fonction triGeneral()
        $("#TLC-navbartri-spinner").show();
        triGeneral(critere);
    } // function triePrix()
    

    //================================================================================================================
    // Tri des encarts selon les distances progressives à l'utilisateur
    //================================================================================================================
    function trieDistances(){
        /*------------------------------------------------------------------------------------------------
            function trieDistances()
            
            Cette fonction trie les encarts des coachs par distances progressives.
            
            pas d'entrées ni sorties.
        ------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "trieDistances()");
        
        // affichage des encarts dans l'ordre des valeurs croissantes du critère
        //=======================================================================
        // affichage du spinner indiquant qu'on a bien pris en compte une action utilisateur
        // NB: il sera ensuite caché par la fonction triGeneral()
        $("#TLC-navbartri-spinner").show();     
        triGeneral(coachs.dist);
    } // trieDistances()
    
    //================================================================================================================
    // Tri des encarts selon les distances progressives à l'utilisateur
    //================================================================================================================
    function trieNotes(){
        /*------------------------------------------------------------------------------------------------
            function trieNotes()
            
            Cette fonction trie les encarts des coachs par Notes dégressives.
            
            pas d'entrées ni sorties.
        ------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "trieNotes()");
        
        // création du tableau du critère de tri
        //-----------------------------------------
        var critere = [];
        var note, optionAffNote;
        for(numCoach = 0; numCoach < coachs.liste.length; numCoach++){
            note = coachs.liste[numCoach].coach_note; // note du coach
            optionAffNote = coachs.liste[numCoach].coach_option_aff_note; // option d'affichage de la note du coach
            if(optionAffNote == 1){ // si option d'affichage de la note
                // alors on met bien la note. En fait, on met 10-note pour que les notes soient par ordre décroissantes
                critere.push(10-note);
            } else { // si pas d'option d'affichage de la note
                // alors on met 10 pour que tous les coachs sans note ou qui ne veulent pas l'afficher soient ensemble.
                // et soient à la fin
                critere.push(10);
            } // if(optionAffNote
                
        } // for(numCoach...)

        // affichage des encarts dans l'ordre des valeurs croissantes du critère
        //=======================================================================
        // affichage du spinner indiquant qu'on a bien pris en compte une action utilisateur
        // NB: il sera ensuite caché par la fonction triGeneral()
        $("#TLC-navbartri-spinner").show();     
        triGeneral(critere);
    } // trieNotes()
    
//###############################################################
// recherche
//###############################################################
    //================================================================================================================
    // recherche d'un coach dans la base de données
    //================================================================================================================
    function rechercheCoach(){
        /*--------------------------------------------------------------
            function rechercheCoach()

            Cette fonction est appelée lors d'une saisie dans le champ de
            recherche d'un coach, ou au clic sur le bouton de recherche du
            coach.
            Elle permet de rechercher un coach dans la base de données.
            et de l'afficher.
            Si le coach est trouvé : 
             - changement de localisation (loc centrée sur le coach, mais
               pas exactement, pour que le marker de l'utilisateur ne se
               superpose pas à celui du coach).
             - modification du titre de la zone d'accueil
             - chargement des coachs à proximité
             - affichage des encarts
            Si le coach n'est pas trouvé, affichage d'une modal l'indiquant
        ---------------------------------------------------------------*/

        infoDebug("log", "rechercheCoach()");

        // extraction du contenu du champ de recherche
        var searchString = $("#TLC-champ-recherche-coach").val();

        // si le champ de recherche est vide
        if(searchString.length == 0){
            infoDebug("info", "rechercheCoach() : champ de recherche vide");
            return; // on quitte la fonction
        } // si le champ de recherche est vide

        // si même recherche que précedemment 
        // (même champ de recherche et même type de recherche)
        if((searchString == recherche.lastCoachSearch) && (recherche.lastSearchType == "coach")){
            infoDebug("info", "rechercheCoach() : même recherche que précedemment");            
            return;
        }  // si même recheche que précedemment

        // à partir d'ici, on a bien une nouvelle recherche donc
        infoDebug("info", "rechercheCoach() : nouvelle recherche");

        // MAJ des infos de la dernière recherche
        recherche.lastCoachSearch = searchString;
        recherche.lastSearchType  = "coach";

        // maj du nombre de recherches
        recherche.nbre++;

        // appel de la fonction ajax qui va demander au serveur si 
        // le coach existe dans la base de données
        demandeRechercheCoach(searchString);

        // réinitialise le champ de recherche
        $("#TLC-champ-recherche-coach").val("");
    } // function rechercheCoach()

//###############################################################
// Evènements
//###############################################################

    //==========================================================================
    // ajout des évènements sur la page
    //==========================================================================
    function addEventInit(){
        /*------------------------------------------------------------------------------------------------------------------------
            function addEventInit()
            
            Cette fonction permet d'ajouter des évènements associés à des éléments de la page
            
            Pas d'entrées ni de sorties
        ------------------------------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "addEventInit()");
        
        // évènement à lancer en cas de changement de la taille de l'écran
        window.onresize = initZoneAction;

        $(function () {  // partie en jQuery

            // boutons de choix (livres, coachs, évènements, ...)
            //===================================================
            $(".TLC-bouton-choix").click(function(){
                // suppression de la calsse "active" pour l'ensemble des boutons du choix
                $(".TLC-bouton-choix").attr("class", "list-group-item list-group-item-action TLC-bouton-choix");
                // le bouton cliqué passe en "active"
                $(this).attr("class", "list-group-item list-group-item-action TLC-bouton-choix active"); 
            }); // fonction appelée lors du clic sur un des boutons de choix

            // boutons de gestion de la carte
            //===============================
            // ajout de l'évènement à lancer lors d'un clic sur le bouton pour cacher la carte
            $("#TLC-button-carte-off").click(hideMap);

            // ajout de l'évènement à lancer lors d'un clic sur le bouton pour afficher la carte
            $("#TLC-button-carte-on").click(showMap);

            // ajout de l'évènement à lancer lors d'un clic sur le bouton pour centrer la carte sur l'utilisateur
            $("#TLC-button-carte-center-user").click(centreCarteSurUser);

            // assignation de fonction pour le champ de changement d'adresse
            //==============================================================
            $("#TLC-form-map-adresse").submit(function(event){
                infoDebug("test", "submit du changement d'adresse");
                // affichage du spinner à côté du champ de recherche d'un coach
                $('#TLC-spinner-champs-recherche').show(); 
                // récupération de l'adresse à géo-coder
                loc.adresseAGeocoder = $("#TLC-map-adresse").val();
                // changement d'adresse
                changeLoc(); 
                // pour éviter que le formulaire ne soit soumis et que la page soit rechargée
                event.preventDefault(); 
                // réinitialise le champ d'adresse
                $("#TLC-map-adresse").val("");
                // gestion du spinner
                $(function(){
                    setTimeout(function(){
                        $('#TLC-spinner-champs-recherche').hide(); // affichage du spinner à côté du champ de recherche
                    }, 700);
                }); // $(function...)
            }); // $("#TLC-form-map-adresse")
            // NB: le gestionnaire ci-dessous permet d'être plus réactif car il permet le géocodage
            // et la mise à jour de la carte pendant la frappe, par contre, ça consomme beaucoup plus de
            // geocodage et de cartes => enlevé pour l'instant
            //document.getElementById('TLC-map-adresse').addEventListener('input', googGeoCod);

            // Champ de recherche d'un coach
            //==============================
            // $("#TLC-champ-recherche-coach").change(rechercheCoach);
            $("#TLC-form-recherche-coach").submit(function(event){
                infoDebug("test", "submit de la recherche d'un coach");
                rechercheCoach();       // recherche du coach dans la base de données
                event.preventDefault(); // pour éviter que le formulaire ne soit soumis et que la page soit rechargée
            });
            
            
            // Boutons de la navbar de tri
            //============================
            $("#TLC-trie-prix").click(triePrix);
            $("#TLC-trie-dist").click(trieDistances);
            $("#TLC-trie-note").click(trieNotes);


            // évènements liés à l'enregistrement de statistiques
            //===================================================
            // ajoute des évènement dédiés sur tous les éléments de classe .statClic pour lesquels
            // on n'avait pas encore ajouté l'évènement de stat.
            majStatClic();

        }); // partie en jQuery 
    } // function addEventInit()

    //==========================================================================
    // ajout des évènements associés au filtre
    //==========================================================================
    function addEventFiltre(){
        /*------------------------------------------------------------------------------------------------------------------------
            function addEventFiltre()
            
            Cette fonction permet d'ajouter des évènements associés aux éléments du filtre
            
            Pas d'entrées ni de sorties
        ------------------------------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "addEventFiltre()");

        $(function () {  // partie en jQuery

            // ajout d'un évènement appelé au changement d'une des checkboxes d'un des filtres.
            //=================================================================================
            // L'évènement lance la fonction filtrage()
            $("."+filtreCoachs.ClasseCheckbox).change(function () {

                // debug
                infoDebug("log", "évènement appelé lorsque l'utilisateur coche une case d'un filtre");
                console.log($(this).data());

                // extraction des infos de la case à cocher, et notamment de l'id du spinner
                var infos = $(this).data('infos');
                var txtIdSpinner = infos.txtIdSpinner;

                // affichage de la modal indiquant qu'on a bien pris en compte une action utilisateur
                $('#'+txtIdSpinner).show();

                // Après un petit instant (pour indiqquer à l'utilisateur que sa demande esr bien prise en compte) 
                setTimeout(function(){
                    $('#'+txtIdSpinner).hide();
                    filtrage(); // appel à la fonction de filtrage
                }, 400);
            }); // ajout d'un évènement au changement d'état de la case à cocher

        }); // partie en jQuery 
    } // function addEventFiltre()

    //==========================================================================
    // ajout des évènements pour les encarts coachs
    //==========================================================================
    function addEventEncartCoachs(numCoach){
        /*------------------------------------------------------------------------------------------------------------------------
            function addEventEncartCoachs()
            
            Cette fonction permet d'ajouter des évènements associés aux encarts des coachs
            
            Entrées : 
            ---------
                numCoach : numéro du coach (dans la liste des coachs à proximité).
        ------------------------------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "addEventEncartCoachs()");

        $(function () {  // partie en jQuery

            // ajout de l'appel à la fonction pour animer le marker correspondant au coach
            //============================================================================
            $("#TLC-encart-bouton-marker-"+numCoach).click(animateMarkerCoach);

        }); // partie en jQuery 
    }  // function addEventEncartCoachs()

    //==========================================================================
    // ajout des évènements pour les markers coachs sur la carte
    //==========================================================================
    function addEventMarkersCoachs(numCoach){
        /*------------------------------------------------------------------------------------------------------------------------
            function addEventMarkersCoachs()
            
            Cette fonction permet d'ajouter des évènements associés aux markers coachs
            
            Entrées : 
            ---------
                numCoach : numéro du coach (dans la liste des coachs à proximité).
        ------------------------------------------------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "addEventMarkersCoachs()");

        $(function () {  // partie en jQuery
            
            // ajout d'un gestionnaire d'évènement au clic sur le marker du coach
            //===================================================================
            carto.listeMarkers[numCoach].addListener('click', clickSurMarkerCoach);

        }); // partie en jQuery 
    } // function addEventMarkersCoachs()

//###############################################################
// initialisations
//###############################################################

    //================================================================================================================
    // initialisations de la zone action
    //================================================================================================================
    function initZoneAction(){
        /*------------------------------------------------------------------------------------------------------------
            function initZoneAction()
            
            Récupère la taille écran, et en fonction del a taille de l'écran, rend visible ou non les 
            zones dédiées aux actions.
        ------------------------------------------------------------------------------------------------------------*/
        
        // debug
        infoDebug("log", "initZoneAction()");
    
        // récupère les données sur la taille de l'écran
        env.ecran.largeur  = $(window).width();
        env.ecran.hauteur  = $(window).height();
        
        // partie en jQuery
        $(function () {         
            // si écran > à celui d'un téléphone portable
            if(env.ecran.largeur > env.ecran.md){
                // dépliage des différents collapses 
                $("#collapseTri").collapse('show');
                $("#collapseRechercher").collapse('show');
                $("#collapseCarte").collapse('show');
                $("#collapseFiltrage").collapse('show');
            } // si écran > à celui d'un téléphone portable
            else{ // si petit écran
                // repliage des différents collapses 
                $("#collapseTri").collapse('hide');
                $("#collapseRechercher").collapse('hide');
                $("#collapseCarte").collapse('hide');
                $("#collapseFiltrage").collapse('hide');
            } // si petit écran
        }); // partie en jQuery

    } // function initZoneAction()
    
    //================================================================================================================
    // initialisations de la page
    //================================================================================================================
    function initialisations(){

        /*--------------------------------------------------------------
        function initialisations()

        Cette fonction se charge de toutes les initialisations à faire
        au lancement du programme.
        ---------------------------------------------------------------*/

        // debug
        infoDebug("log", "initialisations()");

        // init des statistiques avec le nom de cette page et sa version
        initStats("pageCoachs", 1); 
        
        // gestion de la zone action, en fonction de la taille de l'écran
        initZoneAction();

        // ajout des filtres à la page
        addFiltres();    
    
        // affiche un diagnostique en cas d'erreur ajax
        ajaxDebugOn();

        // initialisation des évènements associés aux éléments de la page
        addEventInit();

        var testA;

        // partie en jQuery
        $(function () {  

            // gestion des apparences à l'initialisation
            //------------------------------------------
            // init des tooltips
            $('[data-toggle="tooltip"]').tooltip().tooltip({delay: { "show": 500, "hide": 100 }}); 
            
            // masquage du spinner de la navBar de tri
            $("#TLC-navbartri-spinner").hide();
            
            // cache le bouton pour afficher la carte, car par défaut elle est affichée
            $("#TLC-button-carte-on").hide();
            
            // cache les spinners des zones de recherche
            $("#TLC-spinner-champs-recherche").hide();
        }); // partie en jQuery
        
        // indique la fin de l'initialisation
        fonctionnement.initOK = true;
        
    } //function initialisations()

//###############################################################
// programme principal
//###############################################################

    // lancement des initialisations
    initialisations();

    //NB: La fonction initMap() est appelée par googleAPI dès qu'il a fini de vérifier la clef
    


