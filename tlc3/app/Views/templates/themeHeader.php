<!DOCTYPE html>

<head>

	<!-- Basic Page Needs
================================================== -->
	<title>TOUSLESCOACHS.COM</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
================================================== -->
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/main-color.css" id="colors">

</head>

<body>

	<!-- Wrapper -->
	<div id="wrapper">

		<!-- Header Container
================================================== -->
		<header id="header-container">

			<!-- Header -->
			<div id="header">
				<div class="container">

					<!-- Left Side Content -->
					<div class="left-side">

						<!-- Logo -->
						<div id="logo">
							<a href="index.html"><img src="/images/logo.png" alt=""></a>
						</div>

						<!-- Mobile Navigation -->
						<div class="mmenu-trigger">
							<button class="hamburger hamburger--collapse" type="button">
								<span class="hamburger-box">
									<span class="hamburger-inner"></span>
								</span>
							</button>
						</div>

						<!-- Main Navigation -->
						<nav id="navigation" class="style-1">
							<ul id="responsive">


								<li><a href="/">Accueil</a>
								</li>

								<li><a href="/coach">Coachs</a>
								</li>

								<li><a href="/livre">Livres</a>
								</li>

								<li><a href="/evenement">Evènements</a>
								</li>

								<li><a href="/formation">Formations de coaching</a>
								</li>

							</ul>
						</nav>
						<div class="clearfix"></div>
						<!-- Main Navigation / End -->
					</div>
					<!-- Left Side Content / End -->


					<!-- Right Side Content / End -->
					<div class="right-side">
						<div class="header-widget">

							<?php if (isset($_SESSION['first_name'])) { ?>
								<div class="user-menu">
									<div class="user-name"><span><img src="<?= $_SESSION['picture'] ?>" alt=""></span><?= "$_SESSION[first_name] $_SESSION[last_name]" ?></div>
									<ul>
										<li><a href="/profil"><i class="sl sl-icon-settings"></i>Mon compte</a></li>
										<?php if($_SESSION['role_id'] == 1){
											echo "<li><a href='/crud'><i class=' im im-icon-Administrator'></i> Espace admin</a></li>";
										};
										?>
										<li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
									</ul>
								</div>
							<?php
							} else {
								?>
								<a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Se connecter</a>
							<?php
							}
							?>

						</div>
					</div>
					<!-- Right Side Content / End -->

					<!-- Sign In Popup -->
					<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

						<div class="small-dialog-header">

							<h3>Se connecter</h3>
						</div>

						<!--Tabs -->
						<div class="sign-in-form style-1">

							<ul class="tabs-nav">
								<li class=""><a href="#tab1">Se connecter</a></li>
								<li><a href="#tab2">S'inscrire</a></li>
							</ul>

							<div class="tabs-container alt">

								<!-- Login -->
								<div class="tab-content" id="tab1" style="display: none;">
									<form method="post" class="login" action="/login">

										<?= csrf_field() ?>
										<p class="form-row form-row-wide">
											<label for="email">Adresse email:
												<i class="im im-icon-Male"></i>
												<input type="text" class="input-text" name="email" id="email" value="" required />
											</label>
										</p>

										<p class="form-row form-row-wide">
											<label for="password">Mot de passe:
												<i class="im im-icon-Lock-2"></i>
												<input class="input-text" type="password" name="password" id="password" required />
											</label>
											<span class="lost_password">
												<a href="#">Mot de passe oublié?</a>
											</span>
										</p>

										<div class="form-row">
											<input type="submit" class="button border margin-top-5" name="login" value="Login" />
											<div class="checkboxes margin-top-10">
												<input id="remember-me" type="checkbox" name="check">
												<label for="remember-me">Se souvenir de moi</label>
											</div>
										</div>

									</form>
								</div>

								<!-- Register -->
								<div class="tab-content" id="tab2" style="display: none;">

									<form method="post" class="register" action="/register" oninput='password1.setCustomValidity(password2.value != password1.value ? "Les mots de passe ne correspondent pas." : "")'>

										<?= csrf_field() ?>

										<p class="form-row form-row-wide">
											<label for="last_name">Nom:
												<i class="im im-icon-Male"></i>
												<input type="text" class="input-text" name="last_name" id="last_name" value="" required />
											</label>
										</p>

										<p class="form-row form-row-wide">
											<label for="first_name">Prénom:
												<i class="im im-icon-Male"></i>
												<input type="text" class="input-text" name="first_name" id="first_name" value="" required />
											</label>
										</p>

										<p class="form-row form-row-wide">
											<label for="email2">Adresse mail:
												<i class="im im-icon-Mail"></i>
												<input type="email" class="input-text" name="email" id="email2" value="" required />
											</label>
										</p>
										<p class="form-row form-row-wide">
											<label for="roles">Je suis un:
												<select name="roles" id="roles" required>
													<option></option>
													<option value="3">Utilisateur</option>
													<option value="2">Coach</option>
												</select>
											</label>
										</p>

										<p class="form-row form-row-wide">
											<label for="password1">Mot de passe:
												<i class="im im-icon-Lock-2"></i>
												<input class="input-text" type="password" name="password1" id="password1" required />
											</label>
										</p>

										<p class="form-row form-row-wide">
											<label for="password2">Confirmation du mot de passe:
												<i class="im im-icon-Lock-2"></i>
												<input class="input-text" type="password" name="password2" id="password2" />
											</label>
										</p>

										<input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

									</form>
								</div>

							</div>
						</div>
					</div>
					<!-- Sign In Popup / End -->

				</div>
			</div>
			<!-- Header / End -->
		</header>

		<div class="clearfix"></div>
		<?php if (isset($_SESSION['error'])) { ?>
			<div class="popup error">
				<p><?= $_SESSION['error'] ?></p>
				<button class=" btn-popup" alt="close popup"></button>
			</div>
		<?php };
		if (isset($_SESSION['success'])) { ?>
			<div class="popup success">
				<p><?= $_SESSION['success'] ?></p>
				<button class=" btn-popup" alt="close popup"></button>
			</div>
		<?php } ?>




		<!-- Header Container / End -->