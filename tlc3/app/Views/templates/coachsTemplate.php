<!doctype html>
<html lang="fr">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="description" content="Trouvez tous les coachs à proximité, en fonction de ce qui vous intéresse (développement personnel, santé, sport, business, psychologie, ...), consultez leurs fiches détaillées, leurs coordonnées, leurs horaires, leurs prix ..." />


        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- mes styles perso -->
        <link rel="stylesheet" href="public/css/coachsTemplate.css">

        <!-- titre de l'onglet -->
        <title>TousLesCoachs.com : Tous les coachs à proximité </title>
    </head>
    <body>

        <!-- Indication à afficher si le javascript est coupé --> <!-- -->
        <noscript class="alert alert-warning" role="alert">
                Cette page ne peut pas fonctionner correctement sans Javascript <br>
                Pour activer le javascript, ouvrez les préférences de votre navigateur et autorisez le javascript
        </noscript> <!-- Indication à afficher si le javascript est coupé -->
        
        <!-- variables d'environnement qui seront utilisées par la partie javascript -->
        <div>
            <p hidden id="TLC-ville-init"></p> <!-- ville qui servira de localisation initiale, si pas de ville, utilisation de l'API géoloc -->
        </div> <!-- variables d'environnement qui seront utilisées par la partie javascript -->

        <!-- Menu principal (navBar) -->
        <nav class="navbar fixed-top navbar-dark bg-dark" >
            <span class="TLC-logo" ><strong>TousLesCoachs.com      </strong></span> <!-- "navbar-brand" NB: il y a un alt-255 pour forcer à mettre un espace -->
            <!-- éléments de la barre de menu -->
            <!-- juste pour forcer la mise en page -->
            <ul class="navbar-nav mr-auto">
                <span>   </span> <!-- NB: il y a un alt-255 pour forcer à mettre un espace -->
            </ul> <!-- juste pour forcer la mise en page -->
        
            <!-- Bouton de login -->
            <button type="submit" class="nav-item btn btn-outline-primary mr-2 my-2 my-sm-0 statClic" sc-nom="bouton-login" sc-lienSortie="" sc-priorite="2"  lang="en">Login</button>
        </nav> <!-- Menu principal (navBar) -->   
    
        <!-- debug : affichages d'infos pour débugger -->
        <div id="TLC-aff-infos-debug"> 
        </div> <!-- debug : affichages d'infos pour débugger -->
    
        <!-- Accroche -->
        <!--
        <div>
            <h2 class="text-center"> Tous les coachs près de chez vous </h2>
        </div> --> <!-- Accroche -->
    
        <!-- bandeau photos -->
        <div>
            <!-- <span>Bandeau photo</span> -->
        </div> <!-- bandeau photos -->


         <!-- info Window --> <!-- NB : fenêtre appelée lors d'un clic sur marker coach--> 
        <div style="display: none" >
          <div id="info-content">
          </div>
        </div> <!-- info Window -->
  
        <!-- choix, accueil -->
        <div class="container-fluid px-0 mx-0"> 
            <div class="row align-items-center no-gutters">
                <!-- col du choix d'affichage -->
                <div class="col-md-4 align-self-start " id="TLC-col-choix-aff">
            <!-- choix d'affichage -->
            <nav class="row justify-content-start no-gutters">      
                <div class="col-6">
                    <strong>
                        <a class="list-group-item list-group-item-action TLC-bouton-choix statClic" sc-nom="bouton-choix" sc-typeParam1="text"  sc-lienSortie="" sc-priorite="2"    href="#body" aria-controls="livres">Livres</a>
                    </strong>
                </div>  <!-- col -->
                <div class="col-6">
                    <strong>
                        <a class="list-group-item list-group-item-action TLC-bouton-choix active  statClic"  sc-nom="bouton-choix" sc-typeParam1="text" sc-lienSortie="" sc-priorite="2"  href="#body" aria-controls="Coachs">Coachs</a>
                    </strong>
                </div>  <!-- col -->
                <div class="col-6">
                    <strong>
                        <a class="list-group-item list-group-item-action TLC-bouton-choix statClic" sc-nom="bouton-choix" sc-typeParam1="text" sc-lienSortie="" sc-priorite="2"       href="#body"  aria-controls="Evènements">Evènements</a>
                    </strong>
                </div>  <!-- col -->
                <div class="col-6">
                    <strong>
                        <a class="list-group-item list-group-item-action TLC-bouton-choix statClic" sc-nom="bouton-choix" sc-typeParam1="text" sc-lienSortie="" sc-priorite="2"     href="#body" aria-controls="Formation">Formations</a>
                    </strong> 
                </div>  <!-- col -->
             </nav> <!-- row -->
                </div> <!-- col du choix d'affichage -->
                <!-- col accueil -->
                <div class="col-md-8  align-self-center TLC-col-zone-carto">  
            <h1 class="text-center" id="TLC-accueil-titre">Coachs à proximité </h1>
                </div> <!-- col accueil -->
            </div> <!-- row -->
        </div><!-- choix, accueil -->

        <!--  Zone action (filtre, tri, recherche), carte,  encarts coachs-->
        <div class="container-fluid" id="TLC-container-carto"> 
            <div class="row">
                <!--  Colone de gauche : Zone avec les actions -->        
                <div class="col-md-4 align-self-start TLC-col-choix-filtre">
                    <ul class="list-group">
                        <!-- Boutons pour les actions -->
                        <li class="list-group-item px-1 justify-content-center">
                            <!-- Bouton du Tri -->
                            <a class="btn btn-primary px-0 px-sm-2" sc-nom="zone-action-trier" sc-priorite="2" data-toggle="collapse" href="#collapseTri" role="button" aria-expanded="false" aria-controls="collapseTri">
                                <h2 class="TLC-actions">
                                    Trier
                                </h2> 
                            </a> <!-- Bouton du Tri -->   
                            <!-- Bouton de recherche -->
                            <a class="btn btn-primary px-0 px-sm-2" sc-nom="zone-action-rechercher" sc-priorite="2" data-toggle="collapse" href="#collapseRechercher" role="button" aria-expanded="false" aria-controls="collapseRechercher">
                                <h2 class="TLC-actions">
                                    Rechercher
                                </h2> 
                            </a> <!-- Bouton de recherche -->   
                            <!-- Bouton de gestion de la carte -->
                            <a class="btn btn-primary px-0 px-sm-2" sc-nom="zone-action-carte" sc-priorite="2" data-toggle="collapse" href="#collapseCarte" role="button" aria-expanded="false" aria-controls="collapseCarte">
                                <h2 class="TLC-actions">
                                    Carte
                                </h2> 
                            </a> <!-- Bouton de gestion de la carte -->   
                            <!-- Bouton du filtrage -->
                            <a class="btn btn-primary px-0 px-sm-2" sc-nom="zone-action-filtrer" sc-priorite="2" data-toggle="collapse" href="#collapseFiltrage" role="button" aria-expanded="false" aria-controls="collapseFiltrage">
                                <h2 class="TLC-actions" >
                                    Filtrer
                                </h2> 
                            </a> <!-- Bouton de filtrage -->
                        </li> <!-- Boutons pour les actions -->
            
                        <!-- Zone pour le tri -->
                        <div class="collapse hide" id="collapseTri">
                            <!-- pour la ligne de séparation -->
                            <li class="list-group-item accordion" id="TLC-tri-items">     
                                <div class="row">
                                    <div class="col-3">
                                        <p>Trier</p>
                                    </div> <!-- col -->
                                    <!--  col, Boutons de tri -->
                                    <div class="col">
                                        <button class="btn btn-primary statClic" sc-nom="tri" sc-priorite="2" sc-typeParam1="text" type="button" id="TLC-trie-dist">Distance</button>
                                        <button class="btn btn-primary statClic" sc-nom="tri" sc-priorite="2" sc-typeParam1="text" type="button" id="TLC-trie-prix">Prix</button>
                                        <button class="btn btn-primary statClic" sc-nom="tri" sc-priorite="2" sc-typeParam1="text" type="button" id="TLC-trie-note">Note</button>
                                        <!-- <button class="btn  btn-primary statClic" sc-nom="tri" sc-priorite="2" sc-typeParam1="text" type="button" id="TLC-trie-dispo">Disponibilité</button>-->
                                        <!-- spinner -->
                                        <div class="spinner-border text-primary" role="status" id="TLC-navbartri-spinner">
                                            <span class="sr-only">Loading...</span>
                                        </div> <!-- spinner -->
                                    </div> <!-- col, Boutons de tri -->
                                </div> <!-- row -->
                            </li> <!-- pour la ligne de séparation -->
                        </div> <!-- Zone pour le tri -->

                        <!-- Zone pour la recherche de coachs -->
                        <div class="collapse hide" id="collapseRechercher">
                            <!-- pour la ligne de séparation -->
                            <li class="list-group-item accordion" id="TLC-Rechercher-items">      
                                <div class="row">
                                    <div class="col-3">
                                        <p class="text-center">Rechercher</p>
                                    </div> <!-- col -->
                                    <div class="col-8">
                                        <!-- NB: plusieurs forms différents, pour avoir des boutons de soumission différents et cachés -->    
                                        <!-- form pour le changment d'adresse -->
                                        <form class="nav-item" id="TLC-form-map-adresse" action="#" method="post" name="TLC-form-map-adresse">
                                            <!-- champ pour changer d'adresse -->
                                            <input type="text" class="form-control mr-3 w-auto statClic" sc-nom="champ-changement-lieu" sc-priorite="2"  placeholder="Autre lieu" aria-label="Autre lieu" id="TLC-map-adresse">
                                            <!-- Bouton de submit NB: il est caché-->
                                            <input type="submit" hidden value="changement d'adresse" class="btn btn-outline-primary statClic" sc-nom="bouton-submit-changement-lieu" sc-lienSortie="" sc-priorite="2" id="TLC-submit-map-adresse"/>
                                        </form> <!-- form pour le changment d'adresse -->
                                        
                                        <!-- form pour la recherche de coachs -->
                                        <form class="nav-item" id="TLC-form-recherche-coach" action="#" method="post" name="TLC-form-recherche-coach" >
                                            <!-- champ et bouton pour rechercher un coach -->
                                            <input type="text" class="form-control mr-3 w-auto statClic" sc-nom="champ-recherche-coach" sc-priorite="2"  placeholder="Trouver un coach" aria-label="trouver un coach" id="TLC-champ-recherche-coach">
                                            <!-- Bouton de submit NB: il est caché-->
                                            <button type="submit"  hidden class="btn btn-outline-primary statClic" sc-nom="bouton-submit-recherche-coach" sc-lienSortie="" sc-priorite="2" id="TLC-submit-champ-recherche-coach">rechercher</button>
                                        </form> <!-- form pour la recherche de coachs -->
                                    </div> <!-- col -->
                                    <div class="col-1 ml-0 pl-0">
                                        <!-- spinner -->
                                        <div class="spinner-border spinner-border-md text-primary" role="status" id="TLC-spinner-champs-recherche">
                                        </div>
                                    </div> <!-- col -->
                                </div> <!-- row -->
                            </li> <!-- pour la ligne de séparation -->
                        </div> <!-- Zone pour la recherche de coachs -->

                        <!-- Zone pour la gestion de la carte -->
                        <div class="collapse hide" id="collapseCarte">
                            <li class="list-group-item accordion" id="TLC-carte-items">
                                <div class="row">
                                    <div class="col-3">
                                        <p>Carte </p>
                                    </div> <!-- col -->
                                    <!--  col, Boutons de gestion de la carte -->
                                    <div class="col">
                                        <!-- bouton pour recentrer la carte sur l'utilisateur-->
                                        <img src="public/icons/Marker-bleu-avec-cadre.svg"    class="nav-item mr-3 statClic" sc-nom="bouton-centrage-carte" alt="carte centrée sur l'utilisateur" id="TLC-button-carte-center-user" style="width:40px;height:40px;" data-toggle="tooltip"  title="Centrer la carte sur l'utilisateur" data-trigger="hover" >
                                        
                                        <!--  carte On -->
                                        <img src="public/icons/carte-on-type-gmaps3.svg"  class="nav-item mr-3 statClic"  sc-nom="bouton-carte-on" alt="carte On"                        id="TLC-button-carte-on" style="width:55px;height:42px;" data-toggle="tooltip" title="Afficher la carte" data-trigger="hover" >
                                        <!--  carte Off -->
                                        <img src="public/icons/carte-off-type-gmaps3.svg" class="nav-item mr-3 statClic" sc-nom="bouton-carte-off" alt="carte Off"                       id="TLC-button-carte-off" style="width:55px;height:42px;" data-toggle="tooltip"  title="Masquer la carte" data-trigger="hover" > 
                                    </div> <!-- col, Boutons de tri -->
                                </div> <!-- row -->
                            </li>
                        </div> <!-- Zone pour la gestion de la carte -->
            
                        <!-- Zone pour le filtrage -->
                        <div class="collapse hide" id="collapseFiltrage">
                            <li class="list-group-item accordion" id="TLC-filtre-items">
                                <p class="text-center">Filtrer </p>
                                <!--  C'est ici que sont ajoutés les filtres-->
                            </li>
                        </div> <!-- Zone pour le filtrage -->
                    </ul> 
                </div> <!-- Colonne de Gauche : Zone avec les actions --> 
                
                <!-- Colonne de droite : carte, ( ancienne zone de tri), encarts coachs -->
                <div class="col-md-8  align-self-start TLC-col-zone-carto">  
        
                    <!-- carte -->
                    <div id="TLC-zone-carto">
                        <div id="TLC-map">
                            <!-- C'est ici que la carte sera affichée -->
                        </div>   
                    </div> <!-- carte -->
                            
                    <!-- Zone avec les encarts pour les coachs -->
                    <div id="TLC-encarts-coachs"> 
                    </div>  <!-- Zone avec les encarts pour les coachs -->
                
                <!-- Zone avec la gestion de la pagination -->
                <div>
                    <!--
                    <nav aria-label="navigation avec les résultats">
                        <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        </ul>
                    </nav>  
                    -->
                </div> <!-- Zone avec la gestion de la pagination -->
                </div> <!-- Colonne de droite : carte, tri, encarts coachs -->
                    
            </div> <!-- row -->
        </div> <!-- choix, filtre, carte, tri, encart coachs-->

        <!-- Modal pour indiquer que la réception des données est en cours -->
        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-reception-donnees">
            <div class="modal-dialog modal-sm modal-dialog-centered" style="max-width: 200px;">
                <div class="modal-content">
                    <!-- Header -->
                    <div class="modal-header">
                    <p class="modal-title" id="modal-reception-donnees-label">Loading </p>
                    <!-- spinner -->
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div> <!-- spinner -->
                    <!-- bouton pour fermer la modal
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    -->
                    </div> <!-- Header -->   
                    <!-- body --> 
                    <div class="modal-body">
                    </div> <!-- body --> 
                </div>
            </div>
        </div> <!-- Modal pour indiquer que la réception des données est en cours -->

        <!-- Footer -->
            <div class="container-fluid footer"> 
            <nav class="row">
                <!--  col -->        
                <div class="col-sm-3 align-item-center">
                    <h3 class="text-center TLC-footer-title"> Trouver </h3>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Coachs </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Livres de coaching </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Formations de coaching </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Evènements </a></li>
                    </ul>
                </div><!-- col -->
                <!--  col -->        
                <div class="col-sm-3 align-item-center">
                    <h3 class="text-center TLC-footer-title"> A propos </h3>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Présentation </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >FAQ </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Contact </a></li>
                    </ul>
                </div><!-- col -->
                <!--  col -->  
                <div class="col-sm-3 align-item-center">
                    <h3 class="text-center TLC-footer-title"> Inscription</h3>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Internaute </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Coachs </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Formations de coaching </a></li>
                    </ul>
                    
                </div><!-- col -->
                <!--  col -->  
                <div class="col-sm-3 align-item-center">
                    <h3 class="text-center TLC-footer-title"> Utilisation </h3>
                    <ul class="list-unstyled">
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Conditions </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Confidentialité </a></li>
                        <li> <a href="#" class="TLC-footer-link statClic" sc-nom="footer" sc-typeParam1="text" sc-priorite="2" sc-lienSortie="" >Cookies </a></li>
                    </ul>
                    
                </div><!-- col -->
            </nav><!-- row -->
        </div><!-- Footer -->
        

        <!-- ---------------------------------------------------------------------------------------------- -->

        <!-- Fichiers JavaScript -->
        <div>
            <?= $scriptJsConsoleOnMobiles // js console on mobile phones ?>

            <!-- Pour Bottstrap : jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                    crossorigin="anonymous">
            </script>  <!-- jQuery3.4.1 fonctions ajax et effects inclues -->


            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                    crossorigin="anonymous">
            </script>

            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                    crossorigin="anonymous">
            </script>


            <!-- fichiers communs à plusieurs pages -->
            <script src="public/js/parametres.js"></script>  <!-- paramètres pour mes scripts js -->
            <script src="public/js/commun.js"></script>    <!-- variables communes à mes différents scripts js -->
            <!-- fichiers pour cette page -->
            <script src="public/js/pages/coachs.js"></script>    <!-- scripts de cette page -->
            <?= $scriptGoogleMaps; // API google maps ?>

            
        </div> <!-- Fichiers JavaScript -->

            <div id="TLC-test"> 
            </div>
            