<?php

//===================================================================================
// paramètres de l'application
// %%% à moitié obsolète, il faudra modifier ça
//===================================================================================

// %%%
//partie à remanier :
// paramètres de l'application
require APPPATH."config/appParam.php";


//===================================================================================
// content management
//===================================================================================
	
	//-------------------------------------------------------------------------------
	// $scriptJsConsoleOnMobiles  => disp js console on mobile phones (DEV only)
	//-------------------------------------------------------------------------------
		if( (ENVIRONMENT ==  'development') && ($param->devTools->script['jsConsoleOnMobiles']) ){
			$scriptJsConsoleOnMobiles = '<!-- console js sur tel portable -->';
			$scriptJsConsoleOnMobiles .= '<script src="public/js/mobileConsole.js"></script>';
		} else {
			$scriptJsConsoleOnMobiles = "";
		}

	//-------------------------------------------------------------------------------
	// $scriptGoogleMaps   => google Maps script
	//-------------------------------------------------------------------------------
		if( (ENVIRONMENT ==  'development') && ($param->devTools->useProxy['gMaps'] == false) ){ 
			// without proxy (only in DEV environment)
			$scriptGoogleMaps = '<script src="https://maps.googleapis.com/maps/api/js?key='
								.$param->API->key['gMaps']
								.'&callback=initMap" async defer></script>';
		} else { // with proxy  (default)
			if(false){
				// version passant par le routeur.
				// Ca fonctionne mais ça génère un warning de la console js en précisant que le type
				// n'est pas approprié. C'est pourquoi on préfère la version qui ajoute directement
				// le code du script dans la page.
				$scriptGoogleMaps = '<script src="index.php?proxy=gMapsAPIProxy" async defer></script>';
			} else {
				// version récupérant directement le code pour le mettre dans la vue.
				// adresse du script google maps à récupérer.
				// NB: l'adresse intègre la clef API
				$src="https://maps.googleapis.com/maps/api/js?key="
					.$param->API->key["gMaps"]
					."&callback=initMap";

			    // NB: file_get_content($path) va lire le fichier dont l'adresse est contenue dans path, 
			    // pour le mettre dans une chaîne de caractères.
			    // et echo renvoie la chaîne de caractères avec le script qu'on a récupéré chez google maps
			    $scriptGoogleMaps = '<script async defer>'.file_get_contents($src).'</script>';
			}
		} 


//-----------------------------------------------------------------------------------
// template call
//-----------------------------------------------------------------------------------
	require APPPATH.'Views/templates/coachsTemplate.php';
