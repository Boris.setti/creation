<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
================================================== -->
    <title>TOUSLESCOACHS.COM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
================================================== -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main-color.css" id="colors">

</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
================================================== -->
        <header id="header-container" class="fixed fullwidth dashboard">

            <!-- Header -->
            <div id="header" class="not-sticky">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <a href="/"><img src="/images/logo.png" alt=""></a>
                            <a href="/" class="dashboard-logo"><img src="/images/logo2.png" alt=""></a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        <nav id="navigation" class="style-1">
                            <ul id="responsive">


                                <li><a href="/">Accueil</a>
                                </li>

                                <li><a href="/coach">Coachs</a>
                                </li>

                                <li><a href="/livre">Livres</a>
                                </li>

                                <li><a href="/evenement">Evènements</a>
                                </li>

                                <li><a href="/formation">Formations de coaching</a>
                                </li>

                            </ul>
                        </nav>
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->

                    </div>
                    <!-- Left Side Content / End -->

                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">

                            <?php if (isset($_SESSION['first_name'])) { ?>
                                <div class="user-menu">
                                    <div class="user-name"><span><img src="<?= $_SESSION['picture'] ?>" alt=""></span><?= "$_SESSION[first_name] $_SESSION[last_name]" ?></div>
                                    <ul>
                                        <li><a href="/profil"><i class="sl sl-icon-settings"></i>Mon compte</a></li>
                                        <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                                    </ul>
                                </div>
                            <?php
                            } else {
                                ?>
                                <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Se connecter</a>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>
        <div class="clearfix"></div>
        <!-- Header Container / End -->

        <!-- Dashboard -->
        <div id="dashboard">

            <!-- Navigation
	================================================== -->

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Account">
                        <li><a href="/profil"><i class="sl sl-icon-user"></i> Mon profile</a></li>
                        <li><a href="/profil/vitrine"><i class="sl sl-icon-people "></i> Vitrine</a></li>
                        <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                    </ul>

                </div>
            </div>
            <!-- Navigation / End -->


            <!-- Content
	================================================== -->
            <div class="dashboard-content">

                <!-- Titlebar
================================================== -->
                <div id="titlebar" class="gradient">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="user-profile-titlebar">
                                    <div class="user-profile-avatar"><img src='<?= $coach['picture'] ?>' alt=""></div>
                                    <div class="user-profile-name">
                                        <h2><?= $coach['first_name'] ?> <?= $coach['last_name'] ?></h2>
                                        <p><strong>
                                                <?php foreach ($coach['categories'] as $key => $category) {
                                                    if ($key == 0) {
                                                        echo $category;
                                                    } else {
                                                        echo ' | ' . $category;
                                                    }
                                                } ?>
                                            </strong></p>
                                        <div class="star-rating" data-rating="1">
                                            <div class="rating-counter"><a href="#listing-reviews">(12 reviews)</a></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



                <!-- Content
================================================== -->
                <div class="container">
                    <div class="row sticky-wrapper">


                        <!-- Sidebar
		================================================== -->
                        <div class="col-lg-4 col-md-4 margin-top-0">

                            <!-- Verified Badge -->
                            <div class="verified-badge with-tip" data-tip-content="Account has been verified and belongs to the person or organization represented.">
                                <i class="sl sl-icon-user-following"></i> Verified Account
                            </div>

                            <!-- Contact -->
                            <div class="boxed-widget margin-top-30 margin-bottom-50">
                                <h3>Infos</h3>
                                <ul class="listing-details-sidebar">
                                    <li><i class="sl sl-icon-phone"></i><?= $coach['phone'] ?></li>
                                    <li><i class="fa fa-envelope-o"></i> <a href="#"><?= $coach['email'] ?></a></li>
                                    <li><i class="sl sl-icon-home "></i><?= $coach['address'] ?>, <?= $coach['country'] ?></li>
                                </ul>

                                <ul class="listing-details-sidebar social-profiles">
                                    <li><a href="<?= $coach['facebook'] ?>" class="facebook-profile"><i class="fa fa-facebook-square"></i> Facebook</a></li>
                                    <li><a href="<?= $coach['twitter'] ?>" class="twitter-profile"><i class="fa fa-twitter"></i> Twitter</a></li>
                                    <li><a href="<?= $coach['linkedin'] ?>" style="color: #0077b5;"><i class="fa fa-linkedin" style="color: #0077b5;"></i> Linkedin</a></li>
                                    <li><a href="<?= $coach['website'] ?>"><i class="fa fa-at"></i> Site Web</a></li>
                                </ul>

                                <!-- Reply to review popup -->
                                <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                                    <div class="small-dialog-header">
                                        <h3>Contacter</h3>
                                    </div>
                                    <div class="message-reply margin-top-0">
                                        <textarea cols="40" rows="3" placeholder="Votre message pour <?= $coach['first_name'] ?>"></textarea>
                                        <button class="button">Envoyer</button>
                                    </div>
                                </div>

                                <a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Contacter</a>
                            </div>
                            <!-- Contact / End-->

                        </div>
                        <!-- Sidebar / End -->


                        <!-- Content
		================================================== -->
                        <div class="col-lg-8 col-md-8 padding-left-30">

                            <h3 class="margin-top-0 margin-bottom-40">Offres :</h3>

                            <!-- Listings Container -->
                            <div class="row">

                                <!-- Listing Item -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="listing-item-container list-layout">
                                        <a href="listings-single-page.html" class="listing-item">

                                            <!-- Image -->
                                            <div class="listing-item-image">
                                                <img src="images/listing-item-01.jpg" alt="">
                                                <span class="tag">Eat & Drink</span>
                                            </div>

                                            <!-- Content -->
                                            <div class="listing-item-content">


                                                <div class="listing-item-inner">
                                                    <h3>Tom's Restaurant</h3>
                                                    <span>964 School Street, New York</span>
                                                    <div class="star-rating" data-rating="3.5">
                                                        <div class="rating-counter">(12 reviews)</div>
                                                    </div>
                                                </div>

                                                <span class="like-icon"></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- Listing Item / End -->

                                <!-- Listing Item -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="listing-item-container list-layout">
                                        <a href="listings-single-page.html" class="listing-item">

                                            <!-- Image -->
                                            <div class="listing-item-image">
                                                <img src="images/listing-item-03.jpg" alt="">
                                                <span class="tag">Hotels</span>
                                            </div>

                                            <!-- Content -->
                                            <div class="listing-item-content">

                                                <div class="listing-item-inner">
                                                    <h3>Hotel Govendor</h3>
                                                    <span>778 Country Street, New York</span>
                                                    <div class="star-rating" data-rating="2.0">
                                                        <div class="rating-counter">(17 reviews)</div>
                                                    </div>
                                                </div>

                                                <span class="like-icon"></span>

                                                <div class="listing-item-details">Starting from $59 per night</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- Listing Item / End -->

                                <!-- Listing Item -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="listing-item-container list-layout">
                                        <a href="listings-single-page.html" class="listing-item">

                                            <!-- Image -->
                                            <div class="listing-item-image">
                                                <img src="images/listing-item-04.jpg" alt="">
                                                <span class="tag">Eat & Drink</span>
                                            </div>

                                            <!-- Content -->
                                            <div class="listing-item-content">
                                                <div class="listing-badge now-open">Now Open</div>

                                                <div class="listing-item-inner">
                                                    <h3>Burger House</h3>
                                                    <span>2726 Shinn Street, New York</span>
                                                    <div class="star-rating" data-rating="5.0">
                                                        <div class="rating-counter">(31 reviews)</div>
                                                    </div>
                                                </div>

                                                <span class="like-icon"></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- Listing Item / End -->

                                <div class="col-md-12 browse-all-user-listings">
                                    <a href="#">Browse All Listings <i class="fa fa-angle-right"></i> </a>
                                </div>

                            </div>
                            <!-- Listings Container / End -->


                            <!-- Reviews -->
                            <div id="listing-reviews" class="listing-section">
                                <h3 class="margin-top-60 margin-bottom-20">Reviews</h3>

                                <div class="clearfix"></div>

                                <!-- Reviews -->
                                <section class="comments listing-reviews">

                                    <ul>
                                        <li>
                                            <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /></div>
                                            <div class="comment-content">
                                                <div class="arrow-comment"></div>
                                                <div class="comment-by">Kathy Brown <div class="comment-by-listing">on <a href="#">Burger House</a></div> <span class="date">June 2019</span>
                                                    <div class="star-rating" data-rating="5"></div>
                                                </div>
                                                <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>

                                                <div class="review-images mfp-gallery-container">
                                                    <a href="images/review-image-01.jpg" class="mfp-gallery"><img src="images/review-image-01.jpg" alt=""></a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /> </div>
                                            <div class="comment-content">
                                                <div class="arrow-comment"></div>
                                                <div class="comment-by">John Doe <div class="comment-by-listing">on <a href="#">Tom's Restaurant</a></div> <span class="date">May 2019</span>
                                                    <div class="star-rating" data-rating="4"></div>
                                                </div>
                                                <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /></div>
                                            <div class="comment-content">
                                                <div class="arrow-comment"></div>
                                                <div class="comment-by">Kathy Brown <div class="comment-by-listing">on <a href="#">Burger House</a></div> <span class="date">June 2019</span>
                                                    <div class="star-rating" data-rating="5"></div>
                                                </div>
                                                <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>

                                                <div class="review-images mfp-gallery-container">
                                                    <a href="images/review-image-02.jpg" class="mfp-gallery"><img src="images/review-image-02.jpg" alt=""></a>
                                                    <a href="images/review-image-03.jpg" class="mfp-gallery"><img src="images/review-image-03.jpg" alt=""></a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /> </div>
                                            <div class="comment-content">
                                                <div class="arrow-comment"></div>
                                                <div class="comment-by">John Doe <div class="comment-by-listing">on <a href="#">Hotel Govendor</a></div> <span class="date">May 2019</span>
                                                    <div class="star-rating" data-rating="5"></div>
                                                </div>
                                                <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                                            </div>

                                        </li>
                                    </ul>
                                </section>

                            </div>


                        </div>

                    </div>


                    <!-- Copyrights -->
                    <div class="col-md-12">
                        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                    </div>

                </div>
                <!-- Content / End -->


            </div>
            <!-- Dashboard / End -->


        </div>
        <!-- Wrapper / End -->

        <!-- Scripts
================================================== -->
        <script type="text/javascript" src="/scripts/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="/scripts/jquery-migrate-3.1.0.min.js"></script>
        <script type="text/javascript" src="/scripts/mmenu.min.js"></script>
        <script type="text/javascript" src="/scripts/chosen.min.js"></script>
        <script type="text/javascript" src="/scripts/slick.min.js"></script>
        <script type="text/javascript" src="/scripts/rangeslider.min.js"></script>
        <script type="text/javascript" src="/scripts/magnific-popup.min.js"></script>
        <script type="text/javascript" src="/scripts/waypoints.min.js"></script>
        <script type="text/javascript" src="/scripts/counterup.min.js"></script>
        <script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/scripts/tooltips.min.js"></script>
        <script type="text/javascript" src="/scripts/custom.js"></script>
        <script type="text/javascript" src="/scripts/popup-redirect.js"></script>


        <!-- Google Autocomplete -->
        <script>
            function initAutocomplete() {
                var input = document.getElementById('autocomplete-input');
                var autocomplete = new google.maps.places.Autocomplete(input);

                autocomplete.addListener('place_changed', function() {
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        return;
                    }
                });

                if ($('.main-search-input-item')[0]) {
                    setTimeout(function() {
                        $(".pac-container").prependTo("#autocomplete-container");
                    }, 300);
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>


        <!-- Typed Script -->
        <script type="text/javascript" src="/scripts/typed.js"></script>
        <script>
            var typed = new Typed('.typed-words', {
                strings: ["Attractions", " Restaurants", " Hotels"],
                typeSpeed: 80,
                backSpeed: 80,
                backDelay: 4000,
                startDelay: 1000,
                loop: true,
                showCursor: true
            });
        </script>


        <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
        <script src="/scripts/moment.min.js"></script>
        <script src="/scripts/daterangepicker.js"></script>

        <script>
            $(function() {

                var start = moment().subtract(0, 'days');
                var end = moment().add(2, 'days');

                function cb(start, end) {
                    $('#booking-date-search').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                cb(start, end);
                $('#booking-date-search').daterangepicker({
                    "opens": "right",
                    "autoUpdateInput": true,
                    "alwaysShowCalendars": true,
                    startDate: start,
                    endDate: end
                }, cb);

                cb(start, end);

            });

            // Calendar animation and visual settings
            $('#booking-date-search').on('show.daterangepicker', function(ev, picker) {
                $('.daterangepicker').addClass('calendar-visible calendar-animated bordered-style');
                $('.daterangepicker').removeClass('calendar-hidden');
            });
            $('#booking-date-search').on('hide.daterangepicker', function(ev, picker) {
                $('.daterangepicker').removeClass('calendar-visible');
                $('.daterangepicker').addClass('calendar-hidden');
            });

            $(window).on('load', function() {
                $('#booking-date-search').val('');
            });
        </script>



</body>

</html>