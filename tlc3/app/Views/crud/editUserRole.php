<h1>Les informations de l'utilisateur sélectionné:</h1>

<textarea name="" id="" cols="30" rows="10"><?php print_r($user) ?></textarea>

<h3>Son role actuel est:</h3>

<?php
switch ($_POST['role_id']) {
  case '1':
  echo '<p>Admin</p>';
    break;
  case '2':
echo '<p>Coach</p>';
  break;
  case '3':
  echo '<p>Utilisateur</p>';
    break;
  default:
 
}
?>

<h3>Son futur role sera:</h3>
<form method="post" action="" >

    <?= csrf_field() ?>

    <p class="form-row form-row-wide">
        <label for="roles">Choisir un rôle:
            <i class="im im-icon-Male"></i>
            <select name="roles" id="roles" required>
                <option></option>
                <option value="1">Admin</option>
                <option value="3">Utilisateur</option>
                <option value="2">Coach</option>
            </select>
        </label>
    </p>
    <input type="hidden" name="old_roles_id" value="<?= $_POST['role_id'] ?>">
    <input type="hidden" name="user_id" value="<?= $user_id ?>">
    <input type="hidden" name="action" value="post">

    <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

</form>