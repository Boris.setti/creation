<form method="post" action="" oninput='password1.setCustomValidity(password2.value != password1.value ? "Les mots de passe ne correspondent pas." : "")'>

    <?= csrf_field() ?>

    <p class="form-row form-row-wide">
        <label for="last_name">Nom:
            <input type="text" class="input-text" name="last_name" id="last_name" value="<?=$last_name ?>" required />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="first_name">Prénom:

            <input type="text" class="input-text" name="first_name" id="first_name" value="<?=$first_name ?>" required />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="email2">Adresse mail:

            <input type="email" class="input-text" name="email" id="email2" value="<?=$email ?>" required />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="picture">Emplacement Image:

            <input type="text" class="input-text" name="picture" id="picture" value="<?=$picture ?>" required />
        </label>
    </p>
  

    <p class="form-row form-row-wide">
        <label for="password1">Mot de passe:

            <input class="input-text" type="password" name="password1" id="password1" required />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="password2">Confirmation du mot de passe:

            <input class="input-text" type="password" name="password2" id="password2" />
        </label>
    </p>
    <input type="hidden" name="user_id" value="<?= $user_id ?>">
    <input type="hidden" name="action" value="post">

    <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

</form>