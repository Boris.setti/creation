<?php namespace App\Models;

use CodeIgniter\Model;

class CoachModel extends Model
{
        protected $table      = 'coachs';
        protected $primaryKey = 'coach_id';

        protected $returnType = 'array';
        protected $useSoftDeletes = false;

        protected $allowedFields = [
            'user_id',
            'address',
            'city',
            'postcode',
            'country',
            'geocod_lat',
            'geocod_lng',
            'phone',
            'facebook',
            'twitter',
            'linkedin',
            'website',
            'logo',
            'title',
            'description'
        ];

        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        protected $validationRules    = [];
        protected $validationMessages = [];
        protected $skipValidation     = false;

    
}