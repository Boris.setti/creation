<?php namespace App\Models;

use CodeIgniter\Model;

class CoachCategoryModel extends Model
{
        protected $table      = 'coach_category';
        protected $primaryKey = 'coach_category_id';

        protected $returnType = 'array';
        protected $useSoftDeletes = false;

        protected $allowedFields = ['coach_id', 'category_id'];

        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        protected $validationRules    = [];
        protected $validationMessages = [];
        protected $skipValidation     = false;
}