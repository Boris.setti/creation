<?php
/*
	Ce fichier est OBSOLETE : Il sert encore pour la page 'coachs', mais il s'agit d'un reliquat
	de la configuration avant d'être sous code-igniter .
	
	Paramètres de l'App
	NB: (2019 11 13) Pour l'instant, un certain nombre de paramètres
	sont obsolètes (ils date de la version avant CodeIgniter) et devront être supprimés.
*/   

// initialisations : Nothing to change
//===================================
	// objects init
	// initialisation des objets qui vont contenir  les paramètres
	// NB : il faut les déclarer comme des objets standards avant de leur attribuer des propriétés
	//      sinon PHP va envoyer une erreur, en plus des données qu'on veut envoyer.
	$param 					= new \stdClass();
	$param->devTools 		= new \stdClass();
	$param->API 			= new \stdClass();
	$param->rechercheCoach 	= new \stdClass();

	// définition de constantes
	define("ON" , true );
	define("OFF", false);



// DEV tools
//==========
	// Tools that are only allowed in 'DEV' environment

	// use proxy or not
	// WARNING !!! without proxy, the API key is visible in the html code !!!
	$param->devTools->useProxy = [
		"gMaps" 	=> ON,		// NB: quand c'est très long (>4 secondes !), redémarrer ordi (pb de serveur)
		"gGeoLoc" 	=> OFF
	];

	// js console on mobile phone
	$param->devTools->script = [
		"jsConsoleOnMobiles" => OFF 	
	];


// API
//=====
	// API keys
	$param->API->key = [
		"gMaps"		=> "AIzaSyBLgnhsPO_d5FffAv0feEb0jPX4Z0i6cEo",
		"gGeoLoc" 	=> "AIzaSyCN9u2Lq6gVEBzeyQ-L6yNmFtCD4A4FKLY"
	];

// scripts
//========


// paramètres généraux
//====================
	// nombre maxi de réponses lors de la recherche d'un coach. 
	// NB: ne s'applique pas à la recherche des coachs à proximité
	$param->rechercheCoach->nbreMaxReponses = 15; 

    