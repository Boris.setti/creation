<?php

namespace Config;

/**
 * --------------------------------------------------------------------
 * URI Routing
 * --------------------------------------------------------------------
 * This file lets you re-map URI requests to specific controller functions.
 *
 * Typically there is a one-to-one relationship between a URL string
 * and its corresponding controller class/method. The segments in a
 * URL normally follow this pattern:
 *
 *    example.com/class/method/id
 *
 * In some instances, however, you may want to remap this relationship
 * so that a different class/function is called than the one
 * corresponding to the URL.
 */

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 * The RouteCollection object allows you to modify the way that the
 * Router works, by acting as a holder for it's configuration settings.
 * The following methods can be called on the object to modify
 * the default operations.
 *
 *    $routes->defaultNamespace()
 *
 * Modifies the namespace that is added to a controller if it doesn't
 * already have one. By default this is the global namespace (\).
 *
 *    $routes->defaultController()
 *
 * Changes the name of the class used as a controller when the route
 * points to a folder instead of a class.
 *
 *    $routes->defaultMethod()
 *
 * Assigns the method inside the controller that is ran when the
 * Router is unable to determine the appropriate method to run.
 *
 *    $routes->setAutoRoute()
 *
 * Determines whether the Router will attempt to match URIs to
 * Controllers when no specific route has been defined. If false,
 * only routes that have been defined here will be available.
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('PublicArea');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
//$routes->get('/', 'Home::index');  // si URL sans paramètres => appele la méthode index du Home controller
//$routes->get('/', 'pages::showme/Home');
//$routes->get('/public/(:any)', 'PublicResources::getResource/$1');
//$routes->match(['get', 'post'], '/ajax/(:segment)/(:segment)', 'Ajax::$1/$2'); // sinon, si ajax
//$routes->match(['get', 'post'], 'news/create', 'News::create');
//$routes->get('news/(:segment)', 'news::view/$1');
//$routes->get('(:any)', 'Pages::showme/$1'); // sinon, s'il y a un nom qui correspond à une vue, alors appel au controller des pages avec en paramètre le nom de la vue



//alternative routes

$routes->get('/', 'Home::index');
$routes->get('/livre', 'Book::index');
$routes->get('/evenement', 'Event::index');
$routes->get('/formation', 'Formation::index');

$routes->get('/profil', 'Profil::index');
$routes->get('/profil/vitrine', 'Profil::vitrine', ['filter' => 'coach']);
$routes->get('/profil/update', 'Profil::update');
$routes->post('profil/upload', 'Profil::upload');
$routes->post('/profil/save', 'Profil::save');
$routes->post('/profil/savepwd', 'Profil::savepwd');

$routes->get('/coach', 'Coach::index');

$routes->get('/coach/(:segment)/(:segment)/(:segment)', 'Coach::show/$1/$2/$3');








//////////////////////////////TEST BORIS/////////////////////////////////
// $routes->get('/test', 'Test', ['filter' => 'admin']); //exemple de route filtré
$routes->get('/test', 'Test'); ///////////////////////////////////////////
$routes->post('/post/test', 'Test::testPost'); ////////////////////////////

////////////////////////////LOGIN//////////////////////////////////////////
$routes->post('login', 'Login'); //------------------------------check login 
$routes->post('register', 'Login::register'); //----------------created user
$routes->get('logout', 'Login::logout'); //----------------------logout user

/////////////////////////////CRUD//////////////////////////////////////////
$routes->get('/crud', 'Crud', ['filter' => 'admin']); //-------------Page CRUD
$routes->post('add/user', 'Crud::addUser', ['filter' => 'admin']);
$routes->post('edit/user', 'Crud::EditUser', ['filter' => 'admin']);
$routes->post('del/user', 'Crud::delUser', ['filter' => 'admin']);
$routes->post('edit/userRole', 'Crud::EditUserRole', ['filter' => 'admin']);
$routes->post('edit/coach', 'Crud::EditCoach', ['filter' => 'admin']);
$routes->post('add/categorie','Crud::addCategorie',['filter' => 'admin']);
$routes->post('del/catergorie','Crud::delCategorie',['filter' => 'admin']);
$routes->post('edit/catergorie', 'Crud::EditCategorie', ['filter' => 'admin']);

$routes->get('(:any)','Home::page404');


/*$routes->get('/', 'Pages::showme/home');
$routes->get('/public/(:any)', 'PublicResources::getResource/$1');
$routes->match(['get', 'post'], '/ajax/(:segment)/(:segment)', 'Ajax::$1/$2'); // sinon, si ajax
$routes->match(['get', 'post'], 'news/create', 'News::create');
$routes->get('news/(:segment)', 'news::view/$1');
$routes->get('(:any)', 'Pages::showme/$1'); // sinon, s'il y a un nom qui correspond à une vue, alors appel au controller des pages avec en paramètre le nom de la vue
*/
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
