<?php

namespace App\Controllers;

use App\Models\CategoryModel;
use App\Models\CoachCategoryModel;
use App\Models\UserModel;
use App\Models\CoachModel;



$session = \Config\Services::session();
$session->start();

$validation =  \Config\Services::validation();


class Coach extends BaseController
{
    public function index()
    {
        $db = \Config\Database::connect();

        // Join Coachs and Users
        $query = $db->query('SELECT * FROM coachs LEFT JOIN users ON coachs.user_id = users.user_id');
        $array = $query->getResultArray();
        //On récupére les données des tables coachs et users dans un tableau

        // Unset password in array coaches
        $coaches = [];
        foreach ($array as $coach) {
            unset($coach['password']);
            $coaches[] = $coach;
        }
        //On enlève du tableau tous les mots de passe utilisateur (sécurité)

        // Categories
        $coachCategoryModel = new CoachCategoryModel;
        $CategoryModel = new CategoryModel;

        foreach ($coaches as $key => $coach) {

            $categories_id = $coachCategoryModel->where('coach_id', $coach['coach_id'])->findColumn('category_id');

            if (is_array($categories_id)) {
                $categories = [];
                foreach ($categories_id as $category_id) {
                    $category_id = $CategoryModel->where('category_id', $category_id)->findColumn('name');
                    $categories[] = $category_id[0];
                }
                $coach['categories'] = $categories;
                $coaches[$key]['categories'] = $coach['categories'];
            } else {
                $coach['categories'][] = "Non précisé";
                $coaches[$key]['categories'] = $coach['categories'];
            }
        }


        // Count results
        $count = count($coaches);


        // Prepared data
        $data = [
            'coaches'       => $coaches,
            'number_result' => $count
        ];

        // Echo views
        echo view('templates/themeHeader');
        echo view('coach/index', $data);
    }


    public function show($first_name, $last_name, $coach_id)
    {
        $coachModel = new CoachModel;
        $coach = $coachModel->find($coach_id);

        //Grace au coach_id récupéré, on demande le reste des informations utilisateurs liées à cet id
        $user_id = $coach['user_id'];
        // On récupére dans la table du coach séléctionné son user_id
        $userModel = new UserModel;
        $user = $userModel->where('user_id', $user_id)->first();
        //On fait appel à la table users et on récupére l'utilisateur dont l'id est égal à la valeur récupérée plus haut
        if ($user['first_name'] == $first_name && $user['last_name'] == $last_name) {
            
            foreach ($user as $key => $value) {
                $coach[$key] = $value;
            }
            
            // Unset password in array coaches
            unset($coach['password']);
            // Categories
            $coachCategoryModel = new CoachCategoryModel;
            $CategoryModel = new CategoryModel;

            $categories_id = $coachCategoryModel->where('coach_id', $coach['coach_id'])->findColumn('category_id');

            if (is_array($categories_id)) {
                $categories = [];
                foreach ($categories_id as $category_id) {
                    $category_id = $CategoryModel->where('category_id', $category_id)->findColumn('name');
                    $categories[] = $category_id[0];
                }
                $coach['categories'] = $categories;
            } else {
                $coach['categories'][] = "Non précisé";
            }

            $data = [
                'coach' => $coach,
            ];


            echo view('templates/themeHeader');
            echo view('coach/show', $data);
            echo view('templates/themeFooter');
        } else {
            return redirect()->to('/coach');
        }
    }
}
