<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\CoachModel;
use App\Models\CategoryModel;
use App\Models\UserRoleModel;
use App\Models\CoachCategoryModel;


$session = \Config\Services::session();
$session->start();


class Crud extends BaseController
{





	public function index()
	{
		//appel les table utiliser sur la page CRUD
		$UserModel = new UserModel;
		$user = $UserModel->findAll();

		$UserRoleModel = new UserRoleModel;
		$userRole = $UserRoleModel->findAll();

		$CoachModel = new CoachModel;
		$coach = $CoachModel->findAll();

		$CategroyModel = new CategoryModel;
		$categorie = $CategroyModel->findAll();

		$CoachCategoryModel = new CoachCategoryModel;
		$coachCategory = $CoachCategoryModel->findAll();

		$data =
			[
				'user' => $user,
				'userRoles' => $userRole,
				'coach' => $coach,
				'categorie' => $categorie,
				'coachCategory' => $coachCategory,
			];

		echo view('templates/themeHeader');
		echo view('crud/crud', $data);
		echo view('templates/themeFooter');
	}





	public function addUser()
	{


		switch ($_POST['action']) {
			case 'edit':

				echo view('templates/themeHeader.php');
				echo view('crud/addUser');
				echo view('templates/themeFooter');



				break;

			case 'post':
				// initialisation model et session
				$user = new UserModel;
				$roles = new UserRoleModel;
				$session = session();

				//récupération info table user via email
				$checkUser = $user->where('email', "$_POST[email]")->first();

				if ($checkUser == null) {
					//si null initie les données à envoyer à la BDD user et envoie
					$data = [
						'first_name'  => "$_POST[first_name]",
						'last_name'   => "$_POST[last_name]",
						'email'       => "$_POST[email]",
						'password'    =>  hash('sha384', "$_POST[password1]")
					];
					$user->insert($data);

					//récupération info table user via email
					$User = $user->where('email', "$_POST[email]")->first();

					//si null initie les données à envoyer à la BDD user_role et envoie
					$data = [
						'user_id'   => $User['user_id'],
						'role_id'   => $_POST['roles']
					];
					$roles->insert($data);

					//récupération info table user_role via user_id
					$checkRole = $roles->where('user_id', "$User[user_id]")->first();

					//création de la table coach correspondante
					if ($checkRole['role_id'] == 2) {
						$coachModel = new CoachModel;

						$data = [
							'user_id'   => $User['user_id']
						];

						$coachModel->insert($data);
					}

					return redirect('crud')->with('success', "l utilisateur $checkUser[first_name] $checkUser[last_name] id $User[user_id] a étais crée  ");
				}


				return redirect('crud')->with('error', 'profil existant');

				break;
			default:
				return redirect('crud')->with('error', 'une erreur a eu lieu');
		};
	}





	public function EditUser()
	{


		switch ($_POST['action']) {
			case 'edit':

				$data =
					[
						'user_id' =>  "$_POST[user_id]",
						'first_name' =>  "$_POST[first_name]",
						'last_name' =>  "$_POST[last_name]",
						'email' =>  "$_POST[email]",
						'picture' =>  "$_POST[picture]",
					];

				echo view('templates/themeHeader.php');
				echo view('crud/editUser', $data);
				echo view('templates/themeFooter');

				break;

			case 'post':

				$data = [
					'first_name' => "$_POST[first_name]",
					'last_name' => "$_POST[last_name]",
					'email' => "$_POST[email]",
					'picture' =>  "$_POST[picture]",
					'password' => hash('sha384', "$_POST[password1]")
				];
				$user = new UserModel;
				$user->update($_POST['user_id'], $data);

				return redirect('crud')->with('success', " L'utilisateur $_POST[first_name] $_POST[last_name] a été modifié");

				break;
			default:
				return redirect('crud')->with('error', 'une erreur a eu lieu');
		}
	}





	public function delUser()
	{
		$CoachCategoryModel = new CoachCategoryModel;
		$roles = new UserRoleModel;
		$CoachModel = new CoachModel;
		$user = new UserModel;

		$showCoach = $CoachModel->where('user_id', $_POST['user_id'])->findColumn('coach_id');

		// ordre de suppression important
		$CoachCategoryModel->where('coach_id', $showCoach)->delete();
		$roles->where('user_id', $_POST['user_id'])->delete();
		$CoachModel->where('user_id', $_POST['user_id'])->delete();
		$user->delete($_POST['user_id']);


		return redirect('crud')->with('success', "l'utilisateur id $_POST[user_id] a été supprimer");
	}





	public function EditUserRole()
	{
		switch ($_POST['action']) {
			case 'edit':
				$user = new UserModel;
				$checkUser = $user->where('user_id', "$_POST[user_id]")->first();

				$data = [
					'user' => $checkUser,
					'user_role_id' => $_POST['user_role_id'],
					'user_id' => $_POST['user_id'],
					'role_id' => $_POST['role_id']
				];

				echo view('templates/themeHeader.php');
				echo view('crud/editUserRole', $data);
				echo view('templates/themeFooter');

				break;

			case 'post':

				$roles = new UserRoleModel;
				$coachModel = new CoachModel;

				if ($_POST['roles'] == $_POST['old_roles_id']) {
					return redirect('crud')->with('error', 'Les roles sélectionner sont identique.');
				} elseif ($_POST['roles'] == '2') {

					$data =
						[
							'role_id' => $_POST['roles'],
						];

					$roles->update($_POST['user_id'], $data);

					$data = [
						'user_id'   => $_POST['user_id']
					];

					$coachModel->insert($data);

					return redirect('crud')->with('success', 'Les role a été modifié et une table coach lui a été crée .');
				} elseif ($_POST['old_roles_id'] == "2") {

					$data =
						[
							'role_id' => $_POST['roles'],
						];

					$roles->update($_POST['user_id'], $data);
					$coachModel->where('user_id', $_POST['user_id'])->delete();

					return redirect('crud')->with('success', 'Les role a été modifié et la table coach associer a été supprimé .');
				} else {
					$data =
						[
							'role_id' => $_POST['roles'],
						];
					$roles->update($_POST['user_id'], $data);

					return redirect('crud')->with('success', 'le nouveaux role a été attribué.');
				}

				break;
			default:
				return redirect()->back()->with('error', 'une erreur a eu lieu');
		}
	}





	public function EditCoach()
	{
		switch ($_POST['action']) {

			case 'edit':

				$user = new UserModel;
				$checkUser = $user->where('user_id', "$_POST[user_id]")->first();

				$data = [
					'user' => $checkUser,
					'user_id' => $_POST['user_id'],
					'coach_id' => $_POST['coach_id'],
					'address' => $_POST['address'],
					'city' => $_POST['city'],
					'postcode' => $_POST['postcode'],
					'country' => $_POST['country'],
					'geocod_lat' => $_POST['geocod_lat'],
					'geocod_lng' => $_POST['geocod_lng'],
					'phone' => $_POST['phone'],
					'facebook' => $_POST['facebook'],
					'twitter' => $_POST['twitter'],
					'linkedin' => $_POST['linkedin'],
					'website' => $_POST['website'],
					'logo' => $_POST['logo'],
					'title' => $_POST['title'],
					'description' => $_POST['description'],
				];

				echo view('templates/themeHeader.php');
				echo view('crud/editCoach', $data);
				echo view('templates/themeFooter');

				break;

			case 'post':
				//conversion adresse et ville en coordonner géographique
				$vowels = array("é", "è", "ê", "ë");
				$address = str_replace($vowels, "e", str_replace(" ", "+", $_POST['address'] . " " . $_POST['city']));

				$json = file_get_contents("https://www.mapquestapi.com/geocoding/v1/address?key=" . getenv('API_KEY_MAPQUEST') . "&inFormat=kvp&outFormat=json&location=$address%2CFR&thumbMaps=false");
				$json = json_decode($json);

				$lat = $json->{'results'}[0]->{'locations'}[0]->{'latLng'}->{'lat'};
				$lng = $json->{'results'}[0]->{'locations'}[0]->{'latLng'}->{'lng'};

				$data = [
					'address' => $_POST['address'],
					'city' => $_POST['city'],
					'postcode' => $_POST['postcode'],
					'country' => $_POST['country'],
					'geocod_lat' => $lat,
					'geocod_lng' => $lng,
					'phone' => $_POST['phone'],
					'facebook' => $_POST['facebook'],
					'twitter' => $_POST['twitter'],
					'linkedin' => $_POST['linkedin'],
					'website' => $_POST['website'],
					'logo' => $_POST['logo'],
					'title' => $_POST['title'],
					'description' => $_POST['description'],
				];

				$CoachModel = new CoachModel;
				$CoachModel->update($_POST['coach_id'], $data);

				return redirect("crud")->with('success', 'Les informations on été modifier');

				break;
			default:
				return redirect()->back()->with('error', 'une erreur a eu lieu');
		}
	}





	public function addCategorie()
	{
		switch ($_POST['action']) {
			case 'edit':



				echo view('templates/themeHeader.php');
				echo view('crud/addCategory');
				echo view('templates/themeFooter');
				break;

			case 'post':

				$data = [
					'name' => $_POST['name']
				];
				$CategroyModel = new CategoryModel;
				$CategroyModel->insert($data);

				return redirect('crud')->with('success', "La categorie $_POST[name] a été ajouté");

				break;
			default:
				return redirect()->back()->with('error', 'une erreur a eut lieu');
		}
	}




	public function delCategorie()
	{

		$CategroyModel = new CategoryModel;
		$CoachCategoryModel = new CoachCategoryModel;

		$CoachCategoryModel->where('category_id', $_POST['category_id'])->delete();
		$CategroyModel->where('category_id', $_POST['category_id'])->delete();

		return redirect('crud')->with('success', "la categorie $_POST[name] a été supprimé .");
	}




	public function EditCategorie()
	{
		switch ($_POST['action']) {
			case 'edit':
				$data = [
					'category_id' => $_POST['category_id'],
					'name' => $_POST['name'],
				];

				echo view('templates/themeHeader.php');
				echo view('crud/editCategory', $data);
				echo view('templates/themeFooter');

				break;

			case 'post':
				$data = [
					'name' => $_POST['name']
				];
				$CategroyModel = new CategoryModel;
				$CategroyModel->update($_POST['category_id'], $data);

				return redirect('crud')->with('success', "le nom de la categorie id $_POST[category_id] a été modifié");
				break;
			default:
				return redirect()->back()->with('error', 'une erreur a eut lieu');
		}
	}
}
