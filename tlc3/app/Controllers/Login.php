<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\CoachModel;
use App\Models\UserRoleModel;
use App\Models\CoachCategoryModel;
use App\Database\Migrations\CoachCategory;

$session = \Config\Services::session();

class Login extends BaseController
{
    public function index()
    {
        // initialisation model et session
        $user = new UserModel;
        $roles = new UserRoleModel;
        $session = session();

        // dd($_POST);

        //récupération info table user via email
        $checkUser = $user->where('email', "$_POST[email]")->first();

        
        if ($checkUser == null) {
            //si null stock message erreur en session flash et redirige
   
            return redirect()->back()->with('error','mail incorrect');
        } else {
            if ($checkUser['password'] == hash(getenv('HASH'), "$_POST[password]")) {

                //si MDP correspond avec MDP BDD 
                $checkRole = $roles->where('user_id', "$checkUser[user_id]")->first();

                //check BDD role & stock info utile en variable session
                
                $_SESSION['role_id'] = $checkRole['role_id'];
                $_SESSION['user_id'] = $checkUser['user_id'];
                $_SESSION['first_name'] = $checkUser['first_name'];
                $_SESSION['last_name'] = $checkUser['last_name'];
                $_SESSION['picture'] = $checkUser['picture'];

                // stock message success en session flash et redirige
              
                return redirect()->back()->with('success',"Bienvenue $checkUser[first_name] $checkUser[last_name] ");
            }
            //si MDP ne correspond pas avec MDP BDD stock message erreur en session flash et redirige
          
            return redirect()->back()->with('error','mot de passe incorrect');
        }
    }
    public function register()
    {

         // initialisation model et session
        $user = new UserModel;
        $roles = new UserRoleModel;
        $session = session();

        //récupération info table user via email
        $checkUser = $user->where('email', "$_POST[email]")->first();
        if ($checkUser == null) {
            //si null initie les données à envoyer à la BDD user et envoie
            $data = [
                'first_name'  => "$_POST[first_name]",
                'last_name'   => "$_POST[last_name]",
                'email'       => "$_POST[email]",
                'password'    =>  hash(getenv('HASH'), "$_POST[password1]")
            ];
            $user->insert($data);

            //récupération info table user via email
            $User = $user->where('email', "$_POST[email]")->first();

            //si null initie les données à envoyer à la BDD user_role et envoie
            $data = [
                'user_id'   => $User['user_id'],
                'role_id'   => $_POST['roles']
            ];
            $roles->insert($data);

            //récupération info table user_role via user_id
            $checkRole = $roles->where('user_id', "$User[user_id]")->first();

            //création de la table coach correspondante
            if ($checkRole['role_id'] == 2) {
                $coachModel = new CoachModel;
                
                $data = [
                    'user_id'   => $User['user_id']
                ];
                
                $coachModel->insert($data);

            }

            //stock info utile en variable session
            $_SESSION['role_id'] = $checkRole['role_id'];$coach_id = $coachModel->where('user_id', $User['user_id'])->findColumn('coach_id');

            $_SESSION['user_id'] = $User['user_id'];
            $_SESSION['first_name'] = $User['first_name'];
            $_SESSION['last_name'] = $User['last_name'];
            $_SESSION['picture'] = $User['picture'];

            // stock message success en session flash et redirige


            return redirect()->to('/profil/update')->with('success',"Bienvenue parmi nous $checkUser[first_name] $checkUser[last_name] ");
        }
        


        return redirect()->back()->with('error','profil existant');
    }
    public function logout()
    {
        $session = session();
        $session->destroy();
       
        return redirect()->to('/');
    }
}
