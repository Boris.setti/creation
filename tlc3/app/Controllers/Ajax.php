<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\StatClicModel;
use App\Models\StatUserModel;
use App\Models\StatPosModel;

class Ajax extends BaseController {

	//========================================================================================
    public function test(){
    	// NB: il faudra peut être que j'utilise un gestionnaire d'erreur
    	// pour renvoyer plus ou moins de détails en fonction de l'environnement. 
    	//debugg//
    	if(ENVIRONMENT !== 'production'){
        	echo "test du ajax controller";
        }
    } // public function test()

	//========================================================================================
    public function statClic(){
    	/*
    		insère une ligne dans la table "StatClic", avec les données
    		recues par la méthode POST
    	*/

    	// debug
    	$this->infoDebug("Ajax controller, méthode statClic");

		// instance du modèle
		$model = new StatClicModel();

		// Les noms des champs à récupérer correspondent exactement aux champs de la 
		// base de données.
		$nomChampsARecuperer = $model->allowedFields;

		// insertion dans la base de données
		$result = $this->insertDB($model, $nomChampsARecuperer);
    } // public function statClic()

    //========================================================================================
    public function statPos(){
        /*
            insère une ligne dans la table "StatPos", avec les données
            recues par la méthode POST
        */

        // debug
        $this->infoDebug("Ajax controller, méthode statPos");

        // instance du modèle
        $model = new StatPosModel();

        // Les noms des champs à récupérer correspondent exactement aux champs de la 
        // base de données.
        $nomChampsARecuperer = $model->allowedFields;

        // insertion dans la base de données
        $this->insertDB($model, $nomChampsARecuperer);
    } // public function statPos()

    //========================================================================================
    public function statUser(){
        /*
            insère une ligne dans la table "StatUser", avec les données
            recues par la méthode POST
        */

        // debug
        $this->infoDebug("Ajax controller, méthode statUser");

        // instance du modèle
        $model = new StatUserModel();

        // Les noms des champs à récupérer correspondent exactement aux champs de la 
        // base de données.
        $nomChampsARecuperer = $model->allowedFields;

        // insertion dans la base de données
        $this->insertDB($model, $nomChampsARecuperer);
    } // public function statUser()

    //========================================================================================
    private function fieldsReceptionAndMapping($model, $nomChampsARecuperer){
    	/*
    		Cette fonction permet 
    		- de récupérer les données
    		- de modifier les noms des champs pour les faire correspondre à ceux de la base
    		  de données.
    		- de les mettre en forme pour une insertion en base de données

    		Elle sert notamment pour les insertion en base de données.

    		Entrées : 
    		---------
    			$model : une instance du modèle
    			$nomChampsARecuperer : le nom des champs à récupérer.
    								   Pour le mapping : chaque champ de $nomChampsARecuperer
    								   a un champ correspondant dans $model->allowedFields.
    								   
		   Sorties : 
		   ---------
		   		$data : les données prêtes à être enregistrées en base de donnée

    	*/

		// récupération des champs
		$recuperes = $this->request->getPOST($nomChampsARecuperer);

		// on adapte les noms des champs
		$data = array();
		$num = 0;
		foreach ($recuperes as $key => $value){
			$data[$model->allowedFields[$num]] = $value;
			$num++;
		}

		return $data;
    } // private function  fieldsReceptionAndMapping()

	//========================================================================================
    private function insertDB($model, $nomChampsARecuperer){
    	/*
    		Cette fonction permet : 
    		- récupérer des donées via POST
    		- de les faire correspondre au champs de la table de la base de données
    		- de les insérer dans la table
    		- d'envoyer des infos sur le résultat de l'insertion, selon l'environnement

       		Entrées : 
	  		---------
    			$model : une instance du modèle
    			$nomChampsARecuperer : le nom des champs à récupérer.
    								   Pour le mapping : chaque champ de $nomChampsARecuperer
    								   a un champ correspondant dans $model->allowedFields.
    	*/

		// reception et mapping des données
		$data = $this->fieldsReceptionAndMapping($model, $nomChampsARecuperer);

		// insertion dans la base de données
		$result = $model->insert($data);

		// infos sur les résultats
		$this->infoResult($result);

		// envoie la réponse
		$this->response->setStatusCode(200)->send();
    } // private function insertDB()

	//========================================================================================
    private function infoDebug($message){
    	/*
    		selon l'environnement, envoie des infos de debug
    	*/
    	if(ENVIRONMENT !== 'production'){
    		echo '<p>'.$message.'</p>';
    	}
    } // private function infoDebug()

	//========================================================================================
    private function infoResult($result){
    	/*
    		Selon l'environnement, renvoie des infos sur le résultat
    		-
    	*/
		if(ENVIRONMENT !== 'production'){
			if($result){
				echo "<p> insertion OK </p>";
			} else {
				echo "<p> insertion failed </p>";
			}
		}
    } // private function infoResult


    //========================================================================================

} // class Ajax