<?php namespace App\Controllers;
$session = \Config\Services::session();
$session->start();


class Book extends BaseController
{
	public function index()
	{  
		echo view('templates/themeHeader');
        echo view('pages/book');
        echo view('templates/themeFooter');
	}

}
