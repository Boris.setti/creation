<?php namespace App\Controllers;
$session = \Config\Services::session();
$session->start();


class Formation extends BaseController
{
	public function index()
	{  
		echo view('templates/themeHeader');
        echo view('pages/formation');
        echo view('templates/themeFooter');
	}

}
