<?php

namespace App\Database\Seeds;

class Roles extends \CodeIgniter\Database\Seeder
{
    public function run()
    {

        $data = [
            'name'  => 'Administrator',
            'slug'  => 'Admin'
        ];
        $this->db->table('roles')->insert($data);

        $data = [
            'name'  => 'Coaching',
            'slug'  => 'Coach'
        ];
        $this->db->table('roles')->insert($data);

        $data = [
            'name'  => 'User',
            'slug'  => 'User'
        ];
        $this->db->table('roles')->insert($data);

        
    }
}
