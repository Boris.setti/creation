<?php

namespace App\Database\Seeds;

class Categories extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = [
            'name' => 'Autre',
        ];
        $this->db->table('categories')->insert($data);
        $data = [
            'name' => 'Développement personnel',
        ];
        $this->db->table('categories')->insert($data);
        $data = [
            'name' => 'Santé',
        ];
        $this->db->table('categories')->insert($data);
        $data = [
            'name' => 'Sport',
        ];
        $this->db->table('categories')->insert($data);
        $data = [
            'name' => 'Buisness',
        ];
        $this->db->table('categories')->insert($data);
        $data = [
            'name' => 'Psychologie',
        ];
        $this->db->table('categories')->insert($data);
    }
}
