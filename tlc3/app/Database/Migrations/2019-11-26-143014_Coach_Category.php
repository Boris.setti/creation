<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoachCategory extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
		$this->forge->addField([
			'coach_category_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'coach_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			],
			'category_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'null'			 =>	TRUE
			]
		]);
		$this->forge->addForeignKey('coach_id', 'coachs', 'coach_id');
		$this->forge->addForeignKey('category_id', 'categories', 'category_id');
		$this->forge->addKey('coach_category_id', TRUE);
		$this->forge->createTable('coach_category');
		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('coach_category');
	}
}
