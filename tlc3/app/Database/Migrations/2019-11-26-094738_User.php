<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

use function PHPSTORM_META\type;

class User extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'user_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'first_name'	=> [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'last_name'		=> [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'email'			=> [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'unique'		 => TRUE,
			],
			'picture'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '255',
				'default'	 	 => "/images/user-profile-avatar.jpg",
			],
			'password'		=> [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			]
		]);
		$this->forge->addKey('user_id', TRUE);
		$this->forge->createTable('users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
