######## MAIN LOOP : Casquette Hackaton 
## CREDITS 
#
#
#
#
#
#
## NOTES
#
#
#
##

##URL
url_getPOI="http://sd-59247.dedibox.fr/vizir/getPOI.php?spreadsheetId=1GMdqL_XDFQBOEI4-Ff5UTF3GeLKmUBZIyBm6AArJW3I"
url_setPOI="sd-59247.dedibox.fr/vizir/setPOI.php?spreadsheetId=1GMdqL_XDFQBOEI4-Ff5UTF3GeLKmUBZIyBm6AArJW3I&latitude=[LAT]&longitude=[LON]"
url_getLEDPoulailler="http://sd-59247.dedibox.fr/vizir/getLED.php?spreadsheetId=1GMdqL_XDFQBOEI4-Ff5UTF3GeLKmUBZIyBm6AArJW3I&feuille=LED_poulailler"
url_getLEDTraitement="http://sd-59247.dedibox.fr/vizir/getLED.php?spreadsheetId=1GMdqL_XDFQBOEI4-Ff5UTF3GeLKmUBZIyBm6AArJW3I&feuille=LED_traitement"

#Imports
import sys
sys808.path.insert(1, 'lib/')
import SIM808 as s808
import spreadsheet as s
import LED as l
import time

#INIT 
#boussole_init()
#gyro_init()
#accelero_init()
s808.init_sim808(s808.sim808_port,s808.sim808_simpin) ##ces 2 variables sont initiliasées dans SIM808.py
#start GPS for first time
s808.GPS_start() #call s808.GPS_stop() to save battery, new hot fix in a few sconds !
s808.HTTP_init(s808.sim808_simapn)

pois=s808.HTTP_requestGET(url_getPOI)#--> A faire dans spreadsheet.py
#process POI

#########################
########  LOOP  #########
#########################
time_sec = time.time()
time_min = time.time()
while 1: 
    now = time.time()

    ####Execution instantannée
    #READ Boussole --> à mettre dans exec secondes ? Est-ce lourd en CPU ? 
    #ex:   boussole_read()
    #READ Gyro

    #Process position 

    #READ buttons


    ####Execution secondes 
    if(now-start > 1):
        time_sec=now
        print('\'')

        #READ GPS in background
        s808.GPS_manage()#prints nothing except if GPS is not started
        #READ serial port SMS received ? 
        #-> take actions 

        #Process display LED 
        l.update_LED()
        #Process display OLED 

    ####Execution minutes 
    if(now-start > 60):
        print('\'\'')
        time_min=now

        #sleep if no activity movement ? 

        #GET pois updated
        s.update_POI()
