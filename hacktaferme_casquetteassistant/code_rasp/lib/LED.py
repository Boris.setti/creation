#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#choix entre la lib neopixel (testé et fonctionnel sur RPI0 python3.7.3), ou la lib rpi_ws281x
# -->  Procédure d'installation rpi_ws281x https://tutorials-raspberrypi.com/connect-control-raspberry-pi-ws2812-rgb-led-strips/
#sudo nano /etc/modprobe.d/snd-blacklist.conf
#blacklist snd_bcm2835
#sudo nano /boot/config.txt
#  then set #dtparam=audio=on
#git clone https://github.com/jgarff/rpi_ws281x
#cd rpi_ws281x/
#sudo scons
#cd python
#sudo python setup.py build
#sudo python setup.py install
#

import time
import board
import neopixel

import sys
sys808.path.insert(1, 'lib/')
import spreasheet


# On a Raspberry pi, use this instead, not all pins are supported
pixel_pin = board.D18
# The number of NeoPixels
num_pixels = 16

# RGB GRB RGBW GRBW
ORDER = neopixel.RGBW
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.1, auto_write=False,pixel_order=ORDER)

def led_off():
    pixels.fill((0, 0,0))
    pixels.show()


def set_LED_color():
    global LED1, LED2, LED3, LED4, LED5, LED6, LED7
    #





def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b) if ORDER == neopixel.RGB or ORDER == neopixel.GRB else (r, g, b, 0)

def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(wait)
