### Library for SIM800 series on RaspberryPI
#     name : SIM808.py
#     Implementing AT commands according to AT_commands_V1.09
#     Copyright (C) 2019  Nicolas Daüy

#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program; see the file COPYING . If not, write to the
#     Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#     Please send bugreports with examples or suggestions to
#       nicolas.dauy@gmail.com


##CONFIG
sim808_port="/dev/ttyAMA0"

#USER variables 
sim808_simpin=""
sim808_simapn="free"

import requests
import serial
# import domoticz
import time

script_call="/home/pi/hacktaferme_casquetteassistant/code_rasp/lib/SIM808.py"

#GPS (if enabled) #global variables updated inside functions
gps_HDOP_limit=3.2
gps_started=0
gps_lat=""
gps_lon=""
gps_lat=""
gps_lon=""
gps_altitude=""
gps_SOV=""
gps_COV=""
gps_sat_in_view=""
gps_sat_used=""

try:
    port = serial.Serial(sim808_port, baudrate=9600, timeout=0.4)
except Exception as e:
    print(e)
    # domoticz.log(script_call+" ERROR - opening Com port Failed")
    # domoticz.log(format(e))



def init_sim808(my_port,pin):
    # Enable Serial Communication
    # my_port : "/dev/ttyS1", "/dev/ttyAMA0", "/dev/ttyUSB0"
    #example : port = serial.Serial("/dev/ttyS1", baudrate=9600, timeout=0.5)
    test_com=0
    
    # Test AT comunication : 3 attemps
    for x in range(3):
        port.write(b'AT\r')
        rcv = port.read(10)

        print(rcv)
        if 'OK' in str(rcv):
            test_com=1
            break
        else:
            time.sleep(1)

    time.sleep(0.1)
    if test_com==1:
        # Disable the Echo
        port.write(b'ATE0\r')      
        rcv = port.read(10)
        print(rcv)
        time.sleep(0.1)
       
        # set in text mode 
        port.write(b'AT+CMGF=1\r')      
        rcv = port.read(10)
        print(rcv)
        time.sleep(0.1)

        #Unlock pin 
        if pin!="":
            # domoticz.log("Entering PIN")
            AT_pin='AT+CPIN='+pin+'\r'
            port.write(AT_pin.encode())      
            rcv = port.read(10)
        print(rcv)
        time.sleep(0.1)

        # Save AT&W 
        #port.write(b'AT&W\r')      
        #rcv = port.read(10)
        #print(rcv)

    time.sleep(0.1)

    return test_com


def GPS_start():
    global gps_started,gps_hdop,gps_pdop,gps_vdop,gps_lat,gps_lon,gps_altitude,gps_SOV,gps_COV,gps_sat_in_view,gps_sat_used
    # GPS power = 1
    port.write(b'AT+CGPSPWR=1\r')      
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)
    #Seek RMC sentence
    port.write(b'AT+CGNSSEQ="RMC"\r')      
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)
    #reset var
    gps_started=1
    gps_lat=""
    gps_lon=""
    gps_lat=""
    gps_lon=""
    gps_altitude=""
    gps_SOV=""
    gps_COV=""
    gps_sat_in_view=""
    gps_sat_used=""

def GPS_stop():
    global gps_started
    port.write(b'AT+CGPSPWR=0\r')      
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)
    gps_started=0

def GPS_manage():
    global gps_started,gps_hdop,gps_pdop,gps_vdop,gps_lat,gps_lon,gps_altitude,gps_SOV,gps_COV,gps_sat_in_view,gps_sat_used
    if gps_started==1:
        port.write(b'AT+CGNSINF\r')
        nmea = port.read(1000).decode("utf-8")
        time.sleep(0.1)
        try:
            gps_fix_status=nmea.split(',')[1]
            if gps_fix_status == '1':
                gps_hdop=nmea.split(',')[10]
                gps_pdop=nmea.split(',')[11]
                gps_vdop=nmea.split(',')[12]
                if float(gps_pdop) <= gps_HDOP_limit:
                    print("GPS position set")
                    gps_lat=nmea.split(',')[3]
                    gps_lon=nmea.split(',')[4]
                    gps_altitude=nmea.split(',')[5]
                    gps_SOV=nmea.split(',')[6]#speed over ground
                    gps_COV=nmea.split(',')[7]
                    gps_sat_in_view=nmea.split(',')[14]
                    gps_sat_used=nmea.split(',')[15]
                    return 1
                print("GPS found")
        except:
            print("Unable to parse GNSINF response")
            return 0
    else:
        print("GPS not started")
        return 0


def send_SMS(phone_number,message):
    port.write(b'AT+CMGF=1\r')  # Select Message format as Text mode 
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)

    port.write(b'AT+CNMI=2,1,0,0,0\r')   # New SMS Message Indications
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)

    # Sending a message to a particular Number
    AT_phone='AT+CMGS="'+phone_number+'"\r'
    port.write(AT_phone.encode())
    rcv = port.read(10)
    print(rcv)
    time.sleep(0.1)

    # Message
    port.write(message.encode())  
    port.write(b'\r')
    rcv = port.read(10)
    print(rcv)

    port.write(b'\x1A') # Enable to send SMS
    for i in range(10):
        rcv = port.read(10)
        print(rcv)


def HTTP_init(apn):
    AT_simapn='AT+SAPBR=3,1,"APN","'+apn+'"\r'
    port.write(AT_simapn.encode())  # Select Message format as Text mode 
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.3)

    port.write(b'AT+SAPBR=3,1,"CONTYPE","GPRS"\r')
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    port.write(b'AT+SAPBR=1,1\r')  # Select Message format as Text mode 
    time.sleep(1)
    rcv = port.read(100)
    print(rcv)
    if 'OK' in str(rcv):
        return 1
    else:
        return 0

def HTTP_close():
    port.write(b'AT+SAPBR=0,1\r')  # Select Message format as Text mode 
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

def HTTP_requestGET(url):
    port.write(b'AT+HTTPINIT\r')  # Select Message format as Text mode 
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    #port.write(b'AT+HTTPPARA="CONTENT", "application / x - www - form - urlencoded\r\nConnection: close"\r')   # New SMS Message Indications
    port.write(b'AT+HTTPPARA="CID",1\r')
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    
    # Sending a message to a particular Number
    AT_URL='AT+HTTPPARA="URL","'+url+'"\r'
    print(AT_URL)
    port.write(AT_URL.encode())
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    port.write(b'AT+HTTPACTION=0\r')
    for i in range(40):#40secondes!
        rcv = port.read(100)
        print(rcv)
        if '200' in str(rcv):
            print("success")
            port.write(b'AT+HTTPREAD=0,2000\r')
            time.sleep(0.5)
            reponsehttp = port.read(2000)
            print(reponsehttp)
            time.sleep(0.1)
            port.write(b'AT+HTTPTERM\r')
            return reponsehttp


def HTTP_jsonPOST(url,json_data):

    ##FOR TEST 
    json_data = {"param":"value","param2":0.01}

    port.write(b'AT+HTTPINIT\r')  # Select Message format as Text mode 
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    port.write(b'AT+HTTPPARA="CONTENT","application/json"\r')   # New SMS Message Indications
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    # Sending a message to a particular Number
    AT_URL='AT+HTTPPARA="URL","'+url+'"\r'
    print(AT_URL)
    port.write(AT_URL.encode())
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    # Sending a message to a particular Number
    port.write(b'AT+HTTPDATA=1500,5000\r')
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    port.write(json.dumps(json_data).encode())
    rcv = port.read(100)
    print(rcv)
    time.sleep(0.1)

    port.write(b'AT+HTTPACTION=1\r')
    for i in range(10):
        rcv = port.read(100)
        print(rcv)
        if '200' in str(rcv):
            print("success")
            return 1


print("Sim808 GSM Library loaded")