#!/bin/bash
#list toutes les étapes à éxecuter pour installer complètement un dispositif neuf. 


## Register running.sh in CRON
#--> TODO

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install libffi-dev libbz2-dev libffi-dev liblzma-dev libsqlite3-dev libncurses5-dev libgdbm-dev zlib1g-dev libreadline-dev libssl-dev tk-dev build-essential libncursesw5-dev libc6-dev openssl git

#PYTHON 
#sudo apt purge python2.7-minimal
# wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz
# tar xf Python-3.7.4.tar.xz
# cd Python-3.7.4
# ./configure --enable-optimizations
# make -j 4
# sudo make altinstall
#pip 
sudo apt install python3-pip
#aliases
# ln -s /usr/local/bin/python3.7 /usr/local/bin/python3
# sudo ln -s /usr/local/opt/python-3.7.4/bin/pydoc3.7 /usr/bin/pydoc3.7
# sudo ln -s /usr/local/opt/python-3.7.4/bin/python3.7 /usr/bin/python3.7
# sudo ln -s /usr/local/opt/python-3.7.4/bin/python3.7m /usr/bin/python3.7m
# sudo ln -s /usr/local/opt/python-3.7.4/bin/pyvenv-3.7 /usr/bin/pyvenv-3.7
# sudo ln -s /usr/local/opt/python-3.7.4/bin/pip3.7 /usr/bin/pip3.7
# alias python='/usr/bin/python3.7'
# alias python3='/usr/bin/python3.7'
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1 #python3.7.3
#sudo update-alternatives --install /usr/bin/python python /usr/local/bin/python3.7 2 #python3.7.4
update-alternatives --config python

# cd ../
# rm -Rf Python-3.7.4
# rm Python-3.7.4.tar.xz
#packages 
sudo pip3 install --upgrade setuptools pip
sudo pip3 install requests
sudo pip3 install pyserial
sudo pip3 install RPI.GPIO
sudo pip3 install adafruit-circuitpython-neopixel

sudo pip3 install --force-reinstall adafruit-blinka
sudo pip3 install thingspeak

#LED RGB --> lib alternative sur python 2.7
sudo pip3 install rpi_ws281x
sudo nano /etc/modprobe.d/snd-blacklist.conf
#  then set #dtparam=audio=on
git clone https://github.com/jgarff/rpi_ws281x
cd rpi_ws281x/
sudo scons
cd python
sudo python setup.py build
sudo python setup.py install

#spreadsheet 

pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
