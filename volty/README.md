# QUICK START



### Get started !

Bonjour Selfcity ! 
Voici un petit manuel d'utilisation du site web **VOLTY**, faites-en bon usage :=)

## ÉTAPE 1:
Il vous faudra cloner le **repo Git du projet Volty** : 
[Projet Volty](https://gitlab.com/boris-thomas-myl-ne/volty)

Pour ce, dans le `terminal` marquez :

`git clone [URL projet gitlab]`

Une fois ce repo cloné, il faudra *double-cliquer* sur le fichier nommé `index.html`.
Si vous voulez tester cette application en **local**, il faut aller dans le terminal avant ça et faire :

`npm install`

--> Afin d'ajouter toutes les dépendances du projet.

**OU**

Il vous faudra aller sur le lien du site de préproduction avec cette URL:
[Site hébergé](http://volty.labege.srv.simplon.me/)

## ÉTAPE 2:
Ensuite le plombier va arriver sur la page de connexion où il devra **s'identifier via son compte Google**
Une fois identifié, il aura accès à son **calendrier de la journée.**
Il devra alors *cliquer* sur la mission à effectuer.

## ÉTAPE 3:
Une fois la mission choisie, celui-ci aura 2 possibilités :

En effet, il y aura 2 boutons *cliquables* qui s'afficheront :

- Un bouton "__J'arrive__": il générera une notification Slack qui prévient les standardistes de l'arrivée de ce plombier chez le client actuel.

- Un bouton "__Je pars__": il générera une notification Slack qui prévient les standardistes du départ de ce plombier chez le client actuel.

## TERMINÉ !!

---

## A l'attention de `Maxime Pawlak`:

Voici un petit tuto afin d'ajouter un **Auth 2.0** à l'**API Google CALENDAR** créée pour ce projet.
Tout d'abord, l'ID client généré par le fameux "Auth 2.0" sert à obtenir cette vérification :

![Screenshot](https://gitlab.com/boris-thomas-myl-ne/volty/raw/master/img/screen2.png)

|
|

Ici le plombier sélectionne son compte Google puis **c'est son calendrier qui est affiché.**

![Screen2](https://gitlab.com/boris-thomas-myl-ne/volty/raw/master/img/screen.png)

---

### POUR AJOUTER UN **AUTH 2.0** : 

1. Aller sur le [Google Cloud Platform](https://console.developers.google.com/apis) du projet Volty avec l'adresse mail du plombier concerné.

2. Pour créer votre **CLIENT ID** il faut créer le projet Volty sur votre compte Google. 

3. Alors vous pourrez créer deux choses :

- les **Clés API** (NE PAS TOUCHER)

- les **autorisations OAuth 2.0**

C'est donc ici que vous ajouterez 1 OAuth 2.0 à chaque plombier en plus ;)

---

### POUR GÉRER L'APPLI "BOT" VIA SLACK :

Notre **API Slack** utilise des **Webhooks entrants qui créent le lien entre Slack et notre code.** Tout les webhooks sont nommés *"Bot"*. 

Il faudra ensuite sur cette page *cliquer* sur **"Ajouter une configuration"** pour créer de nouveaux Webhooks

Ce webhook que j'ai créé est **propre à un seul channel sur le serveur Slack Selfcity** (ici, dans le channel test). 
Voici le lien vers la création des webhooks: [Bot](https://selfcity.slack.com/apps/A0F7XDUAZ-webhooks-entrants?next_id=0)

**Pour un prochain channel** : vous pouvez bien entendu ajouter ce webhook à n'importe quel channel que vous voudrez.
Puis dans **le code fourni** par notre groupe, il ne restera plus qu'à changer la variable *"urlSlack"* qui fournira l'accès au nouveau webhook.

##### Exemple: 
`urlSlack = {
    "michel.dupont31400@gmail.com": "https://hooks.slack.com/services/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",
    "martin.lelutin31@gmail.com": "https://hooks.slack.com/services/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",
    "lefoufoufifou04@gmail.com": "https://hooks.slack.com/services/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"
}` 

Ce tableau représente *3 mails test* qui seront remplacés par **les vraies adresses mails** des plombiers et s'il y a un plombier en plus, on rajoute une ligne avec : 
- le mail du nouveau plombier
- qui est suivi d'un ":"
- et ensuite vous avez juste à copier l'URL du nouveau webhook et le mettre entre guillemets

#### ENJOY ! 

---

### A L'OCCASION D'UNE V2, J'AI RÉALISÉ UN LOGO D'APPLICATION QUE VOUS POURREZ AJOUTER SI CELA VOUS CONVIENT OU LE MODIFIER :

Voici le lien : [LOGO](https://gitlab.com/boris-thomas-myl-ne/volty/blob/master/img/VOLTY%20PROJECT.jpg)

Et également pour Selfcity: [LOGO](https://gitlab.com/boris-thomas-myl-ne/volty/blob/master/img/SELFCITY.jpg)

Et version réduite: [Volty](https://gitlab.com/boris-thomas-myl-ne/volty/blob/master/img/SELFCITY.png)

[Selfcity](https://gitlab.com/boris-thomas-myl-ne/volty/blob/master/img/VOLTY%20PROJECT.png)

À prendre ou à laisser ! 

---

La **V1** est utilisable avec **un seul utilisateur.** Il faudra juste remplacer le webhook que vous créerez par celui du plombier afin que l'appli soit fonctionnelle.

La **V2** propose un tableau de **plusieurs utilisateurs**, on peut rajouter autant de plombiers que voulu.
Il faut juste le rajouter dans le tableau comme indiqué ci-dessus.

---

[Volty-Trello](https://trello.com/b/NebQl4ZR/volty)

[Serveur-préproduction](http://volty.labege.srv.simplon.me/)