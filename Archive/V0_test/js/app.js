$("#test").on("click", function() {

    alert(`Tu as cliqué sur un bouton inconnu pauvre fou!!!!!

Tu as de la chance, je suis sympa ,alors je te mets un générateur de phrase sympa. Enjoy ;)`);

    $("#btn-phrases").show(500)
});

$("#btn-phrases").click(function(e) {

    // var parentOffset = $(this).offset()
    // cursorX = e.pageX - parentOffset.left;
    // cursorY = e.pageY - parentOffset.top;
    // console.log(cursorX)

    $(".phrase").remove();
    var minNumber = 1; // le minimum
    var maxNumber = 47; // le maximum
    var randomnumber = Math.floor(Math.random() * (maxNumber) + minNumber);
    var phrase = phrases[randomnumber]
    $('#phrase-aleatoire').append(`<h3 class="phrase">${phrase}</h3>`);

});



var phrases = {
    1: "Tu n'as jamais été aussi proche de l'arrivée.",
    2: 'Inspire, expire.',
    3: 'Tu es une bonne personne !',
    4: 'Tes collègues sont dans la même merde.',
    5: 'Fais-toi un câlin !',
    6: 'Je crois en toi !',
    7: 'Joue avec le petit bouton rouge qui tourne sur ton clavier.',
    8: 'Pose un RTT !',
    9: 'Tue un moustique !',
    10: 'Propose ton aide, quitte à être bloqué.',
    11: 'Je comprends.',
    12: "Hmm, j'entends bien..",
    13: 'Force-toi à sourire, ton cerveau croira que tu te sens bien !',
    14: 'La force est avec toi !',
    15: 'Pingouin dans les champs, hiver très méchant.',
    16: 'Corrige des fautes.',
    17: 'Il existe forcément une solution.',
    18: 'Mange du mafé !',
    19: 'Va fumer si tu es fumeur, sinon, rappuie sur le bouton.',
    20: 'Fais des trucs !',
    21: "Les ornithorynques c'est la vie !",
    22: "Les alpagas, c'est encore plus la vie !",
    23: 'Demande à ton voisin...',
    24: 'Demande à TERMINALTOR !',
    25: "JavaScript c'est comme avoir un coup de soleil et que quelqu'un te tape constamment dessus...",
    26: 'RTFM Imbécile !',
    27: 'PEBKAC !',
    28: 'Ceci est une réponse.',
    29: 'Chocolatine.',
    30: '42 !',
    31: 'Obi-wan Kenobi',
    32: "C'est obvious !",
    33: 'Peut-on égorger une fourmi ?',
    34: 'D : La réponse D',
    35: 'Petit poney, petit poney, tu es tout gris et tout petit, petit Poney !',
    36: 'Regarde-moi !',
    37: 'Demande à une âme charitable de te faire un petit massage :)',
    38: 'Fais des bulles !',
    39: 'Lance un SOS à Poney',
    40: "Lève-toi et lance un SOS à l'attention de toute la promo.",
    41: 'Adopte un animal.',
    42: 'Sens-toi roseau !',
    43: "It's a final countdown",
    44: "I'm blue da be dee da be da",
    45: 'Go Johnny go go.... Johnny be good !',
    46: "It's dangerous to go alone. Take this !",
    47: 'Ferme les yeux et ouvre tes chakras.'
};