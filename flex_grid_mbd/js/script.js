//LES BOUTONS DU GRID

// BOUTON DU GRID TEMPLATE COLUMNS
$("#btn-grid-template-columns").click(function (){
    if($("#grid-template-column").hasClass("grid_temp_col")){
        $("#grid-template-column").removeClass('grid_temp_col')
    }else{
        $("#grid-template-column").addClass('grid_temp_col')    
    }
 });

 // BOUTON DU GRID TEMPLATE ROWS
$("#btn-grid-template-row").click(function (){
    if($("#grid-template-row").hasClass("grid_temp_row")){
        $("#grid-template-row").removeClass('grid_temp_row')
    }else{
        $("#grid-template-row").addClass('grid_temp_row')        
    }
 });

  // BOUTON DU GRID COLUMNS GAP
$("#btn-grid-column-gap").click(function (){
    if($("#grid-column-gap").hasClass("grid-col-gap")){
        $("#grid-column-gap").removeClass('grid-col-gap')
    }else{
        $("#grid-column-gap").addClass('grid-col-gap')        
    }
 });

   // BOUTON DU GRID ROW GAP
$("#btn-grid-row-gap").click(function (){
    if($("#grid-row-gap").hasClass("grid-ro-gap")){
        $("#grid-row-gap").removeClass('grid-ro-gap')
    }else{
        $("#grid-row-gap").addClass('grid-ro-gap')        
    }
 });

    // BOUTON DU GRID GAP
$("#btn-grid-gap").click(function (){
    if($("#grid-gap").hasClass("grid_gap")){
        $("#grid-gap").removeClass('grid_gap')
    }else{
        $("#grid-gap").addClass('grid_gap')        
    }
 });

     // BOUTON DU GRID COLUMN START
$("#btn-grid-col-start").click(function (){
    if($("#boite1").hasClass("boite1")){
        $("#boite1").removeClass('boite1')
    }else{
        $("#boite1").addClass('boite1')        
    }
 });

      //  BOUTON DU GRID COLUMN END
$("#btn-grid-col-end").click(function (){
    if($("#boite5").hasClass("boite5")){
        $("#boite5").removeClass('boite5')
    }else{
        $("#boite5").addClass('boite5')        
    }
 });

 //  BOUTON DU GRID COLUMN 
$("#btn-grid-col").click(function (){
    if($("#boite9").hasClass("boite9")){
        $("#boite9").removeClass('boite9')
    }else{
        $("#boite9").addClass('boite9')        
    }
 });

 //  BOUTON DU GRID ROW START
$("#btn-grid-row-start").click(function (){
    if($("#boite13").hasClass("boite13")){
        $("#boite13").removeClass('boite13')
    }else{
        $("#boite13").addClass('boite13')        
    }
 });

 //  BOUTON DU GRID ROW END
$("#btn-grid-row-end").click(function (){
    if($("#boite17").hasClass("boite17")){
        $("#boite17").removeClass('boite17')
    }else{
        $("#boite17").addClass('boite17')        
    }
 });

 //  BOUTON DU GRID ROW 
$("#btn-grid-row").click(function (){
    if($("#boite21").hasClass("boite21")){
        $("#boite21").removeClass('boite21')
    }else{
        $("#boite21").addClass('boite21')        
    }
 });

  //  BOUTON DU GRID AREA 
$("#btn-grid-area").click(function (){
    if($("#boite25").hasClass("boite25")){
        $("#boite25").removeClass('boite25')
    }else{
        $("#boite25").addClass('boite25')        
    }
 });


 //  LES BOUTONS DU FLEX V1

   //  BOUTON DU JU FLEX END

// $("#btn-ju-flex-end").click(function (){
//     if($("#ju-flex-end").hasClass("ju-flex-end")){
//         $("#ju-flex-end").removeClass('ju-flex-end')
//     }else{

//         $("#ju-flex-end").addClass('ju-flex-end')  
//         }    
// });

//   //  BOUTON DU JU FLEX CENTER

// $("#btn-ju-center").click(function (){
//     if($("#ju-center").hasClass("ju-center")){
//         $("#ju-center").removeClass('ju-center')
//     }else{

//         $("#ju-center").addClass('ju-center')  
//         }    
// });

//   //  BOUTON DU JU FLEX SPACE BETWEEN

// $("#btn-ju-space-between").click(function (){
//     if($("#ju-space-between").hasClass("ju-space-between")){
//         $("#ju-space-between").removeClass('ju-space-between')
//     }else{

//         $("#ju-space-between").addClass('ju-space-between')  
//         }    
// });

//  //  BOUTON DU JU FLEX SPACE AROUND

// $("#btn-ju-space-around").click(function (){
//     if($("#ju-space-around").hasClass("ju-space-around")){
//         $("#ju-space-around").removeClass('ju-space-around')
//     }else{

//         $("#ju-space-around").addClass('ju-space-around')  
//         }    
// });

// // BOUTON DU AL FLEX START

// $("#btn-al-flex-start").click(function (){
//     if($("#al-flex-start").hasClass("al-flex-start")){
//         $("#al-flex-start").removeClass('al-flex-start')
//     }else{

//         $("#al-flex-start").addClass('al-flex-start')  
//         }    
// });

// // BOUTON DU AL FLEX END

// $("#btn-al-flex-end").click(function (){
//     if($("#al-flex-end").hasClass("al-flex-end")){
//         $("#al-flex-end").removeClass('al-flex-end')
//     }else{

//         $("#al-flex-end").addClass('al-flex-end')  
//         }    
// });

// // BOUTON DU AL CENTER

// $("#btn-al-center").click(function (){
//     if($("#al-center").hasClass("al-center")){
//         $("#al-center").removeClass('al-center')
//     }else{

//         $("#al-center").addClass('al-center')  
//         }    
// });

// // BOUTON DU AL BASELINE

// $("#btn-al-baseline").click(function (){
//     if($("#al-baseline").hasClass("al-baseline")){
//         $("#al-baseline").removeClass('al-baseline')
//     }else{

//         $("#al-baseline").addClass('al-baseline')  
//         }    
// });

// // BOUTON DU FL ROW

// $("#btn-fl-row").click(function (){
//     if($("#fl-row").hasClass("fl-row")){
//         $("#fl-row").removeClass('fl-row')
//     }else{

//         $("#fl-row").addClass('fl-row')  
//         }    
// });

// // BOUTON DU FL ROW REVERSE

// $("#btn-fl-row-reverse").click(function (){
//     if($("#fl-row-reverse").hasClass("fl-row-reverse")){
//         $("#fl-row-reverse").removeClass('fl-row-reverse')
//     }else{

//         $("#fl-row-reverse").addClass('fl-row-reverse')  
//         }    
// });

// // BOUTON DU FL COLUMN

// $("#btn-fl-column").click(function (){
//     if($("#fl-column").hasClass("fl-column")){
//         $("#fl-column").removeClass('fl-column')
//     }else{

//         $("#fl-column").addClass('fl-column')  
//         }    
// });

// // BOUTON DU ORDER

// $("#btn-order").click(function (){
//     if($("#order").hasClass("order")){
//         $("#order").removeClass('order')
//     }else{

//         $("#order").addClass('order')  
//         }    
// });

// // BOUTTON DU ALIGN SELF

// $("#btn-align-self").click(function (){
//     if($("#align-self").hasClass("align-self")){
//         $("#align-self").removeClass('align-self')
//     }else{

//         $("#align-self").addClass('align-self')  
//         }    
// });



// $("#btn-nowrap").click(function (){
//     if($("#nowrap").hasClass("nowrap")){
//         $("#nowrap").removeClass('nowrap')
//     }else{

//         $("#nowrap").addClass('nowrap')  
//         }    
// });

// // BOUTTON DU WRAP

// $("#btn-wrap").click(function (){
//     if($("#wrap").hasClass("wrap")){
//         $("#wrap").removeClass('wrap')
//     }else{

//         $("#wrap").addClass('wrap')  
//         }    
// });

// // BOUTON DU WRAP REVERSE

// $("#btn-wrap-reverse").click(function (){
//     if($("#wrap-reverse").hasClass("wrap-reverse")){
//         $("#wrap-reverse").removeClass('wrap-reverse')
//     }else{

//         $("#wrap-reverse").addClass('wrap-reverse')  
//         }    
// });
//  BOUTON DU FLEX FLOW

// $("#btn-flex-flow").click(function (){
//     if($("#flex-flow").hasClass("ju-space-around")){
//         $("#flex-flow").removeClass('ju-space-around')
//     }else{

//         $("#flex-flow").addClass('flex-flow')  
//         }    
// });


//  LES BOUTONS DU FLEX V2

   //bouton ju-flex-start

   $("#btn-ju-flex-start").click(function (){
       $("#ju-flex-start").toggleClass("ju-flex-start");
       $("#ju-flex-start").toggleClass("flex_basique");
   });

   // bouton ju-flex-end

   $("#btn-ju-flex-end").click(function (){
    $("#ju-flex-end").toggleClass("ju-flex-end");
    $("#ju-flex-end").toggleClass("flex_basique");
});


// bouton ju-center

$("#btn-ju-center").click(function (){
    $("#ju-center").toggleClass("ju-center");
    $("#ju-center").toggleClass("flex_basique");
});

// bouton ju-space-between

$("#btn-ju-space-between").click(function (){
    $("#ju-space-between").toggleClass("ju-space-between");
    $("#ju-space-between").toggleClass("flex_basique");
});

// bouton ju-space-around

$("#btn-ju-space-around").click(function (){
    $("#ju-space-around").toggleClass("ju-space-around");
    $("#ju-space-around").toggleClass("flex_basique");
});

// bouton al-flex-start

$("#btn-al-flex-start").click(function (){
    $("#al-flex-start").toggleClass("al-flex-start");
    $("#al-flex-start").toggleClass("flex_basique");
});

// bouton al-flex-end

$("#btn-al-flex-end").click(function (){
    $("#al-flex-end").toggleClass("al-flex-end");
    $("#al-flex-end").toggleClass("flex_basique");
});

// bouton al-center

$("#btn-al-center").click(function (){
    $("#al-center").toggleClass("al-center");
    $("#al-center").toggleClass("flex_basique");
});

// bouton al-baseline

$("#btn-al-baseline").click(function (){
    $("#al-baseline").toggleClass("al-baseline");
    $("#al-baseline").toggleClass("flex_basique");
});

// bouton fl-row

$("#btn-fl-row").click(function (){
    $("#fl-row").toggleClass("fl-row");
    $("#fl-row").toggleClass("flex_basique");
});

// bouton fl-row-reverse

$("#btn-fl-row-reverse").click(function (){
    $("#fl-row-reverse").toggleClass("fl-row-reverse");
    $("#fl-row-reverse").toggleClass("flex_basique");
});

// bouton fl-column

$("#btn-fl-column").click(function (){
    $("#fl-column").toggleClass("fl-column");
    $("#fl-column").toggleClass("flex_basique");
});

// bouton fl-column-reverse

$("#btn-fl-column-reverse").click(function (){
    $("#fl-column-reverse").toggleClass("fl-column-reverse");
    $("#fl-column-reverse").toggleClass("flex_basique");
});

// bouton order

$("#btn-order").click(function (){
    $("#order").toggleClass("order");
    $("#order").toggleClass("flex_basique");
    $("#box1").toggleClass("boxe");
    $("#box1").toggleClass("box");
});

//  bouton align-self

$("#btn-align-self").click(function (){
    $("#align-self ").toggleClass("align-self");
    $("#align-self ").toggleClass("flex_basique");
});

// bouton nowrap

$("#btn-nowrap").click(function (){
    $("#ju-flex-start").toggleClass("nowrap");
    $("#nowrap").toggleClass("flex_basique");
});

// bouton wrap

$("#btn-wrap").click(function (){
    $("#wrap").toggleClass("wrap");
    $("#wrap").toggleClass("flex_basique");
});

// bouton wrap-reverse

$("#btn-wrap-reverse").click(function (){
    $("#wrap-reverse").toggleClass("wrap-reverse");
    $("#wrap-reverse").toggleClass("flex_basique");
});

// bouton flex-flow

$("#btn-flex-flow").click(function (){
    $("#flex-flow").toggleClass("flex-flow");
    $("#flex-flow").toggleClass("flex_basique");
});



// toggle flex
 $(document).ready(function(){
    $('#b1').click(function(){
        $('.justify-content').toggle();
    });
});
$(document).ready(function(){
    $('#b2').click(function(){
        $('.Align-items').toggle();
    });
});
$(document).ready(function(){
    $('#b3').click(function(){
        $('.Flex-direction').toggle();
    });
});
$(document).ready(function(){
    $('#b4').click(function(){
        $('.the_order').toggle();
    });
});
$(document).ready(function(){
    $('#b5').click(function(){
        $('.align-self').toggle();
    });
});
$(document).ready(function(){
    $('#b6').click(function(){
        $('.flex-wrap').toggle();
    });
});
$(document).ready(function(){
    $('#b7').click(function(){
        $('.the_flex-flow').toggle();
    });
});


// toggle grid

$(document).ready(function(){
    $('#btn1').click(function(){
        $('.grid-template-column').toggle();
    });
});
$(document).ready(function(){
    $('#btn2').click(function(){
        $('.grid-template-row').toggle();
    });
});
$(document).ready(function(){
    $('#btn3').click(function(){
        $('.grid-column-gap').toggle();
    });
});
$(document).ready(function(){
    $('#btn4').click(function(){
        $('.grid-row-gap').toggle();
    });
});
$(document).ready(function(){
    $('#btn5').click(function(){
        $('.grid-gap').toggle();
    });
});
$(document).ready(function(){
    $('#btn6').click(function(){
        $('.grid-column-start').toggle();
    });
});
$(document).ready(function(){
    $('#btn7').click(function(){
        $('.grid-column-end').toggle();
    });
});
$(document).ready(function(){
    $('#btn8').click(function(){
        $('.grid-column').toggle();
    });
});
$(document).ready(function(){
    $('#btn9').click(function(){
        $('.grid-row-start').toggle();
    });
});
$(document).ready(function(){
    $('#btn10').click(function(){
        $('.grid-row-end').toggle();
    });
});
$(document).ready(function(){
    $('#btn11').click(function(){
        $('.grid-row').toggle();
    });
});
$(document).ready(function(){
    $('#btn12').click(function(){
        $('.grid-area').toggle();
    });
});

// test bouton accueil

// $(document).ready(function(){
//     $('#btn_page_grid').click(function(){
//         console.log("testeu")
//         $('grid_head').toggle();
//     });
// });



